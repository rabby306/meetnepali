package com.nixbin.MeetNepali.wrapper;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by advosoft on 10/12/2017.
 */
@IgnoreExtraProperties
public class SendGrpMessage {
    public String content;
    public String fromID;
    public long timestamp;
    public String type;

    public SendGrpMessage(String content, String fromID,  long timestamp, String type) {
        this.content = content;
        this.fromID = fromID;
        this.timestamp = timestamp;
        this.type = type;
    }

    public SendGrpMessage() {
    }


}
