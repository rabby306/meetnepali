package com.nixbin.MeetNepali.wrapper;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by advosoft on 10/12/2017.
 */
@IgnoreExtraProperties
public class SendMessage {
    public String content;
    public String fromID;
    public Boolean isRead;
    public long timestamp;
    public String toID;
    public String type;

    public SendMessage(String content, String fromID, Boolean isRead, long timestamp, String toID, String type) {
        this.content = content;
        this.fromID = fromID;
        this.isRead = isRead;
        this.timestamp = timestamp;
        this.toID = toID;
        this.type = type;
    }

    public SendMessage() {
    }


}
