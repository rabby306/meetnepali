package com.nixbin.MeetNepali.wrapper.models;

import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class RegionResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private RegionData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RegionData getData() {
        return data;
    }

    public void setData(RegionData data) {
        this.data = data;
    }

}
