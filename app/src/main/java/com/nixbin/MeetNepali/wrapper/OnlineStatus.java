package com.nixbin.MeetNepali.wrapper;

/**
 * Created by advosoft on 10/16/2017.
 */

public class OnlineStatus {
    public long lastOnline;
    public String status;

    public OnlineStatus(long lastOnline, String status) {
        this.lastOnline = lastOnline;
        this.status = status;
    }

    public OnlineStatus() {
    }
}
