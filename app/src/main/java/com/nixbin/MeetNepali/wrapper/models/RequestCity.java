package com.nixbin.MeetNepali.wrapper.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestCity {

    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
