package com.nixbin.MeetNepali.wrapper;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by advosoft on 12/5/2017.
 */
@IgnoreExtraProperties
public class GroupChat {
   public String creator;
    public String group_icon;
    public String isPrivate;
    public  String members;
    public String name;
    public long timestamp;

    public GroupChat() {
        // Default constructor required for calls to DataSnapshot.getValue(AddGroupWrapper.class)
    }

    public GroupChat(String creator, String group_icon, String isPrivate, String members, String name, long timestamp) {
        this.creator = creator;
        this.group_icon = group_icon;
        this.isPrivate = isPrivate;
        this.members = members;
        this.name = name;
        this.timestamp = timestamp;
    }


}
