package com.nixbin.MeetNepali.wrapper.models;

import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Region {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("countryID")
    @Expose
    private Integer countryID;
    @SerializedName("regionCode")
    @Expose
    private String regionCode;
    @SerializedName("region")
    @Expose
    private String region;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryID() {
        return countryID;
    }

    public void setCountryID(Integer countryID) {
        this.countryID = countryID;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

}
