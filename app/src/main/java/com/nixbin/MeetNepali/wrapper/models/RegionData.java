package com.nixbin.MeetNepali.wrapper.models;

import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class RegionData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("countryName")
    @Expose
    private String countryName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("countryCode3")
    @Expose
    private String countryCode3;
    @SerializedName("regions")
    @Expose
    private List<Region> regions = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

}
