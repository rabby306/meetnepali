package com.nixbin.MeetNepali.wrapper;

import java.io.Serializable;

/**
 * Created by Advosoft2 on 3/9/2017.
 */

public class Bindcontract implements Serializable {

    String profileImg;

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    String name;
    String lastmsg;
    String timestamp;
    String status;
    String chatkey;
    String fromid;
    String lastmsgtype;
    int grpcreateddate;
    int creatorID;

    String friendid;
    String email;

    public Integer getCreatorID()
    {
        return creatorID;
    }

    public void setCreatorID(Integer creatorID)
    {
        this.creatorID = creatorID;
    }

    public String getAdmin_key() {
        return admin_key;
    }

    public void setAdmin_key(String admin_key) {
        this.admin_key = admin_key;
    }

    String admin_key;

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    String member;

    public String getOtherid() {
        return otherid;
    }

    public void setOtherid(String otherid) {
        this.otherid = otherid;
    }

    String otherid;

    public String getGrpisPrivate() {
        return grpisPrivate;
    }

    public void setGrpisPrivate(String grpisPrivate) {
        this.grpisPrivate = grpisPrivate;
    }

    String grpisPrivate;


    public int getGrpcreateddate() {
        return grpcreateddate;
    }

    public void setGrpcreateddate(int grpcreateddate) {
        this.grpcreateddate = grpcreateddate;
    }

    public String getChattype() {
        return chattype;
    }

    public void setChattype(String chattype) {
        this.chattype = chattype;
    }

    String chattype;

    public String getLastmsgtype() {
        return lastmsgtype;
    }

    public void setLastmsgtype(String lastmsgtype) {
        this.lastmsgtype = lastmsgtype;
    }

    public String getFromid() {
        return fromid;
    }

    public void setFromid(String fromid) {
        this.fromid = fromid;
    }

    public String getChatkey() {
        return chatkey;
    }

    public void setChatkey(String chatkey) {
        this.chatkey = chatkey;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastmsg() {
        return lastmsg;
    }

    public void setLastmsg(String lastmsg) {
        this.lastmsg = lastmsg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
