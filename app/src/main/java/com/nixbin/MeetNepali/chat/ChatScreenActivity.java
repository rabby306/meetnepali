package com.nixbin.MeetNepali.chat;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.groupDetails;
import com.nixbin.MeetNepali.activities.BaseActiivty;
import com.nixbin.MeetNepali.activities.MainActivity;
import com.nixbin.MeetNepali.activities.MapGetLocActivity;
import com.nixbin.MeetNepali.activities.SelectMember;
import com.nixbin.MeetNepali.adpter.ChatAdpterClass;
import com.nixbin.MeetNepali.application.AppConfig;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.Helper;
import com.nixbin.MeetNepali.utilities.ServerCallBack;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.ApiCall;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Address;
import com.nixbin.MeetNepali.wrapper.Bindcontract;
import com.nixbin.MeetNepali.wrapper.ChatGroupsMain;
import com.nixbin.MeetNepali.wrapper.NewUser;
import com.nixbin.MeetNepali.wrapper.OnlineStatus;
import com.nixbin.MeetNepali.wrapper.SendGrpMessage;
import com.nixbin.MeetNepali.wrapper.SendMessage;
import com.nixbin.MeetNepali.utilities.SharedPrefManager;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbOnlineStatusTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbchatTable;

/**
 * Created by advosoft on 10/11/2017.
 */

public class ChatScreenActivity extends BaseActiivty implements WebCompleteTask {

    @Bind(R.id.is_typing_img)
    ImageView isTypingImg;
    @Bind(R.id.is_typing)
    TextView isTyping;
    @Bind(R.id.is_typing_ll)
    LinearLayout isTypingLl;
    @Bind(R.id.editEmojicon)
    EditText editEmojicon;
    @Bind(R.id.add_group1)
    ImageView addGroup1;
    @Bind(R.id.camera_icon_chat)
    ImageView cameraIconChat;
    @Bind(R.id.upload_image)
    ImageView uploadImage;
    @Bind(R.id.iv_location)
    ImageView ivLocation;
    @Bind(R.id.send_btn)
    ImageView sendBtn;
    @Bind(R.id.plus_frag)
    LinearLayout plusFrag;
    @Bind(R.id.sticker_frag)
    RelativeLayout stickerFrag;
    @Bind(R.id.bottom_layout)
    LinearLayout bottomLayout;
    @Bind(R.id.loading_more)
    TextView loadingMore;
    @Bind(R.id.loading_rl)
    RelativeLayout loadingRl;
    @Bind(R.id.connecting_in)
    TextView connectingIn;
    @Bind(R.id.connect_layout)
    LinearLayout connectLayout;
    @Bind(R.id.aaaaa)
    LinearLayout aaaaa;
    @Bind(R.id.comment_list)
    ListView commentList;
    @Bind(R.id.main_list)
    RelativeLayout mainList;
    @Bind(R.id.root_rl)
    RelativeLayout rootRl;


    @Bind(R.id.mute_toggle)
    ImageView muteToggle;
    private long mLastClickTime = 0;

    @Bind(R.id.join_leave_toggle)
    ImageView joinLeaveToggle;
    @Bind(R.id.addGroupMember)
    ImageView addGroupMember;

    public static QuickAction quick;
    @Bind(R.id.profile_image)
    ImageView profileImage;
    @Bind(R.id.receiver_name)
    TextView receiverName;
    @Bind(R.id.receiver_status)
    TextView receiverStatus;
    @Bind(R.id.add_group)
    ImageView addGroup;
    @Bind(R.id.information)
    ImageView information;
    private Uri fileUri; // file url to store image/video
    Uri videoUri;



    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String IMAGE_DIRECTORY_NAME = "dothat image";

    final int GET_LOCATION_REQ = 1003;
    public static Context context;
    static Bindcontract chatInfo;
    String membername, memberprofile;
    Boolean mem = false, pro = false;

    ArrayList<ChatGroupsMain> feedsListall = new ArrayList();
    ChatAdpterClass adapter;
    public static String receverprofile;
    String chatID;
    String onlinestatus;

    ArrayList<String> mutedGroups;
    ArrayList<String> joinedGroups;
    List<String> memberList;

    ProgressDialog progressDialog = null;
    JSONObject postData = new JSONObject();
    private final String TAG = "ChatScreen";


    //TODO: get chat info here just by chat key...
    private DatabaseReference fbDatabase;
    ArrayList<String> conversation_key = new ArrayList<>();
    //end amit

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_screen);
        ButterKnife.bind(this);
        context = this;
        //TODO:get chat details.. or send it through push notificaiton..
        //DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(conversation_key.get(i));


        chatInfo = (Bindcontract) getIntent().getSerializableExtra("data");
        //later on remove all crap passing through intent, and just send the chatID. and chat type
        chatID = chatInfo.getChatkey();
        Log.d(TAG, "Chat Key: " + chatID);

        //amit test
        /*
        ValueEventListener chatDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GroupChat groupChat = dataSnapshot.getValue(GroupChat.class);
                receverprofile = groupChat.group_icon;


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        DatabaseReference groupChatRef = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(chatID);

        groupChatRef.addListenerForSingleValueEvent(chatDetailsListener);
        */
        //amit test end, that works.. Implement later..

        receverprofile = chatInfo.getProfileImg();
        Utils.loadImageRound(context, profileImage, chatInfo.getProfileImg());
        receiverName.setText(chatInfo.getName());

        final SharedPrefManager SharedPref = new SharedPrefManager(context);

        joinedGroups = SharedPref.getArray("MeetNepali-joinedGroups");
        mutedGroups = SharedPref.getArray("MeetNepali-mutedGroups");
        WebUrls.debugPrint("Chat Screen", "Chat Type:"+chatInfo.getGrpisPrivate());
        //handle push notification,fix this crap later..
        chatInfo.setGrpisPrivate((chatInfo.getGrpisPrivate() == null) ? "private" : chatInfo.getGrpisPrivate());

        Log.d("Chat Screen", "Chat Private:"+chatInfo.getGrpisPrivate());
        Log.d("Chat Screen", "Chat Key:" + chatInfo.getChatkey());
        if(chatInfo.getGrpisPrivate().equalsIgnoreCase("private"))
        {
            if(chatInfo.getFromid().startsWith("grp"))
            {
                setupMuteButton();
                joinLeaveToggle.setActivated(true);
                addGroupMember.setVisibility(View.VISIBLE);

            }
            else
            {
                setupSingleChatScreen();

            }

        }
        else if(chatInfo.getGrpisPrivate().equalsIgnoreCase("public"))
        {
            muteToggle.setVisibility(View.GONE);

            //List<String> list = new ArrayList<String>(Arrays.asList(chatInfo.getMember().split(" , ")));
            try{
                 memberList = Arrays.asList(chatInfo.getMember().split(","));
            }catch(Exception e)
            {
                //this means this is from push notification
                 memberList = Arrays.asList(MyApplication.getUserID(Constants.UserID));
            }


            //combine these two ifs later..
            if (memberList.contains(MyApplication.getUserID(Constants.UserID))) {
               Helper.debugPrint(TAG, "User member of this group");
                joinLeaveToggle.setActivated(true);
                //we show muted only if group is joined.
                setupMuteButton();
                muteToggle.setVisibility(View.VISIBLE);
                addGroupMember.setVisibility(View.VISIBLE);
            }

            if (joinedGroups.toString().contains(chatInfo.getChatkey())) {
                Helper.debugPrint(TAG, "Group Already Joined");
                joinLeaveToggle.setActivated(true);
                //we show muted only if group is joined.
                setupMuteButton();
                muteToggle.setVisibility(View.VISIBLE);
                addGroupMember.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            setupSingleChatScreen();
        }






        //if (chatInfo.getFromid().startsWith("grp") && chatInfo.getGrpisPrivate().compareTo("private") == 0) {
        //if (chatInfo.getFromid().startsWith("grp") && ((chatInfo.getGrpisPrivate().compareTo("private") == 0) || chatInfo.getCreatorID().toString().equals(MyApplication.getUserID(Constants.UserID)))) {
        if (chatInfo.getFromid().startsWith("grp")) {
            information.setVisibility(View.VISIBLE);
            Log.d("Chat Screen", "Group is private or user is the admin");
        } else {
            information.setVisibility(View.GONE);
            Log.d("Chat Screen", "Group is not private or user is not the admin");
        }
        information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChatScreenActivity.this, groupDetails.class).putExtra("chatkey", chatInfo.getFromid()));
            }
        });

        adapter = new ChatAdpterClass(ChatScreenActivity.this, feedsListall);
        /*if (chatInfo.getChattype().compareTo("grp") == 0) {
            Timestamp stamp = new Timestamp(chatInfo.getGrpcreateddate());
            long sta = stamp.getTime();
            String ds = getDate(sta);
            receiverStatus.setText(ds);
        } else {
            showOnlineStatus();
        }*/
        showOnlineStatus(); // amit modified and commented above block because it was challenging to send grpcreateddate in push notification
        getchatKey();

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.enLargeImage(ChatScreenActivity.this, chatInfo.getProfileImg());
            }
        });



        addGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (getIntent().getExtras()!=null){
                    if (getIntent().getExtras().getString("activity").compareTo("mainactivity")==0){
                        replaceFragment(R.id.container, new HomepageNew());
                    }else if (getIntent().getExtras().getString("activity").compareTo("fromchat")==0){
                       finish();
                    }else {
                        finish();
                    }
                }else {
                    finish();
                }*/
                finish();

            }
        });

        //muteToggle = (ImageView) findViewById(R.id.mute_toggle);
        muteToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action;
                String toastMsg;
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                muteToggle.setActivated(!muteToggle.isActivated());

                if (muteToggle.isActivated()) {
                    Log.d("Toggle", "Activated, GroupID: "+chatInfo.getChatkey());
                    mutedGroups.add(chatInfo.getChatkey());
                    action = "mute";
                    toastMsg = "Notifications Turned off.";
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(MyApplication.TOPIC_PREFIX+chatInfo.getChatkey());
                    //saveArray(String key, ArrayList<String> array)
                    Log.d("Toggle", "Muted Groups:"+mutedGroups.toString());
                }
                else
                {
                    Log.d("Toggle", "Deactivated");
                    mutedGroups.remove(chatInfo.getChatkey());
                    action = "unmute";
                    toastMsg = "Notifications Turned On.";
                    FirebaseMessaging.getInstance().subscribeToTopic(MyApplication.TOPIC_PREFIX+chatInfo.getChatkey());

                }
                SharedPref.saveArray("MeetNepali-mutedGroups", mutedGroups);
                callAPI(chatInfo,action);

            }
        });

        joinLeaveToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action;
                String toastMsg;
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                joinLeaveToggle.setActivated(!joinLeaveToggle.isActivated());
                Log.d("API", "Chat Info:"+chatInfo.toString());
                if (joinLeaveToggle.isActivated()) {
                    Log.d("Toggle", "Joining Group: "+chatInfo.getChatkey());
                    joinedGroups.add(chatInfo.getChatkey());
                    toastMsg = "Group Joined";
                    FirebaseMessaging.getInstance().subscribeToTopic(MyApplication.TOPIC_PREFIX+chatInfo.getChatkey());
                    action = "add_members";
                    muteToggle.setVisibility(View.VISIBLE);
                    addGroupMember.setVisibility(View.VISIBLE);
                }
                else
                {
                    Log.d("Toggle", "Deactivated");
                    joinedGroups.remove(chatInfo.getChatkey());
                    Log.d("Toggle", "Leaving Group: "+chatInfo.getChatkey());
                    toastMsg = "Group Left";
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(MyApplication.TOPIC_PREFIX+chatInfo.getChatkey());
                    action = "remove_members";
                    muteToggle.setVisibility(View.GONE);
                    addGroupMember.setVisibility(View.GONE);
                }
                SharedPref.saveArray("MeetNepali-joinedGroups", joinedGroups);
                callAPI(chatInfo,action);

            }
        });


        addGroupMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), SelectMember.class);
                intent.putExtra("GROUP_ID", chatInfo.getChatkey());
                intent.putExtra("GROUP_MEMBERS", chatInfo.getMember());
                intent.putExtra("ACTION", "add_members");
                intent.putExtra("GROUP_NAME", chatInfo.getName());
                intent.putExtra("GROUP_TYPE", chatInfo.getGrpisPrivate());
                intent.putExtra("CREATOR_ID", chatInfo.getCreatorID());
                startActivity(intent);

                //startActivity(new Intent(ChatScreenActivity.this, SelectMember.class));
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (quick != null)
                        quick.dismiss();

                    String text = editEmojicon.getText().toString().trim();

                    if (!text.equals("")) {

                        if (chatInfo.getFromid().startsWith("grp")) {
                            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbGroupChatTable).child(chatInfo.getFromid());
                            String userId = mDatabase.push().getKey();
                            String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                            String toid = chatInfo.getFromid();

                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            SendGrpMessage sendMessage = new SendGrpMessage(text, fromid, tsLong, "text");
                            mDatabase.child(userId).setValue(sendMessage);

                            editEmojicon.setText("");
                            send_group_notification(text);

                        } else {
                            if (adapter.getCount() == 0) {

                                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
                                String userId = mDatabase.push().getKey();
                                //  mDatabase.child(userId);
                                DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(userId);
                                String userId2 = mDatabase2.push().getKey();

                                String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                                String toid = chatInfo.getFromid();

                                Long tsLong = System.currentTimeMillis() / 1000;
                                String ts = tsLong.toString();
                                SendMessage sendMessage = new SendMessage(editEmojicon.getText().toString().trim(), fromid, true, tsLong, toid, "text");
                                mDatabase2.child(userId2).setValue(sendMessage);


                                String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(toid).child(fbchatTable);
                                NewUser newUser = new NewUser(userId);
                                ref.child(currentUserName).setValue(newUser);

                                DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
                                NewUser newUser1 = new NewUser(userId);
                                ref1.child(toid).setValue(newUser1);


                                editEmojicon.setText("");
                                if (onlinestatus.compareToIgnoreCase("Offline") == 0) {
                                    send_chat_notification(text);
                                }


                            } else {
                                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(chatID);
                                String userId = mDatabase.push().getKey();
                                String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                                String toid = chatInfo.getFromid();

                                Long tsLong = System.currentTimeMillis() / 1000;
                                String ts = tsLong.toString();
                                SendMessage sendMessage = new SendMessage(text, fromid, true, tsLong, toid, "text");
                                mDatabase.child(userId).setValue(sendMessage);

                                editEmojicon.setText("");
                                if (onlinestatus.compareToIgnoreCase("Offline") == 0) {
                                    send_chat_notification(text);
                                }

                            }
                        }


                    }

                } catch (Exception e) {

                }


            }
        });

        cameraIconChat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // GooberApp.getInstance().trackEvent("upload photo", "process",
                // "Track event login");
                // Tracker t = ((GooberApp)
                // getApplication().getApplicationContext())
                // .getTracker(TrackerName.APP_TRACKER);
                // t.setScreenName("User Photo Upload ");
                // t.send(new HitBuilders.AppViewBuilder().build());
                if (quick != null)
                    quick.dismiss();

                Utils.hideKeyboardForFocusedView(ChatScreenActivity.this);
                //  launchCamera(0);

               /* Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                String term = "michel jecson";
                intent.putExtra(SearchManager.QUERY, term);
                startActivity(intent);*/
                launchCamera(0);

                Utils.selectMedia = true;


            }
        });

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quick != null)
                    quick.dismiss();
                Utils.hideKeyboardForFocusedView(ChatScreenActivity.this);
                Utils.selectMedia = true;
                getVideo(0);

            }
        });

        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatScreenActivity.this, MapGetLocActivity.class);
                startActivityForResult(intent, GET_LOCATION_REQ);
            }
        });
    }

    private void setupMuteButton()
    {
        if (mutedGroups.toString().contains(chatInfo.getChatkey())) {
            muteToggle.setActivated(true);
        }

    }

    private void setupSingleChatScreen()
    {
        muteToggle.setVisibility(View.GONE);
        joinLeaveToggle.setVisibility(View.GONE);
        muteToggle.setVisibility(View.GONE);
        addGroupMember.setVisibility(View.GONE);

        WebUrls.debugPrint("Chat Screen", "One to one chat, not showing join and mute buttons");
    }

    private void callAPI(Bindcontract chat, final String action)
    {
        JSONObject apiCallData = new JSONObject();
        ApiCall apiCall = new ApiCall();
        try {
            apiCallData.put("action", action);
            apiCallData.put("group_id", chat.getChatkey());
            apiCallData.put("creator_id", chat.getCreatorID());
            apiCallData.put("group_name", chat.getName());
            apiCallData.put("group_members", chat.getMember());
            apiCallData.put("update_ids", MyApplication.getUserID(Constants.UserID));
            apiCallData.put("update_names", MyApplication.getUserName(Constants.UserName));
            apiCallData.put("chat_type", chat.getGrpisPrivate());
            apiCallData.put("device_token", MyApplication.getUserData(Constants.GCM_ID));

        } catch (Exception e) {
            Log.d("Chat Screen", "513- Error"+e.toString());
        }
        apiCall.apiPost(this, "manage_group", apiCallData, new ServerCallBack() {
                    @Override
                    public void onSuccess(String responseMessage, String responseData) {


                        if(action == "remove_members" || action == "add_members")
                        {
                           try{
                               String newMembers = responseData;
                               Toast.makeText(getApplicationContext(), responseMessage, Toast.LENGTH_LONG).show();
                               //chatInfo.setMember = newMembers;
                               chatInfo.setMember(newMembers);
                           }
                           catch(Exception e)
                           {
                               Log.d("Chat Screen", "521 - Exception " + e.toString());
                           }


                          /*   finish();
                            startActivity(getIntent());*/


                        }
                        if(action == "remove_members")
                        {

                            finish();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("dataaction", "chatFragment");
                            startActivity(intent);
                        }
                    }

                }
        );


    }







    private void showOnlineStatus() {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println("online status " + dataSnapshot.getValue());
                System.out.println("online status key " + dataSnapshot.getKey());
                // data[0] = dataSnapshot.getValue().toString();

                if (dataSnapshot.getKey().compareTo(chatInfo.getFromid().trim()) == 0) {
                    try {
                        Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                        Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));
                        JSONObject jsonObject = new JSONObject(value1);
                        receiverStatus.setText(jsonObject.getString("status"));
                        onlinestatus = jsonObject.getString("status");
                        if (jsonObject.getString("status").compareTo("online") == 0) {
                            receiverStatus.setTextColor(getResources().getColor(R.color.green));
                        } else {
                            receiverStatus.setTextColor(getResources().getColor(R.color.white));
                        }
                    } catch (Exception e) {

                    }

                }



               /* try {
                    JSONObject jsonObject = new JSONObject(value1);

                    ChatGroupsMain contract = new ChatGroupsMain();

                    contract.setTimestamp(jsonObject.getString("timestamp"));
                    contract.setContent(jsonObject.getString("content"));
                    contract.setType(jsonObject.getString("type"));
                    contract.setIsread(jsonObject.getString("isRead"));
                    contract.setFromid(jsonObject.getString("fromID"));

                    feedsListall.add(contract);
                    setList();
                } catch (Exception e) {

                }*/

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getchatKey() {
        if (chatInfo.getFromid().startsWith("grp") && chatInfo.getGrpisPrivate().compareTo("public") == 0) {
            Log.d("ChatScreen", "435 - Chat key: "+chatInfo.getChatkey());
            getDatakey(chatInfo.getChatkey());

        } else {
            String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
            //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable).child(chatInfo.getFromid());
            Log.d("ChatScreen", "Chat Key : " + chatInfo.getFromid());
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    System.out.println("child data key " + dataSnapshot.getKey());
                    System.out.println("child data value chat" + dataSnapshot.getValue());
                    chatID = dataSnapshot.getValue().toString();

                    getDatakey(dataSnapshot.getValue().toString());

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }


    private void getDatakey(String chatkey) {
        DatabaseReference ref;
        if (chatInfo.getFromid().startsWith("grp")) {
            ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbGroupChatTable).child(chatkey);
        } else {
            ref = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(chatkey);
        }


        ref.limitToLast(MyApplication.LOAD_MAX_CONVERSATION_LIST).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println("conversition data key for getdata " + dataSnapshot.getValue());
                // data[0] = dataSnapshot.getValue().toString();



                try {
                    Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                    Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));
                    JSONObject jsonObject = new JSONObject(value1);

                    ChatGroupsMain contract = new ChatGroupsMain();
                    contract.setTimestamp(jsonObject.getInt("timestamp") + "");
                    contract.setContent(jsonObject.getString("content"));
                    contract.setType(jsonObject.getString("type"));
                    contract.setFromid(jsonObject.getString("fromID"));


                    //getMemberinfo(jsonObject.getString("fromID"), jsonObject);

                    // getMemberName(jsonObject.getString("fromID"));

                    // contract.setMemberProfile(memberprofile);
                    // contract.setMembername(membername);
                    if (!chatInfo.getFromid().startsWith("grp")) {
                        contract.setIsread(jsonObject.getString("isRead"));
                        contract.setGrpType("singlechat");

                    } else {
                        contract.setGrpType("grp");
                    }

                    feedsListall.add(contract);
                    setList();
                } catch (Exception e) {

                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void getMemberinfo(final String key, final JSONObject obj) {

        final String[] member_profile = {""};

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(key).child("meet_credentials");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                // data[0] = dataSnapshot.getValue().toString();


                try {

                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                    // data[0] = new JSONObject(value).toString();
                    System.out.println("contentadded " + value);

                    JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));

                    member_profile[0] = jsonObject.getString("profilePicLink");
                    memberprofile = jsonObject.getString("profilePicLink");
                    // member_profile.add(jsonObject.getString("profilePicLink"));
                    // name.set(finalI, jsonObject.getString("name"));



                       /* ChatGroupsMain contract = new ChatGroupsMain();
                        contract.setTimestamp(obj.getInt("timestamp") + "");
                        contract.setContent(obj.getString("content"));
                        contract.setType(obj.getString("type"));
                        contract.setFromid(obj.getString("fromID"));
                        contract.setMemberProfile(jsonObject.getString("profilePicLink"));
                        contract.setMembername(jsonObject.getString("name"));
                        if (!chatInfo.getFromid().startsWith("grp")) {
                            contract.setIsread(obj.getString("isRead"));
                            contract.setGrpType("singlechat");

                        } else {
                            contract.setGrpType("grp");
                        }

                        feedsListall.add(contract);
                        setList();
*/

                } catch (Exception e) {
                Log.d("ChatScreen", "603-Exception:"+e.toString());
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private String getMemberName(String key) {
        final String[] member_profile = {""};
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(key).child("meet_credentials");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                // data[0] = dataSnapshot.getValue().toString();
                try {
                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                    // data[0] = new JSONObject(value).toString();


                    JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));

                    member_profile[0] = jsonObject.getString("name");
                    membername = jsonObject.getString("name");
                    System.out.println("member name " + jsonObject.getString("name"));
                    // name.set(finalI, jsonObject.getString("name"));


                } catch (Exception e) {

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        return member_profile[0];
    }


    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {

        } else {
            // empty_view.setVisibility(View.GONE);
            adapter = new ChatAdpterClass(ChatScreenActivity.this, feedsListall);
            commentList.setAdapter(adapter);
        }
    }

    public void launchCamera(int id) {
        switch (id) {
            case 0:
                // String fileName1 = "temp_image_"
                // + (System.currentTimeMillis() / 1000) + ".jpg";
                // ContentValues values = new ContentValues();
                // values.put(MediaStore.Images.Media.TITLE, fileName1);
                //
                // mUriCapturedImage = ctx.getContentResolver().insert(
                // MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                //
                // Intent cameraIntent = new Intent(
                // android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //
                // cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                // mUriCapturedImage);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, Utils.IMAGE_CAMERA_CHAT);
                break;
            case 1:
                String state = Environment.getExternalStorageState();
                if (!Environment.MEDIA_MOUNTED.equals(state)) {
                    Toast.makeText(ChatScreenActivity.this, "" + getString(R.string.mount_sdcard),
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent videoIntent = new Intent(
                            MediaStore.ACTION_VIDEO_CAPTURE);
                    File tempFolder = new File(
                            Environment.getExternalStorageDirectory(), ".MediaTemp");
                    tempFolder.mkdirs();
                    String fileName = "temp_video_"
                            + (System.currentTimeMillis() / 1000) + ".mp4";
                    Utils.VIDEO_CAMERA_CHAT_PATH = Environment
                            .getExternalStorageDirectory()
                            + "/.MediaTemp/"
                            + fileName;
                    File video = new File(tempFolder, fileName);
                    videoUri = Uri.fromFile(video);
                    videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
                    videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                    videoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                    videoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,
                            (long) (16 * 1024 * 1024));
                    startActivityForResult(videoIntent, Utils.VIDEO_CAMERA_CHAT);

                }
                break;
            default:
                break;
        }
    }

    public void getVideo(int id) {
        switch (id) {
            case 0:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, Utils.IMAGE_GALLERRY_CHAT);
                break;
            case 1:
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i.setType("video/*");
                try {
                    startActivityForResult(i, Utils.VIDEO_GALLERRY_CHAT);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
	 */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v("chat onActivityResult", "-" + requestCode);

        if (resultCode == Activity.RESULT_OK) {

            Log.v("requestCode", "" + requestCode);

            switch (requestCode) {

                case Utils.IMAGE_GALLERRY_CHAT:
                    // handle card image
                    try {
                        Uri result3 = data.getData();
                        Utils.IMAGE_GALLERRY_CHAT_URI = result3;
                        Utils.IMAGE_GALLERRY_CHAT_PATH = ScalingUtilities.compressImage(ChatScreenActivity.this, Utils.IMAGE_GALLERRY_CHAT_URI.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (!Utils.IMAGE_GALLERRY_CHAT_PATH.equals("")) {
                        Log.e("IMAGE_GALLERRY", "" + Utils.IMAGE_GALLERRY_CHAT_PATH);
                        sendMultimediaMessage(Utils.IMAGE_GALLERRY_CHAT_PATH);
                    }

                    break;

                case GET_LOCATION_REQ: {
                    Bundle bundle = null;
                    bundle = data.getExtras();
                    if (bundle != null) {
                        Address address = (Address) bundle.getSerializable(Constants.object);
                        if (address != null) {
                            Log.d("addressafterfilter", "" + address);
                            /*callapiSendLocation(address.toString(), MyApplication.getInstance().getCurrentLatitude() + "",
                                    MyApplication.getInstance().getCurrentLongitude() + "");*/
                            sendLocationMessage(MyApplication.getInstance().getCurrentLatitude() + "", MyApplication.getInstance().getCurrentLongitude() + "");

                        }
                    }
                }
                break;
                case Utils.IMAGE_CAMERA_CHAT:
                    // handle camera image
                    try {
                        Bitmap mBitmap = Utils.handleSamplingAndRotationBitmap(ChatScreenActivity.this, fileUri);
                        Uri uri = getImageUri(ChatScreenActivity.this, mBitmap);
                        Utils.IMAGE_CAMERA_CHAT_PATH = getPath(uri);

                        if (!Utils.IMAGE_CAMERA_CHAT_PATH.equals("")) {
                            Log.e("IMAGE_CAMERA_CHAT_PATH", "-"
                                    + Utils.IMAGE_CAMERA_CHAT_PATH);
                            sendMultimediaMessage(Utils.IMAGE_CAMERA_CHAT_PATH);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case Utils.VIDEO_CAMERA_CHAT:
                    Log.v("VIDEO_CAMERA_CHAT", "1");
                    Log.e("VIDEO_CAMERA_CHAT_PATH", "-" + Utils.VIDEO_CAMERA_CHAT_PATH);

                   /* sendMultimediaMessage(Utils.VIDEO_CAMERA_CHAT_PATH, VIDEO,
                            ifGroup);*/
                    //sendMultimediaMessageVideo(Utils.VIDEO_CAMERA_CHAT_PATH);
                    break;
                case Utils.VIDEO_GALLERRY_CHAT:

                    if (null != data) {
                        Uri selectedVideo = data.getData();
                        String[] filePathColumn = {MediaStore.Video.Media.DATA};

                        Cursor cursor = context.getContentResolver().query(
                                selectedVideo, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String videoPath = cursor.getString(columnIndex);
                        cursor.close();
                        Utils.VIDEO_GALLERRY_CHAT_PATH = videoPath;

                        Log.e("VIDEO_GALLERRY", "-"
                                + Utils.VIDEO_GALLERRY_CHAT_PATH);

                        //sendMultimediaMessageVideo(Utils.VIDEO_GALLERRY_CHAT_PATH);
                    }

                    break;

                case Utils.AUDEO_GALLERRY_CHAT:

                    if (null != data) {
                        Uri audioFileUri = data.getData();
                        String[] filePathColumn = {MediaStore.Audio.Media.DATA};

                        Cursor cursor = context.getContentResolver().query(
                                audioFileUri, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String audioPath = cursor.getString(columnIndex);
                        cursor.close();

                        Utils.AUDEO_GALLERRY_CHAT_PATH = audioPath;

                        Log.e("AUDEO_GALLERRY", "-"
                                + Utils.AUDEO_GALLERRY_CHAT_PATH);

                       /* sendMultimediaMessage(Utils.AUDEO_GALLERRY_CHAT_PATH,
                                AUDIO, ifGroup);*/
                    }

                    break;

                case Utils.AUDEO_RECORD_CHAT:

                    if (null != data) {
                        Uri audioFileUri = data.getData();

                        String[] filePathColumn = {MediaStore.Audio.Media.DATA};

                        Cursor cursor = context.getContentResolver().query(
                                audioFileUri, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String audioPath = cursor.getString(columnIndex);
                        cursor.close();

                        Utils.AUDEO_GALLERRY_CHAT_PATH = audioPath;

                        Log.e("AUDEO_GALLERRY", "-"
                                + Utils.AUDEO_GALLERRY_CHAT_PATH);

                        /*sendMultimediaMessage(Utils.AUDEO_GALLERRY_CHAT_PATH,
                                AUDIO, ifGroup);*/
                    }

            }

        }

    }


    ;

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri) {

        try {
            Cursor cursor = getContentResolver().query(uri, null, null, null,
                    null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id
                    .substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null,
                    MediaStore.Images.Media._ID + " = ? ",
                    new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        } catch (Exception e) {
            try {
                Cursor cursor = getContentResolver().query(uri, null, null,
                        null, null);
                cursor.moveToFirst();
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return "";
        }
    }

    public void sendMultimediaMessage(final String imagePath) {

        if (Utils.isConnectingToInternet(this)) {
            try {
                CommandRequest(imagePath);
            } catch (Exception e) {
                System.out.println("exception.." + e.toString());
            }
        } else {
            toast(getResources().getString(R.string.network_error_msg));
        }
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd MMM yyyy", cal).toString();
        return date;
    }

    private void CommandRequest(String filepath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
        Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
        uploadFile(filepath);

    }


    private void uploadFile(String bitmap) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://meetnepali-4f1d5.appspot.com/messagePics/");
        StorageReference mountainImagesRef = storageRef.child(userId);

        /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);*/
        try {
            InputStream stream = new FileInputStream(new File(bitmap));
            UploadTask uploadTask = mountainImagesRef.putStream(stream);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    // sendMsg("" + downloadUrl, 2);
                    System.out.println("download url" + downloadUrl);
                    Log.d("downloadUrl-->", "" + downloadUrl);
                    sendpictureMessage(downloadUrl + "");
                }
            });
        } catch (Exception e) {

        }


    }

    private void sendpictureMessage(String downloadurl) {
        try {
            if (chatInfo.getFromid().startsWith("grp")) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbGroupChatTable).child(chatInfo.getFromid());
                String userId = mDatabase.push().getKey();
                String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                String toid = chatInfo.getFromid();
                Long tsLong = System.currentTimeMillis() / 1000;
                String ts = tsLong.toString();
                SendGrpMessage sendMessage = new SendGrpMessage(downloadurl, fromid, tsLong, "photo");
                mDatabase.child(userId).setValue(sendMessage);
                send_group_notification(downloadurl);
            } else {
                if (adapter.getCount() == 0) {

                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
                    String userId = mDatabase.push().getKey();
                    //  mDatabase.child(userId);

                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(userId);
                    String userId2 = mDatabase2.push().getKey();

                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    String toid = chatInfo.getFromid();

                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    SendMessage sendMessage = new SendMessage(downloadurl, fromid, true, tsLong, toid, "photo");
                    mDatabase2.child(userId2).setValue(sendMessage);


                    String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(toid).child(fbchatTable);
                    NewUser newUser = new NewUser(userId);
                    ref.child(currentUserName).setValue(newUser);

                    DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
                    NewUser newUser1 = new NewUser(userId);
                    ref1.child(toid).setValue(newUser1);

                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(chatID);
                    String userId = mDatabase.push().getKey();
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    String toid = chatInfo.getFromid();
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    SendMessage sendMessage = new SendMessage(downloadurl, fromid, true, tsLong, toid, "photo");
                    mDatabase.child(userId).setValue(sendMessage);
                }

                if (onlinestatus.compareToIgnoreCase("Offline") == 0) {
                    send_chat_notification("Image Shared");
                }

            }

        } catch (Exception e) {

        }


    }

    private void sendLocationMessage(String lat, String lng) {
        try {
            String location = lat + ":" + lng;

            if (chatInfo.getFromid().startsWith("grp")) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(chatInfo.getFromid());
                String userId = mDatabase.push().getKey();
                String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                String toid = chatInfo.getFromid();

                Long tsLong = System.currentTimeMillis() / 1000;
                String ts = tsLong.toString();
                SendGrpMessage sendMessage = new SendGrpMessage(location, fromid, tsLong, "location");
                mDatabase.child(userId).setValue(sendMessage);
                send_group_notification(location);
            } else {
                if (adapter.getCount() == 0) {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
                    String userId = mDatabase.push().getKey();
                    //  mDatabase.child(userId);

                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(userId);
                    String userId2 = mDatabase2.push().getKey();

                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    String toid = chatInfo.getFromid();

                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    SendMessage sendMessage = new SendMessage(location, fromid, true, tsLong, toid, "location");
                    mDatabase2.child(userId2).setValue(sendMessage);


                    String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(toid).child(fbchatTable);
                    NewUser newUser = new NewUser(userId);
                    ref.child(currentUserName).setValue(newUser);

                    DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
                    NewUser newUser1 = new NewUser(userId);
                    ref1.child(toid).setValue(newUser1);
                } else {

                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(chatID);
                    String userId = mDatabase.push().getKey();
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    String toid = chatInfo.getFromid();

                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    SendMessage sendMessage = new SendMessage(location, fromid, true, tsLong, toid, "location");
                    mDatabase.child(userId).setValue(sendMessage);


                }
                if (onlinestatus.compareToIgnoreCase("Offline") == 0) {
                    send_chat_notification("Location Shared");
                }

            }

        } catch (Exception e) {

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.loadImageRound(context, profileImage, chatInfo.getProfileImg());
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
        mDatabase.child(fromid).setValue(onlineStatus);
    }

    private void send_chat_notification(String message) {
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "notification_send");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("to_id", chatInfo.getFromid());
            data.put("message", message);
            data.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
            jobj.put("data", data);
            System.out.println("json data " + jobj.toString());

        } catch (Exception e) {
            Log.d("ChatScreen", "1216 - Exception :"+ e.toString());
        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(ChatScreenActivity.this, WebUrls.API_URL, object, ChatScreenActivity.this, RequestCode.Code_Sendnotification, 3);
    }

    private void send_group_notification(String message) {
        JSONObject jobj = new JSONObject();
        try {
                //add send private or public chat later.
            jobj.put("method", "private_grpnoti_send");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("grp_id", chatInfo.getFromid());
            data.put("member", chatInfo.getMember());
            data.put("message", message);
            data.put("grp_name", chatInfo.getName());
            data.put("grp_date", chatInfo.getGrpcreateddate()+"");
            data.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
            jobj.put("data", data);
            System.out.println("Group Notification Sent" + jobj.toString());

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(ChatScreenActivity.this, WebUrls.API_URL, object, ChatScreenActivity.this, RequestCode.Code_Sendnotification, 3);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == RequestCode.Code_Sendnotification) {
            if (response != null) {
                System.out.println("send notification" + response);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* if (chatInfo.getFromid().startsWith("grp") && chatInfo.getGrpisPrivate().compareTo("private") == 0) {
            getMenuInflater().inflate(R.menu.option, menu);
        }*/
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionDone:
                startActivity(new Intent(this, groupDetails.class).putExtra("chatkey", chatInfo.getFromid()));
                return true;
        }
        return true;
    }

    private class CheckIfForeground extends AsyncTask<Void, Void, Void> {
        Boolean foreground;

        @Override
        protected Void doInBackground(Void... voids) {

            ActivityManager activityManager = (ActivityManager) ChatScreenActivity.this.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i("Foreground App", appProcess.processName);

                    if (ChatScreenActivity.this.getPackageName().equalsIgnoreCase(appProcess.processName)) {
                        Log.i(Constants.TAG, "foreground true:" + appProcess.processName);
                        foreground = true;
                        // close_app();
                    }
                }
            }
            Log.d(Constants.TAG, "foreground value:" + foreground);
            if (foreground) {
                foreground = false;
                // close_app();
                Log.i(Constants.TAG, "Close App and start Login Activity:");

            } else {
                //if not foreground
                // close_app();
                foreground = false;
                Log.i(Constants.TAG, "Close App");

            }

            return null;
        }
    }
}
