package com.nixbin.MeetNepali.chat;

/**
 * This interface for handle string references of project.
 * 
 * @author Rahul Purohit
 * 
 */
public interface TAGS {

	public static final String SMSBRODCASTACTION = "com.wgchat.SMS_RECEIVED";
	public static final String SHOWERRORACTION = "com.wgchat.receiver.error";
	public static final String PREF_NAME = "wgchat.xml";
	public static final String PREF_EMAIL = "wgchat_email";
	public static final String PREF_PASSWORD = "wgchat_password";
	public static final String PREF_SID = "wgchat_sid";
	public static final String PREF_DISPLAY_NAME = "wgchat_display_name";
	public static final String PREF_DISPLAY_ADDRESS = "wgchat_display_address";
	public static final String PREF_PHONE_NUMBER = "wgchat_phone_number";
	public static final String PREF_COUNTRY_CODE = "wgchat_country_code";
	public static final String PREF_USER_IMAGE = "wgchat_user_image";
	public static final String PREF_UNIQUE_USER_ID = "wgchat_unique_user_id";
	public static final String PREF_IS_PUBLIC_PROFILE = "wgchat_is_public_profile";
	public static final String PREF_USERNAME = "wgchat_username";
	public static final String PREF_DOB = "date_of_birth";
	public static final String PREF_SHARE_DOB	= "share_dob";
	public static final String PREF_ANNIVERSARY_DATE	= "anniversary";
	public static final String PREF_SHARE_ANNIVERSARY	= "share_anniversary";
	public static final String PREF_ANNIVERSARY_TYPE	= "anniversary_type";
	public static final String PREF_SHARE_LAST_SEEN	= "share_last_seen";
	public static final String PREF_SHARE_PHOTO	= "share_photo";
	public static final String PREF_SHARE_USER_ID	= "share_userid";
	public static final String PREF_STATUS	= "status";
	public static final String PREF_COVER_IMAGE	= "cover_image";

	public static final String PREF_IS_SEARCH_NEARBY = "is_search_nearby";
	public static final String PREF_IS_VIEW_OFFLINE_FRINDS = "is_view_offline_frinds ";
	public static final String PREF_IS_VIEW_ONLINE_FRINDS = "is_view_online_frinds ";

	public static final String REPLYCODE = "ReplyCode";
	public static final String ERROR = "Error";
	public static final String SUCCESS = "Success";
	public static final String MSG = "msg";
	public static final String IS_SAVE = "is_save";
	public static final String ID = "id";
	public static final String VARIFICATINCODE = "varificatincode";
	public static final String CODE_ID = "code_id";
	public static final String isVerifiedPhone = "isVerifiedPhone";

	// USER DATA
	public static String USER_ID = "user_id";
	public static String EMAIL = "email";
	public static String DISPLAY_NAME = "display_name";
	public static String PHONE_NUMBER = "phone_number";
	public static String COUNTRY_CODE = "country_code";
	public static String USERIMAGE = "userimage";
	public static String UNIQUE_USERID = "unique_userid";
	public static String IS_PUBLIC_PROFILE = "is_public_profile";
	public static String IS_FOLLOWING = "is_following";
	public static String IS_PUBLIC_ANNIVERSARY = "is_public_anniversary";
	public static String COVER_IMAGE = "cover_image";
	public static String STATUS = "status";
	public static String DOB = "dob";
	public static String ANNIVERSARY_TYPE = "anniversary_type";
	public static String ANNIVERSARY_DATE = "anniversary_date";
	// USER DATA END

	// User Friend List
	public static String CONTACTLIST = "contactList";
	public static String FRIENDREQUEST = "friendrequest";
	public static String USERS = "Users";
	public static String USER = "Users";
	public static String USERLIST = "userList";
	public static String FRIEND_ID = "friend_id";
	public static String CHAT_USER_ID = "chat_user_id";

	public static String IS_FROMCONTACT = "is_fromcontact";
	public static String PROFILE_PICTURE = "profile_picture";
	public static String SEARCH_USER_ID = "search_user_id";
	public static String GCM_REG_ID = "gcm_reg_id";
	public static String PROFILE_IMAGE = "profile_image";
	public static String IS_FRIEND = "is_friend";
	public static String FRIEND_STATUS = "friend_status";
	public static String QR = "qr";
	public static String USERNAME = "username";

	// STATUS DATA
	public static String STATUSLIST = "statuslist";
	public static String STATUS_TEXT = "status";
	public static String STATUS_ID = "status_id";
	public static String STATUS_IS_ACTIVE = "is_active";
	// STATUS DATA END

	// POST DATA
	public static String POSTLIST = "post";
	public static String POST_ID = "id";
	public static String POST_URL = "url";
	public static String POST_META_TITLE = "meta_title";
	public static String POST_META_DESC = "meta_description";
	public static String POST_META_IMG = "meta_image";
	public static String POST_LOC_NAME = "location_name";
	public static String POST_LOC_ADD = "location_address";
	public static String POST_LAT = "latitude";
	public static String POST_LNG = "longitude";
	public static String POST_TEXT = "wallposttext";
	public static String POST_CHECKIN = "checkin_at";
	public static String POST_CREATED = "created";
	public static String POST_MEDIA_LIST = "media";
	public static String POST_COMMENT_LIST = "comments";
	public static String POST_TOTAL_LIKES = "total_likes";
	public static String POST_TOTAL_COMMENTS = "total_comments";
	public static String POST_IS_LIKE = "is_like";
	public static String POST_USER_NAME = "display_name";
	public static String POST_USER_IMG = "user_profile_image";
	public static String POST_USER_ID = "post_owner_id";
	// POST DATA END

	// MEDIA DATA
	public static String MEDIA_POST_ID = "post_id";
	public static String MEDIA_ID = "post_media_id";
	public static String MEDIA_TYPE = "media_type";
	public static String MEDIA_CONTENT = "filename";
	public static String MEDIASCREENSHOT = "mediaScreenShot";
	public static String RESIZEDFILE = "resizedFile";
	public static String CROPPEDFILE = "mediaScreenShot";

	// MEDIA DATA END

	// COMMENT DATA
	public static String COMMENT_POST_ID = "post_id";
	public static String COMMENT_ID = "post_comment_id";
	public static String COMMENT_TEXT = "comments";
	public static String COMMENT_USER_ID = "comment_user_id";
	public static String COMMENT_USER_NAME = "user_display_name";
	public static String COMMENT_USER_IMAGE = "profile_image";
	public static String COMMENT_FB_ID = "facebook_id";
	public static String COMMENT_TIME = "created";
	// COMMENT DATA END

	// COMMENT LIST DATA
	public static String LIST_COMMENT_ID = "comment_id";
	public static String LIST_COMMENTS_CREATED = "created";
	public static String LIST_COMMENTS_TEXT = "comments";
	public static String LIST_COMMENT_USER_ID = "comment_user_id";
	public static String LIST_COMMENTS_USER_NAME = "display_name";
	public static String LIST_COMMENTS_USER_IMAGE = "profile_image";
	// COMMENT LIST DATA END
	
	public static String EJABBER_USERNAME = "ejabber_username";
	public static String EJABBER_PASSWORD = "ejabber_password";

	// MESSAGE EVENTS
	public static String NEW_MESSAGE = "new_message";

	// MESSAGE PORT
	public static String FTYPE = "type";
	public static String UUID = "elementID";
	public static String TURL = "mediaUrlOriginal";
	public static String IS_UPLOADING = "isUploading";
	public static String MESSAGE = "message";
	public static String AUDIO = "audio";
	public static String IMAGE = "image";
	public static String TEXT = "";
	public static String VIDEO = "video";
	public static String CUSTOM_ID = "customid";
	public static String VIDEO_LENGTH = "videoDuration";
	public static String LOCATION = "location";
	public static String STICKER = "sticker";
	
	public static String FROM = "from";
	
	public static String STICKER_PATH = "forIphoneUser";
	public static String Filename = "filename";
	

	public static String mediaUrlTemp = "mediaUrlTemp";

	public static final String MSG_TYPE_TEXT = "text";
	public static final String MSG_TYPE_IMAGE = "image";
	public static final String MSG_TYPE_AUDIO = "audio";
	public static final String MSG_TYPE_VIDEO = "video";
	
	public static int TAG_TYPE_TEXT = 0;
	public static int TAG_TYPE_IMG = 1;
	public static int TAG_TYPE_VIDEO = 2;
	public static int TAG_TYPE_AUDIO = 3;

	public static String FILE = "file";
	
	public static String POSITION = "position";

	public static int TXT_SENT = 0;
	public static int IMG_SENT = 1;
	public static int VIDEO_SENT = 2;
	public static int AUDIO_SENT = 3;
	public static int LOC_SENT = 4;
	public static int STICK_SENT = 5;
	
	public static int TXT_RECEIVED = 10;
	public static int IMG_RECEIVED = 11;
	public static int VIDEO_RECEIVED = 12;
	public static int AUDIO_RECEIVED = 13;
	public static int LOC_RECEIVED = 14;
	public static int STICK_RECEIVED = 15;
	
	public static int HEADER_MSG = 20;
	
	
	public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;
    

	public static String OTR_ENABLE = "otr_enable";
	public static String OTR_DISABLE = "otr_disable";

}
