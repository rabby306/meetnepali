package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.google.gson.Gson;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.FriendListAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by manu on 6/8/2017.
 */

public class FriendList extends BaseFragment implements WebCompleteTask {
    public static String TAG = "FriendList";
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.empty_view)
    TextView empty_view = null;
    @Bind(R.id.ttle)
    TextView ttle = null;
    RequestQueue queue;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    ArrayList<Bindcontract> feedsListall1 = new ArrayList();
    FriendListAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    public final static int reqFilter = 102;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String serviceid = "";
    ActionMenuView bottomBar;

    SwipeRefreshLayout swipeRefreshLayout;
    String url;
    @Bind(R.id.search)
    EditText search;
    Boolean isvisile=false;

    public static FriendList getInstance(Bundle bundle) {
        FriendList chef = new FriendList();
        chef.setArguments(bundle);
        return chef;
    }

    public FriendList() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    boolean visible=false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getUserVisibleHint()) {
            visible = true;

        } else {
            visible = false;

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_myfriends, container, false);
        ButterKnife.bind(this, view);

       final FloatingActionButton myFab = (FloatingActionButton) view.findViewById(R.id.addFriendBtn);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                replaceFragment(R.id.container, new FragmentFindFriend());

               // myFab.setVisibility(view.GONE);

            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                filter(text);
            }
        });

        initView(view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        return view;
    }

    protected void initView(final View view) {


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        get_my_friends_list();

        /*ivFavTab = (Button) view. findViewById(R.id.iv_fav_tab);
        cashout = (Button) view. findViewById(R.id.cashout);
        money = (Button) view. findViewById(R.id.money);
        money1 = (Button) view. findViewById(R.id.money1);
        ivFavTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeClick(view);
            }
        });
        cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostProperty(view);
            }
        });
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileClick(view);
            }
        });
        money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileWebClick(view);
            }
        });*/
        //callapiAllDemo();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;

        adapterall.clearList();
        get_my_friends_list();
    }

    @Override
    public void onStart() {
        super.onStart();
        isRefreshing = true;

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void get_my_friends_list() {
        Log.d(TAG, "274 - Calling Api");
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "my_friends");
            jobj.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));

            jobj.put("data", data);

        } catch (Exception e) {
            Log.d(TAG, "285 - Exception Calling API");
        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        Log.w("Rabby",  " JSON: " + new Gson().toJson(object));
        new WebTask(getActivity(), WebUrls.API_URL, object, FriendList.this, RequestCode.CODE_Myfriend, 1);


    }

    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        swipeRefreshLayout.setRefreshing(false);
        try {
            Log.d(TAG, "Hmm");
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_Myfriend) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONObject outerObj = new JSONObject(object.getString("data"));
                    JSONArray data = null;
                    data = outerObj.getJSONArray("friend_already");
                    ttle.setText("Total: "+data.length()+" friends");
                    JSONObject jobj = null;
                    feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        Bindcontract contract = new Bindcontract();
                        jobj = data.getJSONObject(i);


                        contract.setFriendid(jobj.getInt("friend_id")+"");
                        contract.setProfileImg(WebUrls.BASE_URL_image + jobj.getString("profile_img"));
                        contract.setName(jobj.getString("first_name"));
                        contract.setEmail(jobj.getString("email"));
                        contract.setFromid(jobj.getInt("friend_id")+"");
                        contract.setOtherid(jobj.getInt("Id")+"");
                        contract.setChattype("single");

                        feedsListall.add(contract);
                        empty_view.setVisibility(View.GONE);

                    }
                    //  empty_view.setVisibility(View.VISIBLE);
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }

    }



    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {

            empty_view.setVisibility(View.VISIBLE);
        } else {
            empty_view.setVisibility(View.GONE);
            adapterall = new FriendListAdapter(getActivity(), feedsListall);
            recycleView.setAdapter(adapterall);
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        // map.clear();
        if (charText.length() == 0) {
            if (feedsListall.size() == 0) {
                empty_view.setVisibility(View.VISIBLE);
            } else {
                get_my_friends_list();
                empty_view.setVisibility(View.GONE);
            }

        } else {
            feedsListall1.clear();
            for (Bindcontract wp : feedsListall) {
                if (wp.getName().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    Bindcontract wrapper = new Bindcontract();

                    wrapper.setFriendid(wp.getFriendid());
                    wrapper.setProfileImg(wp.getProfileImg());
                    wrapper.setName(wp.getName());
                    wrapper.setEmail(wp.getEmail());
                    wrapper.setFromid(wp.getFromid());
                    feedsListall1.add(wrapper);

                }
                empty_view.setVisibility(View.GONE);
                recycleView.setAdapter(new FriendListAdapter(getActivity(), feedsListall1));
                new FriendListAdapter(getActivity(), feedsListall1).notifyDataSetChanged();
            }

        }


    }
}

