package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.FriendRequestReceivedListAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by manu on 6/8/2017.
 */

public class FriendRequestReceivedList extends BaseFragment implements WebCompleteTask {
    public static String TAG = "Udo";
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.empty_view)
    TextView empty_view = null;
    RequestQueue queue;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    FriendRequestReceivedListAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    public final static int reqFilter = 102;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String serviceid = "";
    ActionMenuView bottomBar;


    String url;
    @Bind(R.id.search)
    EditText search;
    @Bind(R.id.bottom)
    RelativeLayout bottom;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    public static FriendRequestReceivedList getInstance(Bundle bundle) {
        FriendRequestReceivedList chef = new FriendRequestReceivedList();
        chef.setArguments(bundle);
        return chef;
    }

    public FriendRequestReceivedList() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_chat_request, container, false);
        ButterKnife.bind(this, view);
        initView(view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        return view;
    }

    protected void initView(final View view) {

        //for crate home button
        search.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;

        adapterall.clearList();
        callapiAll();
    }

    @Override
    public void onStart() {
        super.onStart();
        isRefreshing = true;

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void callapiAll() {
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", "my_friends");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(getActivity(), WebUrls.API_URL, object, FriendRequestReceivedList.this, RequestCode.CODE_Myfriend, 1);

    }

    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        swipeRefreshLayout.setRefreshing(false);
        try {
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_Myfriend) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONObject outerObj = new JSONObject(object.getString("data"));
                    JSONArray data = null;
                    data = outerObj.getJSONArray("friend_sentrequest");

                    JSONObject jobj = null;
                    feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        Bindcontract contract = new Bindcontract();
                        jobj = data.getJSONObject(i);

                        contract.setFriendid(jobj.getInt("friend_id") + "");
                        contract.setName(jobj.getString("first_name"));
                        contract.setEmail(jobj.getString("email"));
                        contract.setProfileImg(jobj.getString("profile_img"));


                        feedsListall.add(contract);
                        empty_view.setVisibility(View.GONE);

                    }
                    //  empty_view.setVisibility(View.VISIBLE);
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }

    }


    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            empty_view.setVisibility(View.VISIBLE);
        } else {
            empty_view.setVisibility(View.GONE);
            adapterall = new FriendRequestReceivedListAdapter(getActivity(), feedsListall);
            recycleView.setAdapter(adapterall);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}

