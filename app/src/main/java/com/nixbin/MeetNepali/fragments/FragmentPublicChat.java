package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.SelectMember;
import com.nixbin.MeetNepali.adpter.ChatAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbGroupChatTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupListTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbOnlineStatusTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;

/**
 * Created by manu on 6/8/2017.
 */

public class FragmentPublicChat extends BaseFragment {
    public static String TAG = "fragmentPublicChat";
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.empty_view)
    TextView empty_view = null;
    RequestQueue queue;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    ArrayList<Bindcontract> feedsListall1 = new ArrayList();
    ChatAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    public final static int reqFilter = 102;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String serviceid = "";
    ActionMenuView bottomBar;
    TextView wait;
    ArrayList<String> conversation_key = new ArrayList<>();
    ArrayList<String> conversation_message = new ArrayList<>();
    @Bind(R.id.search)
    EditText etSearchUser;

    private DatabaseReference databaseReference;
    String url;
    Boolean isvisile = false;

    ArrayList<String> profilepic = new ArrayList<>();
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> last_msg = new ArrayList<>();
    ArrayList<String> timestamp = new ArrayList<>();
    ArrayList<String> status = new ArrayList<>();
    ArrayList<String> last_msg_type = new ArrayList<>();
    ArrayList<Integer> timestampcomp = new ArrayList<>();
    ArrayList<String> chattype = new ArrayList<>();
    ArrayList<Integer> grpcreated = new ArrayList<>();
    ArrayList<String> member_list = new ArrayList<>();
    HashMap<Integer, String> hm = new HashMap<Integer, String>();
    ArrayList<Integer> creator_id = new ArrayList<>();
    ArrayList<HashMap<Integer, String>> com_arr = new ArrayList<>();

    public static FragmentPublicChat getInstance(Bundle bundle) {
        FragmentPublicChat chef = new FragmentPublicChat();
        chef.setArguments(bundle);
        return chef;
    }

    public FragmentPublicChat() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    boolean visible = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            visible = true;

        } else {
            visible = false;

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.chat_grouplist, container, false);

        ButterKnife.bind(this, view);
        disableBackButton();
        feedsListall.clear();
        conversation_key.clear();
        conversation_message.clear();
        profilepic.clear();
        name.clear();
        last_msg.clear();
        timestamp.clear();
        status.clear();
        last_msg_type.clear();
        timestampcomp.clear();
        chattype.clear();
        grpcreated.clear();
        member_list.clear();
        creator_id.clear();

        FloatingActionButton addPublicChat = (FloatingActionButton) view.findViewById(R.id.addChatBtn);
        addPublicChat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                replaceFragment(R.id.container, new HomepageNew());
                /*Intent intent = new Intent(getActivity(), SelectMember.class);
                startActivity(intent);*/
            }
        });


        etSearchUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = etSearchUser.getText().toString().toLowerCase(Locale.getDefault());
                filter(text);
            }
        });

        initView(view);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        return view;
    }

    protected void initView(final View view) {
        loadProgressBar("Please Wait", getString(R.string.api_hiting), false);

        wait = (TextView) view.findViewById(R.id.wait);
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //callapiAll();
        wait.setVisibility(View.GONE); //remove later
        loadProgressBar("Please Wait", getString(R.string.api_hiting), false);
        empty_view.setVisibility(View.GONE);


        // String currentUserName = MyApplication.getUserID(Constants.UserID);
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
        //  DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable);

        /*ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println("public group key " + dataSnapshot.getKey());
                try {
                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();

                    JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());

                    System.out.println("public group value" + jsonObject1.getString("name"));
                    if (jsonObject1.getString("isPrivate").compareTo("public") == 0) {
                        conversation_key.add(dataSnapshot.getKey());
                        conversation_message.add(dataSnapshot.getKey());

                        profilepic.add(jsonObject1.getString("group_icon"));
                        name.add(jsonObject1.getString("name"));
                        grpcreated.add(jsonObject1.getInt("timestamp"));
                        member_list.add(jsonObject1.optString("member"));
                    }


                } catch (Exception e) {

                }


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //  cities.add(postSnapshot.getValue().toString());
                   *//* System.out.println("public group data " + postSnapshot.getValue().toString());
                    conversation_key.add(dataSnapshot.getKey());
                    conversation_message.add(postSnapshot.getValue().toString());*//*

                }
                getLastMessage();
                // getuserinfomathod();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
        ref.orderByChild("isPrivate").equalTo("public").limitToLast(MyApplication.MAX_PUBLIC_CHAT_LIST).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));

                    Log.d(TAG, " 311 - Chat:" + value.toString());
                          /*group information*/
                    final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                    Log.d(TAG, "316 - Json Object: " + jsonObject1.toString());
                    Iterator<String> keys = jsonObject1.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        Log.d(TAG, "Inside has next , line 320");
                        conversation_key.add(key);
                        conversation_message.add(key);

                        try {
                            JSONObject value1 = jsonObject1.getJSONObject(key);

                            profilepic.add(value1.getString("group_icon"));
                            name.add(value1.getString("name"));
                            grpcreated.add(value1.getInt("timestamp"));
                            member_list.add(value1.optString("members"));
                            creator_id.add(value1.getInt("creator"));

                            Log.d(TAG, "330 - Key " + key + " fragment public chat" + value1.toString());

                        } catch (Exception e) {
                            Log.d(TAG, "335 - " +  e.getMessage());
                        }
                        if (conversation_key.size() == 10) {

                            Log.d(TAG, "339 - Breaking here why ? ");
                            break;

                        }

                    }

                    dismissProgressBar();
                    getLastMessage();
                } catch (Exception e) {
                    Log.d(TAG, "Exception No public Chat");
                    wait.setText("No Group Chat Found, You can start one now!");
                    wait.setVisibility(View.VISIBLE);
                    setList();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Log.d(TAG, "Conversation key size: " + conversation_key.size());
        if (conversation_key.size() == 0) {
            dismissProgressBar();
            setList();
            Log.d(TAG, "No Public Chat Found");


        }
        else
        {
            Log.d(TAG, "Not setting list");
        }



        /*ivFavTab = (Button) view. findViewById(R.id.iv_fav_tab);
        cashout = (Button) view. findViewById(R.id.cashout);
        money = (Button) view. findViewById(R.id.money);
        money1 = (Button) view. findViewById(R.id.money1);
        ivFavTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeClick(view);
            }
        });
        cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostProperty(view);
            }
        });
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileClick(view);
            }
        });
        money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileWebClick(view);
            }
        });*/
        //callapiAllDemo();

    }


    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }


    private void getuserinfomathod() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};
        profilepic.clear();
        name.clear();

        for (int i = 0; i < conversation_key.size(); i++) {
            //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
            if (conversation_key.get(i).startsWith("grp")) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(conversation_key.get(i));
                profilepic.add("0");
                name.add("0");
                grpcreated.add(0);

                final int finalI = i;
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        try {
                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                            data[0] = new JSONObject(value).toString();

                          /*group information*/
                            final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                            //    profilepic.add(jsonObject.getString("profilePicLink"));
                            // name.add(jsonObject.getString("name"));

                            profilepic.set(finalI, jsonObject1.getString("group_icon"));
                            name.set(finalI, jsonObject1.getString("name"));
                            grpcreated.set(finalI, jsonObject1.getInt("timestamp"));

                            /*create group*/

                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(jsonObject1.getString("creator")).child("meet_credentials");
                            //  profilepic.add("0");
                            // name.add("0");

                            // final int finalI = i;
                            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                                    // data[0] = dataSnapshot.getValue().toString();
                                    try {
                                        Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                                        data[0] = new JSONObject(value).toString();


                                        JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                                        //    profilepic.add(jsonObject.getString("profilePicLink"));
                                        // name.add(jsonObject.getString("name"));

                                        //  profilepic.set(finalI, jsonObject1.getString("group_icon"));
                                        // name.set(finalI, jsonObject.getString("name"));


                                    } catch (Exception e) {

                                    }

                                    if (finalI == conversation_key.size() - 1) {
                                        getonlinestatusmathod();
                                    }



               /* try {
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                } catch (Exception e) {

                }*/


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                        } catch (Exception e) {
                            Log.d(TAG, "543 - Exception : " + e.toString());
                        }

                        if (finalI == conversation_key.size() - 1) {
                            getonlinestatusmathod();
                        }



               /* try {
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                } catch (Exception e) {

                }*/


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(conversation_key.get(i)).child("meet_credentials");
                profilepic.add("0");
                name.add("0");
                grpcreated.add(0);

                final int finalI = i;
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        try {
                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                            data[0] = new JSONObject(value).toString();


                            JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                            //    profilepic.add(jsonObject.getString("profilePicLink"));
                            // name.add(jsonObject.getString("name"));

                            profilepic.set(finalI, jsonObject.getString("profilePicLink"));
                            name.set(finalI, jsonObject.getString("name"));
                            grpcreated.set(finalI, 0);


                        } catch (Exception e) {
                            Log.d(TAG, "603 - Exception : " + e.toString());
                        }

                        if (finalI == conversation_key.size() - 1) {
                            getonlinestatusmathod();
                        }



               /* try {
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                } catch (Exception e) {

                }*/


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }


        }


    }

    private void getonlinestatusmathod() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};
        status.clear();

        for (int i = 0; i < conversation_key.size(); i++) {
            status.add("0");
            System.out.println("conversition key" + conversation_key.get(i));


            //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(conversation_key.get(i));


            final int finalI = i;
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                    // data[0] = dataSnapshot.getValue().toString();
                    try {
                        Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                        // Log.i("online status", "dataSnapshot online status" + new JSONObject(value));
                        data[0] = new JSONObject(value).toString();


                        JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                        // status.add(jsonObject.getString("status"));

                        status.set(finalI, jsonObject.getString("status"));


                    } catch (Exception e) {

                    }

                    if (finalI == conversation_key.size() - 1) {
                        getLastMessage();
                    }


              /*  try{
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                }catch (Exception e){

                }*/


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void getLastMessage() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};

        last_msg.clear();
        timestamp.clear();
        last_msg_type.clear();
        timestampcomp.clear();
        chattype.clear();

        for (int i = 0; i < conversation_key.size(); i++) {

            last_msg.add("0");
            timestamp.add("0");
            last_msg_type.add("0");
            timestampcomp.add(0);
            chattype.add("0");

            //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
           /* DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(conversation_message.get(i));


            final int finalI = i;
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    System.out.println("last message data key for getdata " + dataSnapshot.getValue().toString());
                    // data[0] = dataSnapshot.getValue().toString();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        //  cities.add(postSnapshot.getValue().toString());
                        System.out.println("lastmsg data " + postSnapshot.getValue().toString());

                        Map<String, String> value = (Map<String, String>) postSnapshot.getValue();
                        Log.i("last message json", "dataSnapshot last message json" + new JSONObject(value));
                        data[0] = new JSONObject(value).toString();

                        try {
                            JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                            String content = jsonObject.getString("content");
                            String timestamp_str = jsonObject.getString("timestamp");
                            Timestamp stamp = new Timestamp(Integer.parseInt(timestamp_str));
                            long sta = stamp.getTime();
                            String ds = getDate(sta);
                            //  long timestamptime = stamp.getTime()-Integer.parseInt(timestamp_str);

                            last_msg.add(content);
                            timestamp.add(ds);

                            System.out.println("contentdata " + content + " timestamp " + ds);


                        } catch (Exception e) {

                        }

                        break;

                    }

                    if (finalI == conversation_key.size() - 1) {
                        getChatData();
                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });*/

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupChatTable).child(conversation_message.get(i));

            final int finalI = i;
            ref.limitToLast(MyApplication.MAX_PUBLIC_CONVERSATION_LIST).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    try {
                        System.out.println("conversition data " + dataSnapshot.getValue());
                        System.out.println("conversition data key " + dataSnapshot.getKey());
                        // data[0] = dataSnapshot.getValue().toString();


                        Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                        Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));

                        JSONObject jsonObject = new JSONObject(value1);
                        String content = jsonObject.getString("content");
                        String timestamp_str = jsonObject.getString("timestamp");

                        //  long timestamptime = stamp.getTime()-Integer.parseInt(timestamp_str);

                        last_msg.set(finalI, content);
                        timestamp.set(finalI, timestamp_str);
                        last_msg_type.set(finalI, jsonObject.getString("type"));
                        timestampcomp.add(Integer.parseInt(timestamp_str));
                        chattype.set(finalI, "grp");
                        /*last_msg.add(content);
                        timestamp.add(ds);
                        last_msg_type.add(jsonObject.getString("type"));*/
                        getChatData();
                        /*if (finalI == conversation_key.size() - 1) {

                        }*/


                    } catch (Exception e) {
                        Log.d(TAG, "819 - Got Exception : "+e.toString());

                    }

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }

    }


    private void getChatData() {
        feedsListall.clear();

        try {


            Log.d(TAG, " 858 - Conversation Key Size " + conversation_key.size());

            for (int i = 0; i < conversation_key.size(); i++) {

                Bindcontract contract = new Bindcontract();
                contract.setName(name.get(i));
                contract.setStatus("");
                contract.setProfileImg(profilepic.get(i));
                contract.setLastmsg(last_msg.get(i));
                contract.setTimestamp(timestamp.get(i));
                contract.setChatkey(conversation_message.get(i));
                contract.setFromid(conversation_key.get(i));
                contract.setLastmsgtype(last_msg_type.get(i));
                contract.setChattype(chattype.get(i));
                contract.setGrpcreateddate(grpcreated.get(i));
                contract.setGrpisPrivate("public");
                contract.setMember(member_list.get(i));
                contract.setCreatorID(creator_id.get(i));

                feedsListall.add(contract);
            }

            Collections.sort(feedsListall, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    Bindcontract p1 = (Bindcontract) o1;
                    Bindcontract p2 = (Bindcontract) o2;
                    return p2.getTimestamp().compareToIgnoreCase(p1.getTimestamp());
                }
            });

            //getDataNew();
            setList();
            dismissProgressBar();
        } catch (Exception e) {
            Log.d(TAG, "892 - Exception " + e.toString());

        }


    }


    public void setList() {
        Log.d(TAG, " 901 -  service list " + feedsListall.size());
        //wait.setVisibility(View.GONE);
        if (feedsListall.size() == 0) {
            wait.setVisibility(View.VISIBLE);
        } else {
            wait.setVisibility(View.GONE);
            empty_view.setVisibility(View.GONE);

        }
        adapterall = new ChatAdapter(getActivity(), feedsListall); //amit changed - moved these two lines outside else
        recycleView.setAdapter(adapterall);
    }


    class MyComparator implements Comparator {
        Map map;

        public MyComparator(Map map) {
            this.map = map;
        }

        @Override
        public int compare(Object o1, Object o2) {
            return (o1.toString()).compareTo(o2.toString());
        }
    }

    class ReversedOrdering implements Comparator<Integer> {
        public int compare(Integer lhs, Integer rhs) {
            // compare reversed
            return rhs.compareTo(lhs);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

        /*MenuItem search = menu.findItem(R.id.searchFood);
        search.setVisible(false);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEE MMM dd hh:mm:ss z yyyy", cal).toString();
        return date;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        // map.clear();
        if (charText.length() == 0) {
            if (feedsListall.size() == 0) {
                empty_view.setVisibility(View.VISIBLE);
            } else {
                getChatData();
                empty_view.setVisibility(View.GONE);
            }

        } else {
            feedsListall1.clear();
            for (Bindcontract wp : feedsListall) {
                if (wp.getName().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    Bindcontract wrapper = new Bindcontract();

                    wrapper.setName(wp.getName());
                    wrapper.setStatus(wp.getStatus());
                    wrapper.setProfileImg(wp.getProfileImg());
                    wrapper.setLastmsg(wp.getLastmsg());
                    wrapper.setTimestamp(wp.getTimestamp());
                    wrapper.setChatkey(wp.getChatkey());
                    wrapper.setFromid(wp.getFromid());
                    wrapper.setLastmsgtype(wp.getLastmsgtype());
                    wrapper.setChattype(wp.getChattype());
                    wrapper.setGrpcreateddate(wp.getGrpcreateddate());
                    wrapper.setGrpisPrivate(wp.getGrpisPrivate());
                    wrapper.setMember(wp.getMember());
                    wrapper.setCreatorID(wp.getCreatorID());
                    feedsListall1.add(wrapper);

                }
                empty_view.setVisibility(View.GONE);
                recycleView.setAdapter(new ChatAdapter(getActivity(), feedsListall1));
                new ChatAdapter(getActivity(), feedsListall1).notifyDataSetChanged();
            }

        }


    }
}

