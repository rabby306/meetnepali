package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.SearchAddressActivity;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.views.ValidationClass;
import com.nixbin.MeetNepali.webutility.ApiConfig;
import com.nixbin.MeetNepali.webutility.AppConfig;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.EndRoute;
import com.nixbin.MeetNepali.wrapper.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;

/**
 * Created by advosoft on 9/8/2017.
 */

public class FragmentUpdateProfile extends BaseFragment {
    public static String TAG = "Update Profile";
    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.edit_image)
    ImageView editImage;

    @Bind(R.id.name)
    EditText name;

    @Bind(R.id.dob)
    EditText dob;

    @Bind(R.id.email)
    EditText email;

    @Bind(R.id.mobile)
    EditText mobile;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.update)
    Button update;

    @Bind(R.id.datepick)
    ImageView datePicker;

    //@Bind(R.id.profile_pic_drawer)
    //ImageView profileImageDrawer;

    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    String filepath = "";
    ProgressDialog dialog1;
    String dateOfBirth = MyApplication.getAuthToken(Constants.UserNameDOB);
    private double latitude;
    private double longitude;
    String profilePicFB;

    public static FragmentUpdateProfile getInstance(Bundle bundle) {
        FragmentUpdateProfile chef = new FragmentUpdateProfile();
        chef.setArguments(bundle);
        return chef;
    }

    public FragmentUpdateProfile() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() { }

    @Override
    protected void setListener() { }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.update_profile, container, false);
        ButterKnife.bind(this, view);
        disableBackButton();
        setHasOptionsMenu(true);
        initView(view);
        email.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(false);
                return false;
            }
        });
        return view;
    }

    protected void initView(final View view) {
        dialog1 = new ProgressDialog(getActivity());
        name.setText(MyApplication.getUserName(Constants.UserName));
        email.setText(MyApplication.getUserEmail(Constants.UserEmail));
        mobile.setText(MyApplication.getUserMobile(Constants.UserMobile));
        dob.setText(MyApplication.getUserDOB(Constants.UserNameDOB));
        address.setText(MyApplication.getUserLocation(Constants.UserLocation));
        Utils.loadImageRound(getActivity(), profileImage,WebUrls.BASE_URL_image+ MyApplication.getProfileImage(getActivity(),Constants.name));

        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "date picker click ", Toast.LENGTH_SHORT).show();
                showDatePicker();

            }
        });

        Log.d(TAG, "Screen Loading");
        getUserNameAndPictureFromFirebase();

        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageTypePost();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_profile();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void showDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, (month));
                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                dob.setText(sdf.format(calendar.getTime()));

                dateOfBirth = String.valueOf(day_of_month) + "-" + String.valueOf(month +1)  + "-" + String.valueOf(year);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
        dialog.show();





        /*DatePickerFragment date = new DatePickerFragment();
        *//**
         * Set Up Current Date Into dialog
         *//*
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);


        *//**
         * Set Call back to capture selected date
         *//*

        date.setCallBack(ondate);

        date.show(getActivity().getFragmentManager(), "jknkcdcdc");*/

    }
    //not sure when this is triggered.
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

/*
            dob.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1)
                    + "/" + String.valueOf(year));
            dateOfBirth = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year);*/
            //amit fix for above was change month
            dob.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year));
            dateOfBirth = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year);
            Log.d(TAG, "Date:"+ dateOfBirth);
        }
    };



    public void uploadImageTypePost() {
        setimagepath();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle("Choose option");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select image from ")
                .setCancelable(false)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(getActivity().getFilesDir(), fileName);
        }
    }

    private void openGallery() {

        /* Intent chooseIntent = new Intent(Intent.ACTION_PICK,
         MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
         chooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
         startActivityForResult(chooseIntent, REQUEST_CODE_GALLERY);*/

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);

        // startActivity(new Intent(ContainerDetailActivity.this,
        // GalleryActivity.class));

    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(getActivity(), data.getData());

                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    //  profileImage.setImageBitmap(resized);
                    filepath = getPath(getActivity(), getImageUri(getActivity(), resized));
                    // Utils.loadImageRound(getActivity(),profileImage,filepath);

                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
              //  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(getActivity(), profileImage,"file://" + mFileTemp.getPath());

                // MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {

                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                //filepath= getPath(data.getData());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filepath = getPath(getActivity(), getImageUri(getActivity(), resized));
              ///  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(getActivity(), profileImage,"file://" + mFileTemp.getPath());
                //   profileImage.setImageBitmap(resized);
            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        } else if (requestCode == Constants.LOCATION) {
            try {
                Bundle bundle = data.getExtras();
                EndRoute route = (EndRoute) bundle.getSerializable(Constants.object);
                latitude = route.getLat();
                longitude = route.getLng();
                address.setText(route.getAddress());
            } catch (Exception e) {

            }

        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void update_profile() { //update profile

        Log.d(TAG, "Update Clicked");
        JSONObject jobj = new JSONObject();
        String email_id = email.getText().toString();
        String name_user = name.getText().toString();
        if (TextUtils.isEmpty(name_user)) {
            name.setError(getString(R.string.name_validation));
            name.requestFocus();
        } else if (TextUtils.isEmpty(email_id)) {
            email.setError(getString(R.string.notempty));
            email.requestFocus();
        } else if (!ValidationClass.validateEmail(email_id)) {
            email.setError(getString(R.string.email_validation));
            email.requestFocus();
        } else if (mobile.getText().toString().compareTo("")!=0&&mobile.getText().toString().length()<8){
            mobile.setError(getString(R.string.mobile_validation));
            mobile.requestFocus();
        }else {
            if (Utils.isConnectingToInternet(getActivity())) {
                try {
                    dialog1.show();
                    dialog1.setMessage("please wait");
                    jobj.put("method", "update_profile");
                    JSONObject data = new JSONObject();
                    data.put("user_id", MyApplication.getUserID(Constants.UserID));
                    data.put("name", name.getText().toString());
                    data.put("dob", dateOfBirth);
                    Log.d(TAG,"SEnd Date:"+ dateOfBirth);
                    data.put("phone", mobile.getText().toString());
                    data.put("lat", latitude);
                    data.put("lng", longitude);
                    data.put("address", address.getText().toString().trim());
                    data.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
                    jobj.put("data", data);

                    //  if (filepath.compareTo("") == 0) {
                    System.out.println("file path " + filepath);
                    MultipartBody.Part fileToUpload2 = null;
                    File file1 = new File(filepath);
                    RequestBody requestBody2 = RequestBody.create(MediaType.parse("***/*//*//**//*"), file1);
                    fileToUpload2 = MultipartBody.Part.createFormData("profile_img", file1.getName(), requestBody2);

                    if (filepath.compareTo("") != 0) {

                        RequestBody name_str = RequestBody.create(MediaType.parse("text/plain"), jobj.toString().trim());
                        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                        if (fileToUpload2 != null) {
                            Call<JsonObject> call = getResponse.Updatepost(fileToUpload2, name_str);
                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    dialog1.cancel();
                                    if (response.isSuccessful()) {
                                        try {
                                            System.out.println("update data " + response.body().toString());
                                            JSONObject obj = new JSONObject(response.body().toString());
                                            JSONObject obj1 = new JSONObject(obj.getString("data"));
                                            MyApplication.setUserName(Constants.UserName, obj1.getString("name"));
                                            MyApplication.setUserEmail(Constants.UserLocation, obj1.getString("address"));
                                            MyApplication.setUserMobile(Constants.UserMobile, obj1.getString("phone"));
                                            MyApplication.setUserDOB(Constants.UserNameDOB, obj1.getString("dob"));
                                            MyApplication.setProfileImage(getActivity(), obj1.getString("profile_img"));
                                            setUserValue(WebUrls.BASE_URL_image+obj1.getString("profile_img") + "");
                                           // CommandRequest(filepath);
                                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            DateFormat target_date = new SimpleDateFormat("yyyy/MM/dd");
                                            Log.d(TAG, "Saving Profile to Local DB");
                                            //Didn't work
                                            //Utils.loadImageRound(getContext(), profileImageDrawer, WebUrls.BASE_URL_image+ MyApplication.getProfileImage(getContext(), Constants.name));

                                           //amit commented on feb 27 no need to do this bullshit.
                                            /* try {
                                                Date startDate = df.parse(obj1.getString("dob"));
                                                String newDateString = df.format(startDate);
                                                String readReminderdate = target_date.format(startDate);
                                                Log.d(TAG, "Inside Date formatting.");
                                                System.out.println("date formate change" + newDateString);
                                                System.out.println("new date formate " + readReminderdate);
                                                System.out.println("date formate change" + newDateString);
                                                MyApplication.setUserDOB(Constants.UserNameDOB, readReminderdate);

                                            } catch (ParseException e) {
                                                Log.d(TAG, "Error processing date");
                                                e.printStackTrace();
                                            } */
                                            Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (JSONException je) {
                                            Log.d(TAG, je.toString());
                                        }


                                    } else {
                                        System.out.println(" request error" + response.errorBody());
                                        dialog1.cancel();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    dialog1.cancel();
                                    Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                                    System.out.println("fail response.." + t.toString());
                                }
                            });
                        }
                    }
                    // }
                    else {

                        System.out.println("update json " + jobj.toString());
                        RequestBody name_str = RequestBody.create(MediaType.parse("text/plain"), jobj.toString().trim());
                        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

                        Call<JsonObject> call = getResponse.Updatepostdata(name_str);
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                dialog1.cancel();
                                if (response.isSuccessful()) {
                                    try {
                                        System.out.println("update data " + response.body().toString());
                                        JSONObject obj = new JSONObject(response.body().toString());
                                        JSONObject obj1 = new JSONObject(obj.getString("data"));
                                        MyApplication.setUserName(Constants.UserName, obj1.getString("name"));
                                        MyApplication.setUserEmail(Constants.UserLocation, obj1.getString("address"));
                                        MyApplication.setUserMobile(Constants.UserMobile, obj1.getString("phone"));
                                        MyApplication.setProfileImage(getActivity(), obj1.getString("profile_img"));
                                        MyApplication.setUserDOB(Constants.UserNameDOB, obj1.getString("dob"));

                                        //amit commented on 27th feb, no need for this bullshit.
                                        /*
                                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        DateFormat target_date = new SimpleDateFormat("yyyy/MM/dd");
                                        try {
                                            Date startDate = df.parse(obj1.getString("dob"));
                                            String newDateString = df.format(startDate);
                                            String readReminderdate = target_date.format(startDate);

                                            System.out.println("date formate change" + newDateString);
                                            System.out.println("new date formate " + readReminderdate);
                                            System.out.println("date formate change" + newDateString);
                                            MyApplication.setUserDOB(Constants.UserNameDOB, readReminderdate);
                                            setUserValue(WebUrls.BASE_URL_image+obj1.getString("profile_img") + "");
                                         //   setUserValueold(profilePicFB);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }*/
                                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException je) {

                                    }


                                } else {
                                    System.out.println(" " + response.errorBody());
                                    dialog1.cancel();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                dialog1.cancel();
                                Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                                System.out.println("fail response.." + t.toString());
                            }
                        });

                    }
                } catch (Exception e) {
                    System.out.println("exception.." + e.toString());
                }
            } else {
                toast(getResources().getString(R.string.internet_issue));
            }
        }
    }

    @OnClick(R.id.address)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address:
                startActivityForResult(new Intent(getActivity(), SearchAddressActivity.class), Constants.LOCATION);
                break;
        }
    }

    private void getUserNameAndPictureFromFirebase() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child("meet_credentials");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                //Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                //data[0] = new JSONObject(value).toString(); //commented for exception

                try {
                    data[0] = new JSONObject(value).toString();
                    JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));
                    profilePicFB = jsonObject.getString("profilePicLink");
                } catch (Exception e) {
                    Log.d("Update Profile", "662-Exception: " + e.toString());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    private void uploadFile(Bitmap bitmap) {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://meetnepali-4f1d5.appspot.com/usersProfilePics/");
        StorageReference mountainImagesRef = storageRef.child(userId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                // sendMsg("" + downloadUrl, 2);
                System.out.println("download url" + downloadUrl);
                Log.d("downloadUrl-->", "" + downloadUrl);
                setUserValue(downloadUrl + "");
            }
        });

    }

    private void setUserValue(String downloadurl) {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName);

        User user = new User(name.getText().toString(),  downloadurl);
        mDatabase.child("meet_credentials").setValue(user);
    }






    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
//////////////////////////////////////////////END//////////////////////////////////////////////////

    //unused method for reference only..
    public String getPath(Uri uri) {

        try {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null,
                    null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id
                    .substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getActivity().getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null,
                    MediaStore.Images.Media._ID + " = ? ",
                    new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        } catch (Exception e) {
            try {
                Cursor cursor = getActivity().getContentResolver().query(uri, null, null,
                        null, null);
                cursor.moveToFirst();
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return "";
        }
    }

    private void CommandRequest(String filepath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
        Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
        uploadFile(bitmap);

    }

    private void setUserValueold(String downloadurl) {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName);

        User user = new User(name.getText().toString(), downloadurl);
        mDatabase.child("meet_credentials").setValue(user);
    }

}
