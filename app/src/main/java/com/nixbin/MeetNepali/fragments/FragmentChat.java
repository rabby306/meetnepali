package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.ChatAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbGroupChatTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupListTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbOnlineStatusTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbchatTable;

/**
 * Created by manu on 6/8/2017.
 */

public class FragmentChat extends BaseFragment {
    public static String TAG = "chatfragment";
    @Bind(R.id.recycleView)
    RecyclerView recycleView;

    @Bind(R.id.empty_view)
    TextView empty_view = null;
    RequestQueue queue;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    ArrayList<Bindcontract> feedsListall1 = new ArrayList();
    ArrayList<Bindcontract> feedsListall3 = new ArrayList();
    ChatAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    public final static int reqFilter = 102;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String serviceid = "";
    ActionMenuView bottomBar;
    TextView wait;
    ArrayList<String> conversation_key = new ArrayList<>();
    ArrayList<String> conversation_message = new ArrayList<>();
    @Bind(R.id.search)
    EditText etSearchUser;
    Boolean isVisible =false;

    private DatabaseReference databaseReference;
    String url;

    ArrayList<String> profilepic = new ArrayList<>();
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> last_msg = new ArrayList<>();
    ArrayList<String> timestamp = new ArrayList<>();
    ArrayList<String> status = new ArrayList<>();
    ArrayList<String> last_msg_type = new ArrayList<>();
    ArrayList<Integer> timestampcomp = new ArrayList<>();
    ArrayList<String> chattype = new ArrayList<>();
    ArrayList<Integer> grpcreated = new ArrayList<>();
    ArrayList<String> grp_isprivate = new ArrayList<>();
    ArrayList<String> member_list = new ArrayList<>();
    ArrayList<Integer> creator_id = new ArrayList<>();
    HashMap<Integer, String> hm = new HashMap<Integer, String>();

    ArrayList<HashMap<Integer, String>> com_arr = new ArrayList<>();


    public static FragmentChat getInstance(Bundle bundle) {
        FragmentChat chef = new FragmentChat();
        chef.setArguments(bundle);
        return chef;
    }

    public FragmentChat() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");

        }
        return false;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    boolean visible=false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getUserVisibleHint()) {
            visible = true;

        } else {
            visible = false;

        }
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.chat_list, container, false);
        ButterKnife.bind(this, view);

        disableBackButton();
        feedsListall.clear();
        conversation_key.clear();
        conversation_message.clear();
        profilepic.clear();
        name.clear();
        last_msg.clear();
        timestamp.clear();
        status.clear();
        last_msg_type.clear();
        timestampcomp.clear();
        chattype.clear();
        grpcreated.clear();
        member_list.clear();
        creator_id.clear();

        FloatingActionButton myFab = (FloatingActionButton) view.findViewById(R.id.addChatBtn);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                replaceFragment(R.id.container, new FriendsNew());



            }
        });

        etSearchUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = etSearchUser.getText().toString().toLowerCase(Locale.getDefault());
                filter(text);
            }
        });
       /* if(isVisible){
            initView(view);

        }

*/

        initView(view);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        return view;
    }

    protected void initView(final View view) {


        wait = (TextView) view.findViewById(R.id.wait);
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //callapiAll();
        wait.setVisibility(View.GONE); //remove this later
        Log.d(TAG, "Display Progress Bar");
        loadProgressBar("Please Wait", getString(R.string.api_hiting), false);
        empty_view.setVisibility(View.GONE);


        // String currentUserName = MyApplication.getUserID(Constants.UserID);
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable); //gets conversation list from users table

        ref.limitToLast(MyApplication.LOAD_MAX_CHAT_LIST).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "child data key " + dataSnapshot.getKey());

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //  cities.add(postSnapshot.getValue().toString());
                    Log.d(TAG,"child data " + postSnapshot.getValue().toString());
                    conversation_key.add(dataSnapshot.getKey());
                    conversation_message.add(postSnapshot.getValue().toString());
                    Log.d(TAG, "Snapshot Value:"+ postSnapshot.getValue().toString());


                }
                Log.d(TAG,"271 Conversation Key " + conversation_key.toString());
                getuserinfomathod();
                dismissProgressBar();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (conversation_key.size() == 0) {
            Log.d(TAG, " 300 - No Conversation");
            dismissProgressBar();
            setList();
        }



        /*ivFavTab = (Button) view. findViewById(R.id.iv_fav_tab);
        cashout = (Button) view. findViewById(R.id.cashout);
        money = (Button) view. findViewById(R.id.money);
        money1 = (Button) view. findViewById(R.id.money1);
        ivFavTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeClick(view);
            }
        });
        cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostProperty(view);
            }
        });
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileClick(view);
            }
        });
        money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileWebClick(view);
            }
        });*/
        //callapiAllDemo();

    }


    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }


    private void getuserinfomathod() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};
        profilepic.clear();
        name.clear();
        //final int limit = conversation_key.size() < MyApplication.LOAD_MAX_CHAT_LIST ? conversation_key.size() : MyApplication.LOAD_MAX_CHAT_LIST ; //amit limit for chat list
       // Log.d(TAG, "Inside get User info");
        //i suspect the problem is with the profile picture.. check later..
        Log.d(TAG, "Size of Conversation:"+ conversation_key.size());

        for (int i = 0; i < conversation_key.size(); i++) {
            profilepic.add("0");
            name.add("0");
            grpcreated.add(0);
            grp_isprivate.add("0");
            member_list.add("0");
            creator_id.add(0);

            //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
            Log.d(TAG, " 389 Conversation Key:"+conversation_key.get(i));
            final int chatIndex = i;
            if (conversation_key.get(i).startsWith("grp"))
            {

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(conversation_key.get(i));

                Log.d(TAG, "Chat Key: " + conversation_key.get(i));

                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        if(dataSnapshot.exists()) {
                            try {
                                Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));

                                data[0] = new JSONObject(value).toString();


                          /*group information*/
                                final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                                //    profilepic.add(jsonObject.getString("profilePicLink"));
                                // name.add(jsonObject.getString("name"));
                                Log.d(TAG, "Final Int : " + chatIndex);
                                if (jsonObject1.getString("isPrivate").compareTo("private") == 0) {
                                    grp_isprivate.set(chatIndex, "private");
                                } else {
                                    grp_isprivate.set(chatIndex, "public");
                                }

                                profilepic.set(chatIndex, jsonObject1.getString("group_icon"));
                                name.set(chatIndex, jsonObject1.getString("name"));
                                grpcreated.set(chatIndex, jsonObject1.getInt("timestamp"));
                                // member_list.set(finalI, jsonObject1.getString("members"));
                                member_list.set(chatIndex, "144");

                                Log.d(TAG, "Members list:" + jsonObject1.getString("members"));
                                creator_id.set(chatIndex, jsonObject1.getInt("creator"));



                            /*create group*/

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(jsonObject1.getString("creator")).child("meet_credentials");
                                //  profilepic.add("0");
                                // name.add("0");

                                // final int finalI = i;
                                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                                        // data[0] = dataSnapshot.getValue().toString();
                                        try {
                                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                                            data[0] = new JSONObject(value).toString();


                                            JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                                            //    profilepic.add(jsonObject.getString("profilePicLink"));
                                            // name.add(jsonObject.getString("name"));

                                            //  profilepic.set(finalI, jsonObject1.getString("group_icon"));
                                            // name.set(finalI, jsonObject.getString("name"));


                                        } catch (Exception e) {
                                            Log.d(TAG, "456 - Exception: " + e.toString());
                                        }
                                        try {
                                            Log.d(TAG, "Inside call get user online status, 468");
                                            if (chatIndex == conversation_key.size() - 1) {
                                                get_user_online_status();
                                            }
                                        } catch (Exception e) {
                                            Log.d(TAG, "468 - Exception: " + e.toString());
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                            } catch (Exception e) {
                                Log.d(TAG, "491 - Exception: " + Log.getStackTraceString(e).toString());
                            }
                        }
                        else
                        {
                            Log.d(TAG, "No data in snapshot for key:"+conversation_key.get(chatIndex));
                            if (chatIndex == conversation_key.size() - 1) {
                                get_user_online_status();
                            }
                        }

                        //amit commented march 3 2018, no need to get user online status for group chat
                        /*
                        if (finalI == conversation_key.size() - 1) {
                            get_user_online_status();
                        }
                        */


               /* try {
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                } catch (Exception e) {

                }*/


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                Log.d(TAG, "Not Group :" + conversation_key.get(i));
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(conversation_key.get(i)).child("meet_credentials");



                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        try {
                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                            data[0] = new JSONObject(value).toString();
                            JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                            Log.d(TAG, "GOt Users credentials, picture: " + jsonObject.getString("profilePicLink") + "Name:" + jsonObject.getString("name") );


                            //    profilepic.add(jsonObject.getString("profilePicLink"));
                            // name.add(jsonObject.getString("name"));

                            profilepic.set(chatIndex, jsonObject.getString("profilePicLink"));
                            name.set(chatIndex, jsonObject.getString("name"));
                            grpcreated.set(chatIndex, 0);
                            grp_isprivate.set(chatIndex, "private");


                        } catch (Exception e) {
                            Log.d(TAG, "553 - Exception: " + e.toString());
                            profilepic.set(chatIndex, "");
                            name.set(chatIndex, "Someone");
                            grpcreated.set(chatIndex, 0);
                            grp_isprivate.set(chatIndex, "private");
                        }

                        if (chatIndex == conversation_key.size() - 1) {
                            get_user_online_status();
                            Log.d(TAG, "calling user online status in not group chat");
                        }
                        else
                        {
                            Log.d(TAG, "final I:"+chatIndex +" not equal to conversation_key size :"+conversation_key.size());
                        }



               /* try {
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                } catch (Exception e) {

                }*/


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }


        }


    }

    private void get_user_online_status() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};
        status.clear();
        Log.d(TAG, "Inside get user online status");
        for (int i = 0; i < conversation_key.size(); i++) {
            status.add("0");
            System.out.println("conversition key: " + conversation_key.get(i));
            final int finalI = i;
            if (!conversation_key.get(i).startsWith("grp"))
            {
                //   DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(AppConfig.fbImListTable+"/"+currentUserName);
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(conversation_key.get(i));
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        try {
                            Log.d(TAG, "618 - Data Snapshot: " + dataSnapshot.getValue());
                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                            // Log.i("online status", "dataSnapshot online status" + new JSONObject(value));
                            data[0] = new JSONObject(value).toString();


                            JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                            // status.add(jsonObject.getString("status"));

                            status.set(finalI, jsonObject.getString("status"));


                        } catch (Exception e) {
                            status.set(finalI,"offline"); //couldn't get status so setting offline.
                            Log.d(TAG, "630 - Exception: " + e.toString());
                        }




              /*  try{
                    JSONObject jobject = new JSONObject(new JSONObject(value).toString());

                    Bindcontract contract = new Bindcontract();

                    contract.setName(jobject.getString("name"));
                    contract.setStatus(jobject.getString("email"));

                    feedsListall.add(contract);

                    setList();
                }catch (Exception e){

                }*/


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }


            if (finalI == conversation_key.size() - 1) {
                getLastMessage();
                Log.d(TAG, "Calling get last message");
            }
            else
            {
                Log.d(TAG, "Not calling last message, finalI : " + finalI + "Conversation Key size: " + conversation_key.size());
            }
        }

    }

    private void getLastMessage() {
        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        final String[] data = {""};

        last_msg.clear();
        timestamp.clear();
        last_msg_type.clear();
        timestampcomp.clear();
        chattype.clear();

        Log.d(TAG, "Inside get last message, conversation_key Size: " + conversation_key.size());
        for (int i = 0; i < conversation_key.size(); i++) {

            last_msg.add("0");
            timestamp.add("0");
            last_msg_type.add("0");
            timestampcomp.add(0);
            chattype.add("0");

            Log.d(TAG, " 693 Conversation Key: " + conversation_key.get(i));
            if (conversation_key.get(i).startsWith("grp"))
            {
                Log.d(TAG, "Conversation key starts with group, calling db ref now");
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupChatTable).child(conversation_message.get(i));

                final int finalI = i;
                ref.limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        try {
                            System.out.println("conversition data " + dataSnapshot.getValue());
                            System.out.println("conversition data key " + dataSnapshot.getKey());
                            // data[0] = dataSnapshot.getValue().toString();


                            Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                            Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));

                            JSONObject jsonObject = new JSONObject(value1);
                            String content = jsonObject.getString("content");
                            String timestamp_str = jsonObject.getString("timestamp");

                            //  long timestamptime = stamp.getTime()-Integer.parseInt(timestamp_str);

                            last_msg.set(finalI, content);
                            timestamp.set(finalI, timestamp_str);
                            last_msg_type.set(finalI, jsonObject.getString("type"));
                            timestampcomp.add(Integer.parseInt(timestamp_str));
                            chattype.set(finalI, "grp");
                        /*last_msg.add(content);
                        timestamp.add(ds);
                        last_msg_type.add(jsonObject.getString("type"));*/
                            getChatData();
                        /*if (finalI == conversation_key.size() - 1) {

                        }*/


                        } catch (Exception e) {
                            Log.d(TAG, "776 - Exception: " + e.toString());
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }  else {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbchatTable).child(conversation_message.get(i));

                final int finalI = i;
                ref.limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        try {
                            System.out.println("conversition data " + dataSnapshot.getValue());
                            System.out.println("conversition data key " + dataSnapshot.getKey());
                            // data[0] = dataSnapshot.getValue().toString();


                            Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                            Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));

                            JSONObject jsonObject = new JSONObject(value1);
                            String content = jsonObject.getString("content");
                            String timestamp_str = jsonObject.getString("timestamp");

                            //  long timestamptime = stamp.getTime()-Integer.parseInt(timestamp_str);

                            last_msg.set(finalI, content);
                            timestamp.set(finalI, timestamp_str);
                            last_msg_type.set(finalI, jsonObject.getString("type"));
                            timestampcomp.add(Integer.parseInt(timestamp_str));
                            chattype.set(finalI, "singlechat");
                        /*last_msg.add(content);
                        timestamp.add(ds);
                        last_msg_type.add(jsonObject.getString("type"));*/
                            getChatData();
                        /*if (finalI == conversation_key.size() - 1) {

                        }*/


                        } catch (Exception e) {
                            Log.d(TAG, "838 - Exception: " + e.toString());
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

        }

    }


    private void getChatData() {
        feedsListall.clear();

        try {


            System.out.println("name size " + conversation_key.size());

            for (int i = 0; i < conversation_key.size(); i++)
            {
                Log.d(TAG, "what is going on" + i + "Key Size:"+ conversation_key.size());

                Bindcontract contract = new Bindcontract();
                contract.setName(name.get(i));
                contract.setStatus(status.get(i));
                contract.setProfileImg(profilepic.get(i));
                contract.setLastmsg(last_msg.get(i));
                contract.setTimestamp(timestamp.get(i));
                contract.setChatkey(conversation_message.get(i));
                contract.setFromid(conversation_key.get(i));
                contract.setLastmsgtype(last_msg_type.get(i));
                contract.setChattype(chattype.get(i));
                contract.setGrpcreateddate(grpcreated.get(i));
                contract.setGrpisPrivate(grp_isprivate.get(i));
                contract.setCreatorID(creator_id.get(i));
                Log.d(TAG, "Chat Key: "+conversation_key.get(i));
                if (conversation_key.get(i).startsWith("grp")) {
                    try {
                        Log.d(TAG, "Member List: " + member_list.toString());
                        contract.setMember(member_list.get(i));

                    } catch (Exception e) {
                        Log.d(TAG, " 898 - Exception getting Group Info : " + e.toString());
                        contract.setMember("hmm");
                        setList();
                    }

                }
                feedsListall.add(contract);
            }

            feedsListall3.clear();
            for (Bindcontract wp : feedsListall) {
                if (wp.getGrpisPrivate().toLowerCase(Locale.getDefault())
                        .startsWith("private")) {
                    Bindcontract wrapper = new Bindcontract();

                    wrapper.setName(wp.getName());
                    wrapper.setStatus(wp.getStatus());
                    wrapper.setProfileImg(wp.getProfileImg());
                    wrapper.setLastmsg(wp.getLastmsg());
                    wrapper.setTimestamp(wp.getTimestamp());
                    wrapper.setChatkey(wp.getChatkey());
                    wrapper.setFromid(wp.getFromid());
                    wrapper.setLastmsgtype(wp.getLastmsgtype());
                    wrapper.setChattype(wp.getChattype());
                    wrapper.setGrpcreateddate(wp.getGrpcreateddate());
                    wrapper.setGrpisPrivate(wp.getGrpisPrivate());
                    wrapper.setMember(wp.getMember());
                    wrapper.setCreatorID(wp.getCreatorID());
                    feedsListall3.add(wrapper);

                }

            }

            Collections.sort(feedsListall3, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    Bindcontract p1 = (Bindcontract) o1;
                    Bindcontract p2 = (Bindcontract) o2;
                    return p2.getTimestamp().compareToIgnoreCase(p1.getTimestamp());
                }
            });

            //getDataNew();
            setList();
        } catch (Exception e) {
            Log.d(TAG, "542 - Exception: " + e.toString());

        }


    }




    public void setList() {
        System.out.println("service list" + feedsListall3.size());
        wait.setVisibility(View.GONE);
        if (feedsListall.size() == 0) {
            empty_view.setVisibility(View.VISIBLE);
        } else {
            empty_view.setVisibility(View.GONE);
            adapterall = new ChatAdapter(getActivity(), feedsListall);
            recycleView.setAdapter(adapterall);
        }

    }


    class MyComparator implements Comparator {
        Map map;

        public MyComparator(Map map) {
            this.map = map;
        }

        @Override
        public int compare(Object o1, Object o2) {
            return (o1.toString()).compareTo(o2.toString());
        }
    }

    class ReversedOrdering implements Comparator<Integer> {
        public int compare(Integer lhs, Integer rhs) {
            // compare reversed
            return rhs.compareTo(lhs);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

        /*MenuItem search = menu.findItem(R.id.searchFood);
        search.setVisible(false);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEE MMM dd hh:mm:ss z yyyy", cal).toString();
        return date;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        // map.clear();
        if (charText.length() == 0) {
            if (feedsListall3.size() == 0) {
                empty_view.setVisibility(View.VISIBLE);
            } else {
                getChatData();
                empty_view.setVisibility(View.GONE);
            }

        } else {
            feedsListall1.clear();
            for (Bindcontract wp : feedsListall3) {
                if (wp.getName().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    Bindcontract wrapper = new Bindcontract();

                    wrapper.setName(wp.getName());
                    wrapper.setStatus(wp.getStatus());
                    wrapper.setProfileImg(wp.getProfileImg());
                    wrapper.setLastmsg(wp.getLastmsg());
                    wrapper.setTimestamp(wp.getTimestamp());
                    wrapper.setChatkey(wp.getChatkey());
                    wrapper.setFromid(wp.getFromid());
                    wrapper.setLastmsgtype(wp.getLastmsgtype());
                    wrapper.setChattype(wp.getChattype());
                    wrapper.setGrpcreateddate(wp.getGrpcreateddate());
                    wrapper.setGrpisPrivate(wp.getGrpisPrivate());
                    wrapper.setMember(wp.getMember());
                    wrapper.setCreatorID(wp.getCreatorID());

                    feedsListall1.add(wrapper);



                }
                empty_view.setVisibility(View.GONE);
                recycleView.setAdapter(new ChatAdapter(getActivity(), feedsListall1));
                new ChatAdapter(getActivity(), feedsListall1).notifyDataSetChanged();
            }

        }


    }


    public class Background extends AsyncTask<Void, Integer, Void> {
        private Handler mHandler = new Handler(Looper.getMainLooper());
        protected Void doInBackground(Void... urls) {

            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        protected void onPostExecute(Long result) {
            //showDialog("Downloaded " + result + " bytes");
        }
    }
}

