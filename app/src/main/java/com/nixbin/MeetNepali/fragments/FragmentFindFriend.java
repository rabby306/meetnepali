package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.google.gson.Gson;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.FindFriendAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.Helper;
import com.nixbin.MeetNepali.utilities.Pref;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.ApiConfig;
import com.nixbin.MeetNepali.webutility.AppConfig;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.BindFindfriend;
import com.nixbin.MeetNepali.wrapper.models.Data;
import com.nixbin.MeetNepali.wrapper.models.RequestCity;
import com.nixbin.MeetNepali.wrapper.models.UserList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nixbin.MeetNepali.webutility.WebUrls.BASE_URL_image;

/**
 * Created by manu on 6/8/2017.
 */

public class FragmentFindFriend extends BaseFragment implements WebCompleteTask {
    public static String TAG = "Udo";
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.empty_view)
    TextView empty_view = null;
    RequestQueue queue;
    ArrayList<BindFindfriend> feedsListall = new ArrayList();
    ArrayList<BindFindfriend> feedsListall1 = new ArrayList();
    FindFriendAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    public final static int reqFilter = 102;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String serviceid = "";
    ActionMenuView bottomBar;
    SwipeRefreshLayout swipeRefreshLayout;
    String url;
    @Bind(R.id.bottom_toolbar)
    ActionMenuView bottomToolbar;
    @Bind(R.id.clear)
    TextView clear;
    @Bind(R.id.iv)
    ImageView iv;
    @Bind(R.id.et_search_user)
    EditText etSearchUser;
    @Bind(R.id.remove)
    TextView remove;
    @Bind(R.id.rl_search_last_user)
    RelativeLayout rlSearchLastUser;
    @Bind(R.id.bottom)
    RelativeLayout bottom;
    @Bind(R.id.search)
    EditText search;
    Pref pref;
    private int pageCount=0;

    public static FragmentFindFriend getInstance(Bundle bundle) {
        FragmentFindFriend chef = new FragmentFindFriend();
        chef.setArguments(bundle);
        return chef;
    }

    public FragmentFindFriend() {
    }


    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        setTitleOnAction(TAG, false);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");

        }return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        disableBackButton();
        FloatingActionButton myFab = (FloatingActionButton) view.findViewById(R.id.addFriendBtn);
        myFab.setVisibility(view.GONE);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                filter(text);
            }
        });
        initView(view);



        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Find Friends");
        setHasOptionsMenu(true);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        return view;
    }

    protected void initView(final View view) {


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        pref=new Pref(getContext());
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recycleView.setHasFixedSize(true);

        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //get_user_list();
        DownloadRecomandedFriends();


        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLastItemDisplaying(recyclerView)) {
                    //get_user_list();
                    DownloadRecomandedFriends();
                }
            }
        });


/*remove.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        rlSearchLastUser.setVisibility(View.GONE);
    }
});
     *//*   searchResults = feedsListall;
        etSearchUser.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,int count) {
//get the text in the EditText
                String searchString=etSearchUser.getText().toString();
                int textLength=searchString.length();
                searchResults.clear();

                for(int i=0;i<feedsListall.size();i++)
                {
                    String playerName=feedsListall.get(i).getName().toString();
                    if(textLength<=playerName.length()){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(playerName.substring(0,textLength)))
                            searchResults.add(feedsListall.get(i));
                    }

                }
                adapterall.notifyDataSetChanged();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchTxt = s.toString().trim();

              //  codeMapSearch.clear();
              ///  countriesArraySearch.clear();

                *//*for (int i = 0; i < feedsListall.size(); i++) {
                    String name = countriesArray.get(i);

                    if (name.toLowerCase().startsWith(searchTxt.toLowerCase())) {
                        codeMapSearch.put(countriesArray.get(i),codeMap.get(countriesArray.get(i)));
                        countriesArraySearch.add(countriesArray.get(i));
                    }
                }*//*

               // lv.setAdapter(new CountryAdapter(countriesArraySearch, codeMapSearch, dialog));

                if (!searchTxt.equals("")) {
                    clear.setVisibility(View.VISIBLE);
                } else {
                    clear.setVisibility(View.INVISIBLE);
                }
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                etSearchUser.setText("");

            }
        });*/
        /*ivFavTab = (Button) view. findViewById(R.id.iv_fav_tab);
        cashout = (Button) view. findViewById(R.id.cashout);
        money = (Button) view. findViewById(R.id.money);
        money1 = (Button) view. findViewById(R.id.money1);
        ivFavTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeClick(view);
            }
        });
        cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostProperty(view);
            }
        });
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileClick(view);
            }
        });
        money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileWebClick(view);
            }
        });*/
        //callapiAllDemo();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

    }

    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {

            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }
    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;

        //   adapterall.clearList();
        //get_user_list();
    }

    @Override
    public void onStart() {
        super.onStart();
        isRefreshing = true;

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void get_user_list() {
        pageCount++;
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "people_nearby");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("search", "");
            data.put("city", pref.getString("city_id"));
            data.put("state", pref.getString("region_id"));
            data.put("country", pref.getString("country_id"));
            data.put("pageNumber",pageCount);

            data.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        Log.w("Rabby",  " JSON: " + new Gson().toJson(object));
        new WebTask(getActivity(), WebUrls.API_URL, object, FragmentFindFriend.this, RequestCode.CODE_FriendList, 1);
    }

    public void onComplete(String response, int taskcode) {
        swipeRefreshLayout.setRefreshing(false);
        try {
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_FriendList) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONArray data = null;
                    data = object.getJSONArray("data");

                    JSONObject jobj = null;
                    //feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        BindFindfriend contract = new BindFindfriend();
                        jobj = data.getJSONObject(i);
                        contract.setName(jobj.getString("name"));
                        contract.setEmail(jobj.getString("name")); //changed here to display name instead of email in friends list
                        contract.setUserid(jobj.getString("user_id"));
                        contract.setImageurl(jobj.getString("profile_img"));
                        // contract.setPhonenumber(jobj.getString("phone"));
                        feedsListall.add(contract);
                        empty_view.setVisibility(View.GONE);

                    }
                    //  empty_view.setVisibility(View.VISIBLE);
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            empty_view.setVisibility(View.VISIBLE);
        } else {
            empty_view.setVisibility(View.GONE);
            //searchResults=OriginalValues initially

            adapterall = new FindFriendAdapter(getActivity(), feedsListall);
            recycleView.setAdapter(adapterall);


        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        /*inflater.inflate(R.menu.menu_search_food, menu);
        MenuItem search = menu.findItem(R.id.searchFood);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.searchFood:
                // rlSearchLastUser.setVisibility(View.VISIBLE);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapterall.getFilter().filter(newText);
                return true;
            }
        });
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        // map.clear();
        adapterall.getFilter().filter(charText);

        /*if (charText.length() == 0) {
            if (feedsListall.size() == 0) {
                empty_view.setVisibility(View.VISIBLE);
            } else {
                get_user_list();
                empty_view.setVisibility(View.GONE);
            }

        } else {
            feedsListall1.clear();
            for (BindFindfriend wp : feedsListall) {
                if (wp.getName().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    BindFindfriend wrapper = new BindFindfriend();

                    wrapper.setName(wp.getName());
                    wrapper.setEmail(wp.getEmail());
                    wrapper.setUserid(wp.getUserid());
                    wrapper.setImageurl(wp.getImageurl());

                    feedsListall1.add(wrapper);

                }

                empty_view.setVisibility(View.GONE);
                recycleView.setAdapter(new FindFriendAdapter(getActivity(), feedsListall1));
               // new FindFriendAdapter(getActivity(), feedsListall1).notifyDataSetChanged();
            }

        }*/


    }

    private void DownloadRecomandedFriends()
    {
        pageCount++;
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("people_nearby");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("");
        data.setCountry(pref.getString("country_id"));
        data.setState(pref.getString("region_id"));
        data.setCity(pref.getString("city_id"));
        data.setPageNumber(pageCount+"");
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<UserList> call = getResponse.GetRecomandedUserData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {

                if (response.isSuccessful()) {
                    try {
                        if(response.body().getData().size()>0) {
                            search.setVisibility(View.VISIBLE);
                            if(feedsListall.size()==0){
                                adapterall = new FindFriendAdapter(getActivity(), feedsListall);
                            recycleView.setAdapter(adapterall);}

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                BindFindfriend contract = new BindFindfriend();
                                contract.setName(response.body().getData().get(i).getName());
                                contract.setEmail(response.body().getData().get(i).getEmail()); //changed here to display name instead of email in friends list
                                contract.setUserid(response.body().getData().get(i).getUserId() + "");
                                contract.setImageurl(response.body().getData().get(i).getProfileImg());
                                // contract.setPhonenumber(jobj.getString("phone"));
                                feedsListall.add(contract);
                                empty_view.setVisibility(View.GONE);

                            }
                            adapterall.notifyDataSetChanged();

                        }
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {

               // Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }

}

