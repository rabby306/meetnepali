package com.nixbin.MeetNepali.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nixbin.MeetNepali.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 11/21/2017.
 */

public class FriendsNew extends BaseFragment {

    @Bind(R.id.tabs)
    TabLayout tabs;
    @Bind(R.id.viewpager)
    ViewPager viewpager;

    @Override
    protected void initUi(View view) {

    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    public static FriendsNew getInstance(Bundle bundle) {
        FriendsNew chef = new FriendsNew();
        chef.setArguments(bundle);
        return chef;
    }

    public FriendsNew() {
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onClick(View v) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.new_home_page, container, false);
        ButterKnife.bind(this, view);
        disableBackButton();

        initView(view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);


        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    protected void initView(final View view) {
        //setupViewPager(viewpager);
        tabs.addTab(tabs.newTab().setText("Recommended"));
        tabs.addTab(tabs.newTab().setText("My Friends"));
        tabs.addTab(tabs.newTab().setText("Friend Requests"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        //viewpager.setOffscreenPageLimit(1);
        final PagerAdapter adapter = new PagerAdapter
                (getFragmentManager(), 1,1);
        viewpager.setAdapter(adapter);

      //  viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    tabs.setScrollPosition(0,0,true);
                   // tabs.getTabAt(0).select();


                    final PagerAdapter adapter = new PagerAdapter
                            (getFragmentManager(), 1,1);
                    viewpager.setAdapter(adapter);
                    //viewpager.setCurrentItem(tab.getPosition());
                   /* viewpager.setCurrentItem(0);
                    tabs.setupWithViewPager(viewpager);*/


                }
                if (tab.getPosition()==1){
                    tabs.setScrollPosition(1,0,true);
                  //  tabs.getTabAt(1).select();
                    final PagerAdapter adapter = new PagerAdapter
                            (getFragmentManager(), 1,2);
                    viewpager.setAdapter(adapter);
                   // viewpager.setCurrentItem(tab.getPosition());
                   /* viewpager.setCurrentItem(0);
                    tabs.setupWithViewPager(viewpager);*/

                }
                if (tab.getPosition()==2){
                    tabs.setScrollPosition(2,0,true);
                   // tabs.getTabAt(2).select();
                    final PagerAdapter adapter = new PagerAdapter
                            (getFragmentManager(), 1,3);
                    viewpager.setAdapter(adapter);
                   // viewpager.setCurrentItem(tab.getPosition());
                   /* viewpager.setCurrentItem(0);
                    tabs.setupWithViewPager(viewpager);*/

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

       // tabs.setupWithViewPager(viewpager);
       // tabs.setOnTabSelectedListener(onTabSelectedListener(viewpager));





    }
    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
    /*private void setupViewPager(ViewPager viewPager) {
        HomepageNew.ViewPagerAdapter adapter = new HomepageNew.ViewPagerAdapter(getChildFragmentManager());
        adapter.notifyDataSetChanged();
        adapter.addFragment(new FragmentChat(), "Chat");
        adapter.addFragment(new FragmentPublicChat() , "Public Group");
        adapter.addFragment(new FriendList(), "Friends");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        onTabSelectedListener(viewPager);

    }*/

   /* class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    FragmentChat tab1 = new FragmentChat();
                    return tab1;
                case 1:
                    FragmentPublicChat tab2 = new FragmentPublicChat();
                    return tab2;
                case 2:
                    FriendList tab3 = new FriendList();
                    return tab3;
                default:
                    return null;
            }

           // return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }*/



    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        int tabc;

        public PagerAdapter(FragmentManager fm, int NumOfTabs ,int tab) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
            this.tabc=tab;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment=null;
            System.out.println("position pager"+position);
            switch (position) {
                case 0:
                    if (tabc==1){
                        fragment = new FragmentFindFriend();
                    }else if (tabc==2){
                        fragment = new FriendList();
                    }else {
                        fragment = new FriendRequestList();
                    }

                    break;

                case 1:
                  //  fragment = new FragmentPublicChat();
                    break;

                case 2:
                   // fragment = new FriendList();
                    break;

                default:
                    fragment=null;
                    break;
            }
            return fragment;

        }

        @Override
        public int getCount() {
           /* int a=0;
            switch (mNumOfTabs){
                case 0:
                    a=1;
                    break;
                case 1:
                    a=2;
                    break;
                case 2:
                    a=3;
                    break;
            }*/
            return mNumOfTabs;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
