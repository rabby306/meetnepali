package com.nixbin.MeetNepali.adpter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.MapGetLocActivityChat;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.Helper;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.ChatGroupsMain;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class ChatAdpterClass extends BaseAdapter {
    private ArrayList<ChatGroupsMain> feedsListall;
    private static Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    static String date_tag="";
    static String date_tag2="";


    //private String DelateApi = ApiManagerClass.API_URL+ApiManagerClass.DelateJob;
    public static final String KEY_propertyID = "id";

    public ChatAdpterClass(Context context, ArrayList<ChatGroupsMain> feedsListall)
    {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    private class ViewHolder {
        private TextView sender_text_text, sender_text_time, receiver_text, receiver_text_time, sender_pic_text_time, sender_loc_text_time, receiver_pic_text_time, receiver_loc_text_time,tag_text;
        LinearLayout sender_ll, receiver_ll, sender_pic_ll, sender_loc_ll, receiver_pic_ll, receiver_loc_ll,tag_ll,date_tag_ll;
        ImageView profile_image, profile_pic_image, sender_pic, profile_loc_image, sender_loc, receiver_pic, receiver_loc;
        TextView sender_text_name,sender_img_name,sender_loc_name,date_tag_text;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.chat_screen_item, null);
            holder = new ViewHolder();

            // for reciever section
            holder.profile_image = (ImageView) convertView.findViewById(R.id.profile_image);
            holder.sender_text_text = (TextView) convertView.findViewById(R.id.sender_text_text);
            holder.sender_text_time = (TextView) convertView.findViewById(R.id.sender_text_time);
            holder.receiver_text = (TextView) convertView.findViewById(R.id.receiver_text);
            holder.receiver_text_time = (TextView) convertView.findViewById(R.id.receiver_text_time);

            holder.sender_pic_text_time = (TextView) convertView.findViewById(R.id.sender_pic_text_time);
            holder.sender_loc_text_time = (TextView) convertView.findViewById(R.id.sender_loc_text_time);
            holder.receiver_pic_text_time = (TextView) convertView.findViewById(R.id.receiver_pic_text_time);
            holder.receiver_loc_text_time = (TextView) convertView.findViewById(R.id.receiver_loc_text_time);

            holder.profile_pic_image = (ImageView) convertView.findViewById(R.id.profile_pic_image);
            holder.sender_pic = (ImageView) convertView.findViewById(R.id.sender_pic);
            holder.sender_loc = (ImageView) convertView.findViewById(R.id.sender_loc);
            holder.receiver_pic = (ImageView) convertView.findViewById(R.id.receiver_pic);
            holder.receiver_loc = (ImageView) convertView.findViewById(R.id.receiver_loc);
            holder.profile_loc_image = (ImageView) convertView.findViewById(R.id.profile_loc_image);


            holder.sender_ll = (LinearLayout) convertView.findViewById(R.id.sender_ll);
            holder.receiver_ll = (LinearLayout) convertView.findViewById(R.id.receiver_ll);
            holder.sender_pic_ll = (LinearLayout) convertView.findViewById(R.id.sender_pic_ll);
            holder.sender_loc_ll = (LinearLayout) convertView.findViewById(R.id.sender_loc_ll);
            holder.receiver_pic_ll = (LinearLayout) convertView.findViewById(R.id.receiver_pic_ll);
            holder.receiver_loc_ll = (LinearLayout) convertView.findViewById(R.id.receiver_loc_ll);
            holder.tag_ll=(LinearLayout)convertView.findViewById(R.id.tag_ll);
            holder.tag_text=(TextView)convertView.findViewById(R.id.tag_text);

            holder.sender_text_name=(TextView)convertView.findViewById(R.id.sender_text_name);
            holder.sender_img_name=(TextView)convertView.findViewById(R.id.sender_img_name);
            holder.sender_loc_name=(TextView)convertView.findViewById(R.id.sender_loc_name);

            holder.date_tag_ll=(LinearLayout)convertView.findViewById(R.id.date_tag_ll);
            holder.date_tag_text=(TextView)convertView.findViewById(R.id.date_tag_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final ChatGroupsMain feeds = (ChatGroupsMain) getItem(position);

        date_tag=getOnlyDate(Long.parseLong(feeds.getTimestamp()));
        if (position==0){
            holder.date_tag_ll.setVisibility(View.VISIBLE);
            holder.date_tag_text.setText(date_tag);
        }else {
            final ChatGroupsMain feeds2 = (ChatGroupsMain) getItem(position-1);

            date_tag2=getOnlyDate(Long.parseLong(feeds2.getTimestamp()));
            if (date_tag.compareTo(date_tag2)!=0){
                /*add tag*/
                holder.date_tag_ll.setVisibility(View.VISIBLE);
                holder.date_tag_text.setText(date_tag);

            }else {
                holder.date_tag_ll.setVisibility(View.GONE);
            }
        }





        if (MyApplication.getFirebaseID(Constants.FirebaseId).compareTo(feeds.getFromid().trim()) == 0) {

            Timestamp stamp = new Timestamp(Long.parseLong(feeds.getTimestamp()));
            long sta = stamp.getTime();


            if (feeds.getType().compareTo("text") == 0) {
                holder.receiver_ll.setVisibility(View.VISIBLE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.GONE);

                holder.receiver_text.setText(feeds.getContent());
                holder.receiver_text_time.setText(getDate(sta));


            } else if (feeds.getType().compareTo("photo") == 0) {
                holder.receiver_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.VISIBLE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.GONE);

                Glide.with(context).load(feeds.getContent()).placeholder(R.drawable.cast_album_art_placeholder).crossFade().into(holder.receiver_pic);
                holder.receiver_pic_text_time.setText(getDate(sta));
                holder.receiver_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFullImage(feeds.getContent());
                    }
                });

            }else if (feeds.getType().compareTo("tag")==0){
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.VISIBLE);
                holder.tag_text.setText("New Group Created "+getDate(sta));
            }else {
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.VISIBLE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.GONE);

                holder.receiver_loc_text_time.setText(getDate(sta));

                holder.receiver_loc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String loc = feeds.getContent();
                        String[] loca = loc.split(":");
                        double pLong = Double.parseDouble(loca[1]);
                        double pLat = Double.parseDouble(loca[0]);
                        LatLng fromPostion = new LatLng(pLat, pLong);
                        Bundle args = new Bundle();
                        args.putParcelable("longLat_dataPrivider", fromPostion);
                        Intent i = new Intent(context, MapGetLocActivityChat.class);
                        i.putExtra("_latitude", pLat);
                        i.putExtra("_longitude", pLong);
                        i.putExtras(args);
                        context.startActivity(i);
                    }
                });
            }

        } else { //if i didn't send the message.

            Timestamp stamp = new Timestamp(Long.parseLong(feeds.getTimestamp()));
            long sta = stamp.getTime();

            if (feeds.getType().compareTo("text") == 0) {
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.VISIBLE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.GONE);

                if (feeds.getGrpType().compareTo("grp")==0){
                    holder.sender_text_name.setVisibility(View.VISIBLE);
                    holder.sender_img_name.setVisibility(View.GONE);
                    holder.sender_loc_name.setVisibility(View.GONE);
                    holder.sender_text_name.setText(feeds.getMembername());
                }


                holder.sender_text_text.setText(feeds.getContent());
                holder.sender_text_time.setText(getDate(sta));
                Glide.clear(holder.profile_image);
                holder.profile_image.setImageResource(android.R.color.transparent);
                getMemberinfo(feeds.getFromid(),holder.sender_text_name,holder.profile_image);

                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String imagePath = v.getTag(R.id.profile_image).toString();
                        //Log.d("Image Path", "Image path:" + imagePath);
                        Helper.enLargeImage(context, imagePath);

                    }
                });

               /* if (feeds.getGrpType().compareTo("grp")==0){
                    Utils.loadImageRound(context, holder.profile_image, WebUrls.API_URL+feeds.getMemberProfile());
                }else {
                    Utils.loadImageRound(context, holder.profile_image, ChatScreenActivity.receverprofile);
                }*/


            } else if (feeds.getType().compareTo("photo") == 0) {
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.VISIBLE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.GONE);

                if (feeds.getGrpType().compareTo("grp")==0){
                    holder.sender_text_name.setVisibility(View.GONE);
                    holder.sender_img_name.setVisibility(View.VISIBLE);
                    holder.sender_loc_name.setVisibility(View.GONE);
                    holder.sender_img_name.setText(feeds.getMembername());
                }


                Glide.with(context).load(feeds.getContent()).placeholder(R.drawable.cast_album_art_placeholder).crossFade().into(holder.sender_pic);
                /*if (feeds.getGrpType().compareTo("grp")==0){
                    Utils.loadImageRound(context, holder.profile_image, WebUrls.API_URL+feeds.getMemberProfile());
                }else {
                    Utils.loadImageRound(context, holder.profile_image, ChatScreenActivity.receverprofile);
                }*/
                getMemberinfo(feeds.getFromid(),holder.sender_img_name,holder.profile_image);

                holder.sender_pic_text_time.setText(getDate(sta));
                holder.sender_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFullImage(feeds.getContent());
                    }
                });


            }else if (feeds.getType().compareTo("tag")==0){
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.GONE);
                holder.tag_ll.setVisibility(View.VISIBLE);
                holder.tag_text.setText("New Group Created "+getDate(sta));
            }
            else {
                holder.receiver_ll.setVisibility(View.GONE);
                holder.receiver_pic_ll.setVisibility(View.GONE);
                holder.receiver_loc_ll.setVisibility(View.GONE);
                holder.sender_ll.setVisibility(View.GONE);
                holder.sender_pic_ll.setVisibility(View.GONE);
                holder.sender_loc_ll.setVisibility(View.VISIBLE);
                holder.tag_ll.setVisibility(View.GONE);

                if (feeds.getGrpType().compareTo("grp")==0){
                    holder.sender_text_name.setVisibility(View.GONE);
                    holder.sender_img_name.setVisibility(View.GONE);
                    holder.sender_loc_name.setVisibility(View.VISIBLE);
                    holder.sender_loc_name.setText(feeds.getMembername());
                }



                holder.sender_loc_text_time.setText(getDate(sta));
               /* if (feeds.getGrpType().compareTo("grp")==0){
                    Utils.loadImageRound(context, holder.profile_image, WebUrls.API_URL+feeds.getMemberProfile());
                }else {
                    Utils.loadImageRound(context, holder.profile_image, ChatScreenActivity.receverprofile);
                }*/
                getMemberinfo(feeds.getFromid(),holder.sender_loc_name,holder.profile_image);
                holder.sender_loc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String loc = feeds.getContent();
                        String[] loca = loc.split(":");
                        double pLong = Double.parseDouble(loca[1]);
                        double pLat = Double.parseDouble(loca[0]);
                        LatLng fromPostion = new LatLng(pLat, pLong);
                        Bundle args = new Bundle();
                        args.putParcelable("longLat_dataPrivider", fromPostion);
                        Intent i = new Intent(context, MapGetLocActivityChat.class);
                        i.putExtra("_latitude", pLat);
                        i.putExtra("_longitude", pLong);
                        i.putExtras(args);
                        context.startActivity(i);
                    }
                });


            }


        }



        return convertView;
    }

    @Override
    public int getCount() {
        return feedsListall.size();
    }

    @Override
    public Object getItem(int position) {
        return feedsListall.get(position);
    }

    @Override
    public long getItemId(int position) {
        return feedsListall.indexOf(getItem(position));
    }

    private String getDate(long time) {
        long a=(long) 151244926000L;
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        if (time>a){
            cal.setTimeInMillis(time/1000 );
        }else {
            cal.setTimeInMillis(time * 1000);
        }

        String date = DateFormat.format("hh:mm a", cal).toString();
        return date;
    }
    private String getOnlyDate(long time) {
        long a=(long) 151244926000L;
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        if (time>a){
            cal.setTimeInMillis(time/1000 );
        }else {
            cal.setTimeInMillis(time * 1000);
        }
        String date = DateFormat.format("dd/MM/yyyy", cal).toString();
        return date;
    }

    public static void showFullImage(String imagePath) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(dialog.getWindow().FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.full_image);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.show();
        ImageView image = (ImageView) dialog.findViewById(R.id.image);

        Glide.with(context)
                .load(imagePath).placeholder(R.drawable.cast_album_art_placeholder)
                .into(image);
        image.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void getMemberinfo(final String key, final TextView tv,final ImageView iv) {

        final String[] member_profile = {""};

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(key).child("meet_credentials");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                // data[0] = dataSnapshot.getValue().toString();


                try {

                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                    // data[0] = new JSONObject(value).toString();
                    System.out.println("content added "+value);

                    JSONObject jsonObject = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));

                    member_profile[0] = jsonObject.getString("profilePicLink");
                   // memberprofile = jsonObject.getString("profilePicLink");
                    // member_profile.add(jsonObject.getString("profilePicLink"));
                    // name.set(finalI, jsonObject.getString("name"));
                    tv.setText(jsonObject.getString("name"));
                    Log.d("Chat adapter class", "Inside load profile picture, url" + jsonObject.getString("profilePicLink"));
                    //Utils.loadImageRound(context, iv, WebUrls.BASE_URL_image+jsonObject.getString("profilePicLink")); //this was broken
                    Glide.clear(iv);
                    Utils.loadImageRound(context, iv, jsonObject.getString("profilePicLink"));
                    iv.setTag(R.id.profile_image,jsonObject.getString("profilePicLink"));


                } catch (Exception e) {

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }

}

