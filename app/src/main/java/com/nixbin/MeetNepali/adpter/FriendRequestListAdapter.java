package com.nixbin.MeetNepali.adpter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nixbin.MeetNepali.R;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbin.MeetNepali.activities.FriendRequestActivity;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class FriendRequestListAdapter extends RecyclerView.Adapter<FriendRequestListAdapter.MyViewHolder> implements WebCompleteTask {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> feedsListall;
    private List<Bindcontract> feedsListall;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;

    public FriendRequestListAdapter(Context context, List<Bindcontract> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
    }

    public void setList(List<Bindcontract> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.friend_request_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }


    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Bindcontract feeds = feedsListall.get(position);
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getName());
        // Utils.loadImageRound(context, holder.img, "https://i.pinimg.com/originals/89/8b/6c/898b6cdb2059322fd78f0673ccca55b3.jpg");
        // holder.tvSoldOut.setTag(position);
        if (feeds.getProfileImg().compareTo("") != 0) {
            Utils.loadImageRound(context, holder.img, WebUrls.BASE_URL_image+ feeds.getProfileImg());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   context.startActivity(new Intent(context, UdoDetailActivity.class).putExtra("data",feeds));
            }
        });
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_select = position;
                showAlertDialog(context, feeds, position_select, feeds.getFriendid(), "Are you sure?\nYou want to Accept this Friend!", context.getString(R.string.app_name), true);

            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_select = position;
                showAlertDialog(context, feeds, position_select, feeds.getFriendid(), "Are you sure?\nYou want to Decline this Friend!", context.getString(R.string.app_name), false);

            }
        });
    }

    public AlertDialog showAlertDialog(final Context context, final Bindcontract feeds, final int pos, final String cartId, String msg, String title, final Boolean status) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(msg);
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callapiAll(cartId, status);
            }
        });

        alertBuilder.setNegativeButton("Cancel", null);

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView name;
        private Button accept, decline;
        LinearLayout rl_accept_decline;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            accept = (Button) itemView.findViewById(R.id.accept);
            decline = (Button) itemView.findViewById(R.id.decline);
            rl_accept_decline = (LinearLayout) itemView.findViewById(R.id.rl_accept_decline);

        }
    }

    private void callapiAll(String friendid, Boolean status) {
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "friend_status");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("friend_id", friendid);
            data.put("status", status + "");

            jobj.put("data", data);

            System.out.println("json data " + jobj.toString());

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(context, WebUrls.API_URL, object, FriendRequestListAdapter.this, RequestCode.CODE_AcceptFriend, 1);

    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response ", response);
        if (taskcode == RequestCode.CODE_AcceptFriend) {
            if (response != null) {
                try {
                    JSONObject jobj = new JSONObject(response);
                    if (jobj.getString("status").compareTo("success") == 0) {
                        Toast.makeText(context, jobj.getString("message"), Toast.LENGTH_SHORT).show();

                        context.startActivity(new Intent(context, FriendRequestActivity.class).putExtra("activity","fromchat"));
                        ((Activity)context).finish();
                    }
                } catch (Exception e) {

                }


            }
        }


    }
}
