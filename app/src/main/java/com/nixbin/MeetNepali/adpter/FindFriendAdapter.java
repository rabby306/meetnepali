package com.nixbin.MeetNepali.adpter;

import android.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.fragments.FragmentFindFriend;
import com.nixbin.MeetNepali.fragments.FriendsNew;
import com.nixbin.MeetNepali.utilities.Helper;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.BindFindfriend;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class FindFriendAdapter extends RecyclerView.Adapter<FindFriendAdapter.MyViewHolder> implements WebCompleteTask, Filterable {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> friendsList;
    private List<BindFindfriend> friendsList;
    private List<BindFindfriend> mArrayList;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;

    public FindFriendAdapter(Context context, List<BindFindfriend> friendsList) {
        this.context = context;
        this.friendsList = friendsList;
        this.mArrayList = friendsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
    }

    public void setList(List<BindFindfriend> BindAllJobList) {
        if (BindAllJobList != null)
            this.friendsList.addAll(BindAllJobList);
        else
            this.friendsList = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.find_friend_list_item;

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    friendsList = mArrayList;
                } else {

                    ArrayList<BindFindfriend> filteredList = new ArrayList<>();

                    for (BindFindfriend androidVersion : mArrayList) {

                        if (androidVersion.getName().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    friendsList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = friendsList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                friendsList = (ArrayList<BindFindfriend>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void clearList() {
        if (friendsList != null)
            friendsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final BindFindfriend feeds = friendsList.get(position);
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getEmail());

        // holder.tvSoldOut.setTag(position);
        if (feeds.getImageurl().compareTo("") != 0) {
            Utils.loadImageRound(context, holder.img, WebUrls.BASE_URL_image + feeds.getImageurl());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }
        //   Utils.loadImageRound(context, holder.img, "https://i.pinimg.com/originals/89/8b/6c/898b6cdb2059322fd78f0673ccca55b3.jpg");

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageURL = WebUrls.BASE_URL_image + feeds.getImageurl();
                Helper.enLargeImage(context, imageURL);
                Log.d("FindFriendAdapter", "Image URL: "+ imageURL);

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   context.startActivity(new Intent(context, UdoDetailActivity.class).putExtra("data",feeds));
                Log.d("FindFriendAdapter", "The Whole Item Clicked"); //later on display user detail popup..
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_select = position;
                showAlertDialog(context, feeds, position_select, feeds.getUserid(), "Send Friend Request to " + feeds.getName() + "?", context.getString(R.string.app_name),1);
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_select = position;
                showAlertDialog(context, feeds, position_select, feeds.getUserid(), "Want to Remove " + feeds.getName() + "?", context.getString(R.string.app_name),2);
            }
        });
    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            friendsList.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    public AlertDialog showAlertDialog(final Context context, final BindFindfriend feeds, final int pos, final String cartId, String msg, String title, final int methodchoose) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(msg);
        alertBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(methodchoose==1)
                callapiAll(cartId,"friend_request");
                else
                    callapiAll(cartId,"delete_friend");
                /*replaceFragment(R.id.container, new FragmentFindFriend());

                Fragment currentFragment =context. getFragmentManager().find("YourFragmentTag");
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(currentFragment);
                fragmentTransaction.attach(currentFragment);
                fragmentTransaction.commit();*/

                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new FragmentFindFriend())
                        .commit();
            }
        });

        alertBuilder.setNegativeButton("Cancel", null);

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img,remove;
        private LinearLayout add;
        private TextView name;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            remove = (ImageView) itemView.findViewById(R.id.remove);
            name = (TextView) itemView.findViewById(R.id.name);
            add = (LinearLayout) itemView.findViewById(R.id.add);
        }
    }

    private void callapiAll(String friendid,String method) {
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", method);
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("friend_id", friendid);
            jobj.put("data", data);
        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        Log.w("Rabby",  " JSON: " + new Gson().toJson(object));
        new WebTask(context, WebUrls.API_URL, object, FindFriendAdapter.this, RequestCode.CODE_AddFriend, 1);

    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response ", response);
        if (taskcode == RequestCode.CODE_AddFriend) {
            if (response != null) {
                try {
                    JSONObject jobj = new JSONObject(response);
                    if (jobj.getString("status").compareTo("success") == 0) {
                        Toast.makeText(context, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                       /* FragmentFindFriend frg = new FragmentFindFriend();
                        frg.refreshList();*/

                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new FriendsNew())
                                .commit();
                    }
                } catch (Exception e) {

                }


            }
        }


    }


}
