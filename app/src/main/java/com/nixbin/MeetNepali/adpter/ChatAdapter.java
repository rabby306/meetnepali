package com.nixbin.MeetNepali.adpter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.chat.ChatScreenActivity;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.views.ISO8601;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> feedsListall;
    private List<Bindcontract> feedsListall;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;

    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;

    public ChatAdapter(Context context, List<Bindcontract> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       // pd = new ProgressDialog(context);
      //  queue = Volley.newRequestQueue(context);
    }

    public void setList(List<Bindcontract> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.chat_item_list;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }


    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Bindcontract feeds = feedsListall.get(position);

        System.out.println("last msg "+feeds.getLastmsg());
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getName());

        /*if (feeds.getGrpisPrivate().compareTo("public")==0){
            holder.status.setVisibility(View.INVISIBLE);
        }else {*/
            holder.status.setVisibility(View.VISIBLE);
        //}

      /*  if (feeds.getLastmsgtype().compareTo("text")==0){
            holder.status.setText(feeds.getLastmsg());
        }else if (feeds.getLastmsgtype().compareTo("photo")==0){
            holder.status.setText("Image Shared");
        }else if (feeds.getLastmsgtype().compareTo("tag")==0){
            holder.status.setText(feeds.getLastmsg());
        }else {
            holder.status.setText("Location Shared");
        }*/

        try{
            Timestamp stamp = new Timestamp(Integer.parseInt(feeds.getTimestamp()));
            long sta = stamp.getTime();
            String ds = getDate(sta);
            holder.time.setText(ISO8601.aTimeAgo(ds));
        }catch (Exception e){

        }


        // holder.time.setText(feeds.getTimestamp());


        if (feeds.getChattype().compareTo("grp")==0){
            holder.status_color.setVisibility(View.GONE);
            Utils.loadImageRoundGroup(context, holder.img, feeds.getProfileImg());
        }else {
            holder.status_color.setVisibility(View.VISIBLE);

            if (feeds.getStatus().compareTo("online")==0){
                holder.status_color.setTextColor(context.getResources().getColor(R.color.green));
            }else {
                holder.status_color.setTextColor(context.getResources().getColor(R.color.white));
            }
            Utils.loadImageRound(context, holder.img, feeds.getProfileImg());
        }


        // holder.tvSoldOut.setTag(position);
        /*if (feeds.getProfileImg().compareTo("")!=0) {
            Utils.loadImageRound(context, holder.img, WebUrls.API_URL + feeds.getProfileImg());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   context.startActivity(new Intent(context, ChatScreenActivity.class).putExtra("data",feeds));
            }
        });
    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView name, status, time,status_color;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (TextView) itemView.findViewById(R.id.status);
            time = (TextView) itemView.findViewById(R.id.time);
            status_color = (TextView) itemView.findViewById(R.id.status_color);

        }
    }

    private String getDate(long time) {
        long a=(long) 151244926000L;
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        if (time>a){
            cal.setTimeInMillis(time/1000);
        }else {
            cal.setTimeInMillis(time * 1000);
        }

        String date = DateFormat.format("EEE MMM dd hh:mm:ss z yyyy", cal).toString();
        return date;
    }
    private String getOnlyDate(long time) {
        long a=(long) 151244926000L;
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        if (time>a){
            cal.setTimeInMillis(time/1000);
        }else {
            cal.setTimeInMillis(time * 1000);
        }

        String date = DateFormat.format("dd/MM/yyyy", cal).toString();
        return date;
    }
}
