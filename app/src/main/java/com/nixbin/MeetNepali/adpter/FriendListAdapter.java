package com.nixbin.MeetNepali.adpter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nixbin.MeetNepali.R;
import com.android.volley.RequestQueue;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.chat.ChatScreenActivity;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.fragments.FriendList;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.MyViewHolder> implements WebCompleteTask {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> feedsListall;
    private List<Bindcontract> feedsListall;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;

    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;

    public FriendListAdapter(Context context, List<Bindcontract> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      //  pd = new ProgressDialog(context);
        //queue = Volley.newRequestQueue(context);
    }

    public void setList(List<Bindcontract> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.friend_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }

    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Bindcontract feeds = feedsListall.get(position);
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getName());
       // Utils.loadImageRound(context, holder.img, "https://i.pinimg.com/originals/89/8b/6c/898b6cdb2059322fd78f0673ccca55b3.jpg");
        // holder.tvSoldOut.setTag(position);
        if (feeds.getProfileImg().compareTo("")!=0) {
            Utils.loadImageRound(context, holder.img, feeds.getProfileImg());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feeds.setGrpisPrivate("single"); //otherwise it will crash
                context.startActivity(new Intent(context, ChatScreenActivity.class).putExtra("data",feeds).putExtra("activity","fromchat"));
            }
        });
        holder.friend_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog(context, feeds.getOtherid(), "Are you sure you want to delete?", "Delete");
            }
        });

    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return feedsListall.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img,friend_delete;
        private TextView name;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            friend_delete = (ImageView) itemView.findViewById(R.id.friend_delete);


        }
    }
    android.support.v7.app.AlertDialog alertDialog = null;
    public android.support.v7.app.AlertDialog showAlertDialog(final Context context, final String frindid, String msg, String title) {
        android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(msg);
        alertBuilder.setNeutralButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
               // callapiDeleate(MyApplication.getInstance().getSid(Constants.AccessToken), feeds.getPostID());
                callDeleteFriend(frindid);
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
        alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

    private void callDeleteFriend(String frindid) {
        System.out.println("myid"+MyApplication.getUserID(Constants.UserID)+" other_id "+frindid);

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", "delete_friend");
            JSONObject data = new JSONObject();

            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("id", frindid);

            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(context, WebUrls.API_URL, object, FriendListAdapter.this, RequestCode.Code_deleteFriend, 1);


    }
    @Override
    public void onComplete(String response, int taskcode) {
        System.out.println("response "+response);
        if (taskcode==RequestCode.Code_deleteFriend){
            if (response!=null){
                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new FriendList())
                        .commit();
            }
        }

    }
}
