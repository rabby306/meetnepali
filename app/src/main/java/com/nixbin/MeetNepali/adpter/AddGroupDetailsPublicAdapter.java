package com.nixbin.MeetNepali.adpter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class AddGroupDetailsPublicAdapter extends RecyclerView.Adapter<AddGroupDetailsPublicAdapter.MyViewHolder> {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> feedsListall;
    private List<Bindcontract> feedsListall;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;

    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> profile = new ArrayList<>();


    public AddGroupDetailsPublicAdapter(Context context, List<Bindcontract> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);

    }

    public void setList(List<Bindcontract> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.friend_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }


    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Bindcontract feeds = feedsListall.get(position);
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getName());
        // Utils.loadImageRound(context, holder.img, "https://i.pinimg.com/originals/89/8b/6c/898b6cdb2059322fd78f0673ccca55b3.jpg");
        // holder.tvSoldOut.setTag(position);
        if (feeds.getProfileImg().compareTo("") != 0) {
            Utils.loadImageRound(context, holder.img, feeds.getProfileImg());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }
//        holder.checkBox.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // context.startActivity(new Intent(context, ChatScreenActivity.class).putExtra("data",feeds));
            }
        });
        if (MyApplication.getUserID(Constants.UserID).compareTo(feeds.getAdmin_key())==0){
            holder.friend_delete.setVisibility(View.VISIBLE);
        }else {
            holder.friend_delete.setVisibility(View.GONE);
        }
        holder.friend_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"please contact to our support",Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img,friend_delete;
        private TextView name;
        private CheckBox checkBox;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            friend_delete = (ImageView) itemView.findViewById(R.id.friend_delete);


        }
    }

    private void checkdata(String namestr, String image) {
        name.add(namestr);
        profile.add(image);


    }

    private void uncheckdata(String namestr, String image) {
        name.remove(namestr);
        profile.remove(image);


    }
}
