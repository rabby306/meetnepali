package com.nixbin.MeetNepali.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nixbin.MeetNepali.R;


/**
 * Created by Advosoft2 on 3/1/2017.
 */

public class MenuAdapter extends BaseAdapter {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;
    private String name;
    private int profile;
    private int mIcons[];
    private int selectedPosition = 0;
    private String mNavTitles[];
    private LayoutInflater mInflater;

    public MenuAdapter(String Titles[], int Icons[], Context mContext) {
        mNavTitles = Titles;
        mIcons = Icons;
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mNavTitles.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = mInflater.inflate(R.layout.drawer_item, parent, false);

        TextView textView = (TextView) convertView.findViewById(R.id.text_value);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon_navigation);
        ImageView arraow_icon = (ImageView) convertView.findViewById(R.id.arraow_icon);

        icon.setImageResource(mIcons[position]);
        textView.setText(mNavTitles[position]);

        /*if (position == selectedPosition)
            arraow_icon.setImageResource(R.drawable.arrow_menu_activated);
        else
            arraow_icon.setImageResource(R.drawable.arrow_menu_deactivated);*/

        return convertView;
    }

    public void setSelectedPosition(int position) {

        this.selectedPosition = position;

    }

}
