package com.nixbin.MeetNepali.adpter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.DataTransferInterface;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class SelectMemberAdapter extends RecyclerView.Adapter<SelectMemberAdapter.MyViewHolder> {
    // public Locality feeds1 = null;
    // private List<AllPropertyModelUser> feedsListall;
    private List<Bindcontract> feedsListall;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;

    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> profile = new ArrayList<>();
    ArrayList<String> friendid = new ArrayList<>();
    DataTransferInterface dataTransferInterface;


    public SelectMemberAdapter(Context context, List<Bindcontract> feedsListall,DataTransferInterface dtinterface) {
        this.context = context;
        this.feedsListall = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
        dataTransferInterface=dtinterface;
    }

    public void setList(List<Bindcontract> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.select_member_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);

    }


    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Bindcontract feeds = feedsListall.get(position);
        //Pass the values of feeds object to Views
        holder.name.setText(feeds.getName());
        // Utils.loadImageRound(context, holder.img, "https://i.pinimg.com/originals/89/8b/6c/898b6cdb2059322fd78f0673ccca55b3.jpg");
        // holder.tvSoldOut.setTag(position);
        if (feeds.getProfileImg().compareTo("") != 0) {
            Utils.loadImageRound(context, holder.img, feeds.getProfileImg());
        } else {
            holder.img.getResources().getDrawable(R.drawable.round_profile_image);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // context.startActivity(new Intent(context, ChatScreenActivity.class).putExtra("data",feeds));
                if (holder.checkBox.isChecked()){

                    uncheckdata(feeds.getName(), feeds.getProfileImg(),feeds.getFriendid());
                    //  check=false;
                    holder.checkBox.setChecked(false);
                }else {
                    // checkboxlist[getPosition]=holder.checkBox.isChecked();
                    checkdata(feeds.getName(), feeds.getProfileImg(),feeds.getFriendid());
                    holder.checkBox.setChecked(true);
                    //   check=true;
                }
            }
        });
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkdata(feeds.getName(), feeds.getProfileImg(),feeds.getFriendid());
                } else {
                    uncheckdata(feeds.getName(), feeds.getProfileImg(),feeds.getFriendid());
                }

            }
        });

    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView name;
        private CheckBox checkBox;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);


        }
    }

    private void checkdata(String namestr, String image,String friendidstr) {
        name.add(namestr);
        profile.add(image);
        friendid.add(friendidstr);

       // List<String> al = new ArrayList<>();
// add elements to al, including duplicates
        Set<String> hs = new HashSet<>();
        Set<String> hs1 = new HashSet<>();
        Set<String> hs2 = new HashSet<>();
        hs.addAll(name);
        name.clear();
        name.addAll(hs);

        hs1.addAll(profile);
        profile.clear();
        profile.addAll(hs1);

        hs2.addAll(friendid);
        friendid.clear();
        friendid.addAll(hs2);

        dataTransferInterface.setValues(name, profile,friendid);

    }

    private void uncheckdata(String namestr, String image,String friendidstr) {
        name.remove(namestr);
        profile.remove(image);
        friendid.remove(friendidstr);

        Set<String> hs = new HashSet<>();
        hs.addAll(name);
        name.clear();
        name.addAll(hs);

        hs.addAll(profile);
        profile.clear();
        profile.addAll(hs);

        hs.addAll(friendid);
        friendid.clear();
        friendid.addAll(hs);
        dataTransferInterface.setValues(name, profile,friendid);

    }
}
