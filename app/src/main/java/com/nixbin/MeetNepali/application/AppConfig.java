package com.nixbin.MeetNepali.application;

import android.os.Build;

import com.nixbin.MeetNepali.webutility.WebUrls;

public class AppConfig {
    //This is the main config now, weburl references from here.
    public final static boolean debug = false;
    public final static boolean devMode = false;
    public final static boolean firebaseDevMode = false;

    public final static boolean https = false;

    private final static String EMULATOR_URL = "www.meetemulator.com/meetw3/";
    private final static String LOCAL_URL = "www.meetlocal.com/meetw3/";
    //private final static String LOCAL_URL = "192.168.100.81/meetw3/";
    private final static String PROD_URL="www.meetnepali.com/";

    private final static String DEV_URL = isEmulator() ? EMULATOR_URL : LOCAL_URL;
//this setting are not used, web urls one is used

	public static final String APP_URL = devMode ? DEV_URL : PROD_URL;

    public static final String BASE_URL = getBaseUrl();
    public static final String PORTAL_URL = BASE_URL + "portal/settings";
	public static final String URL_LOGIN = BASE_URL +"api/login";// Server user login url
	public static final String URL_REGISTER = BASE_URL +"api/register";// Server user register url
	public static final String URL_LOAD = BASE_URL +"api/session/";//URL to load after authentication , create session there
	public static final String URL_PUSH_NOTIFICATION = BASE_URL+"api/fcm/";
	public static final String URL_LOGOUT = BASE_URL +"api/logout";

	//firebase references...
    public static final String fbUsersTable = firebaseDevMode ? "dev_meet_users" : "meet_users";
    public static final String fbGroupListTable = firebaseDevMode ? "dev_meet_rooms" : "meet_rooms";
    public static final String fbchatTable = firebaseDevMode ? "dev_meet_conversations" : "meet_conversations";
    public static final String fbGroupChatTable = firebaseDevMode ? "dev_meet_room_conversations" : "meet_room_conversations";
    public static final String fbOnlineStatusTable =  firebaseDevMode ? "dev_meet_online_status" : "meet_online_status";

    public static final String fbGroupIconStorage = "gs://meetnepali-4f1d5.appspot.com/group_icons/";




    //push notification stuff..
	// global topic to receive app wide push notifications
	public static final String TOPIC_GLOBAL = "global";
	// broadcast receiver intent filters
	public static final String REGISTRATION_COMPLETE = "registrationComplete";
	public static final String PUSH_NOTIFICATION = "pushNotification";
	// id to handle the notification in the notification tray
	public static final int NOTIFICATION_ID = 100;
	public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
	public static final String SHARED_PREF = "meet_firebase";


	//end firebase

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    private static String getBaseUrl()
    {
        String  baseURL;
        String protocol;
        if(https){ protocol = "https"; } else {protocol = "http";}
        if(devMode){ baseURL = protocol + "://" + APP_URL; }
        else { baseURL = protocol + "://" + APP_URL;}

        return baseURL;
    }

   /* private static String getAppUrl()
    {
        String  AppURL;
        if(devMode){ AppURL = DEV_APP_URL; }
        else { AppURL = APP_URL;}

        return AppURL;
    }*/

}
