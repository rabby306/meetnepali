package com.nixbin.MeetNepali.application;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * Created by Advosoft2 on 6/27/2017.
 */

public class MyApplication extends MultiDexApplication {
    public static MyApplication myApplication = null;
    private static Context ctx;
    private String CANCER = "MEETNEPALI";
    public static SharedPreferences sp;
    private double currentLatitude;
    private double currentLongitude;
    private int hight;
    /* amit modified */
    public static final String APPURL = "http://meet.advocosoft"; //see WebUrls.java
    public static final int LOAD_MAX_CHAT_LIST = 30;
    public static final int LOAD_MAX_CONVERSATION_LIST = 30;
    public static final int MAX_PUBLIC_CHAT_LIST = 200;
    public static final int MAX_PUBLIC_CONVERSATION_LIST=30;

    //global Push notification TOPIC:
    public static final String GLOBAL_TOPIC = "meet-global";
    public static final String TOPIC_PREFIX = "meetGroup-";
    public static final String ANDROID_TOPIC = "meet-android";

    //amit modified end
    private static final String isLogin = "UserLoginStatus";
    public static ImageLoader loader;
    private static final String profileImage = "profileImage";

    public static final String filter_friends="filterfrinds";
    public static final String filter_partner="filterpartner";
    public static final String filter_categor="filtercategory";
    public static final String filter_location="filterlocation";
    public static final String filter_lat="filterlat";
    public static final String filter_long="filterlong";
    public static final String filter_range="filterrange";
    public static final String filter_startdate="startdate";
    public static final String filter_enddate="enddate";
    public static final String contrycode="contrycode";
    public static final String server_updatelist="serverupdatelist";
    public static final String latitude="lat";
    public static final String longitude="lng";
    public static final String profieimage="profileimage";

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = new MyApplication();
//        Crittercism.initialize(getApplicationContext(), "9e06313bc11a42fbb3b9e92051213e9900555300");
        ctx = getApplicationContext();
        sp = ctx.getSharedPreferences(CANCER, 0);
        loader = ImageLoader.getInstance();
        loader.init(ImageLoaderConfiguration.createDefault(this));
    }
    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;

    }

    public static void showMessage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();

    }
    public void hideSoftKeyBoard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            // check if no view has focus:
            View v = activity.getCurrentFocus();
            if (v == null)
                return;

            inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MyApplication getInstance() {
        if (myApplication == null) {
            myApplication = new MyApplication();
        }


        return myApplication;
    }
    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }



    public static void setUserLocation(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getUserLocation(String type) {
        String e = sp.getString(type, "");
        return e;
    }



    public double getCurrentLatitude() {
        return currentLatitude;
    }
    public int getImageHight() {
        return hight;
    }

    public void getImageHight(int hight) {
        this.hight = hight;
    }
    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public static String getAuthToken(String type){
        String token = sp.getString(type, "");
        return token;
    }

    public static void saveAuthToken(String type, String value)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }


    public static String getUserData(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void saveUserData(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static void setSid(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getSid(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void setUserID(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getUserID(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static String getlangID(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void setLangId(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getUserName(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void setUserName(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static void setUserEmail(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getUserEmail(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void setUserMobile(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getUserMobile(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static String getUserDOB(String type) {
        String e = sp.getString(type, "");
        return e;
    }
    public static void setUserDOB(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getProfileImage(Context context,String s) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(profileImage, "");
    }

    public static void setProfileImage(Context context, String profile) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(profileImage, profile);
        editor.commit();
    }


    public static Boolean getUserLoginStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(isLogin, false);
    }

    public static void setUserLoginStatus(Context context, Boolean isUserLogin) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(isLogin, isUserLogin);
        editor.commit();
    }
    /*add filter*/
    public static Boolean getFilter_friends(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(filter_friends, false);
    }

    public static void setFilter_friends(Context context, Boolean friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(filter_friends, friends);
        editor.commit();
    }

    public static Boolean getFilter_partner(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(filter_partner, false);
    }

    public static void setFilter_partner(Context context, Boolean friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(filter_partner, friends);
        editor.commit();
    }

    public static String getFilter_categor(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_categor, "");
    }

    public static void setFilter_categor(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_categor, friends);
        editor.commit();
    }

    public static String getFilter_location(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_location, "");
    }

    public static void setFilter_location(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_location, friends);
        editor.commit();
    }

    public static String getFilter_lat(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_lat, "");
    }

    public static void setFilter_lat(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_lat, friends);
        editor.commit();
    }

    public static String getFilter_long(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_long, "");
    }

    public static void setFilter_long(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_long, friends);
        editor.commit();
    }

    public static String getFilter_range(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_range, "");
    }

    public static void setFilter_range(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_range, friends);
        editor.commit();
    }

    public static String getFilter_startdate(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_startdate, "");
    }

    public static void setFilter_startdate(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_startdate, friends);
        editor.commit();
    }

    public static String getFilter_enddate(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(filter_enddate, "");
    }

    public static void setFilter_enddate(Context context, String friends) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(filter_enddate, friends);
        editor.commit();
    }
    public static void setContrycode(Context context, String data){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(contrycode, data);
        editor.commit();
    }
    public static String getContrycode(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(contrycode, "");
    }
    public static void setServer_updatelist(Context context, String data){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(server_updatelist, data);
        editor.commit();
    }
    public static String getServer_updatelist(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(server_updatelist, "");
    }

    public static void setLatitude(Context context, String data){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(latitude, data);
        editor.commit();
    }
    public static String getLatitude(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(latitude, "");
    }
    public static void setLongitude(Context context, String data){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(longitude, data);
        editor.commit();
    }
    public static String getLongitude(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(longitude, "");
    }


    public static void setFirebaseID(String type, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(type, value);
        editor.commit();
    }
    public static String getFirebaseID(String type) {
        String e = sp.getString(type, "");
        return e;
    }

}
