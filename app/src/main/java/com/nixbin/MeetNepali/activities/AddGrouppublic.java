package com.nixbin.MeetNepali.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.AddGroupPublicAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.GroupChat;
import com.nixbin.MeetNepali.wrapper.Bindcontract;
import com.nixbin.MeetNepali.wrapper.GroupSendMessage;
import com.nixbin.MeetNepali.wrapper.NewUser;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbGroupChatTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupIconStorage;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupListTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbchatTable;

/**
 * Created by advosoft on 11/23/2017.
 */

public class AddGrouppublic extends BaseActiivty implements WebCompleteTask{
    @Bind(R.id.add_icon)
    ImageView addIcon;
    @Bind(R.id.group_title)
    EditText groupTitle;
    @Bind(R.id.privat_toggle)
    ToggleButton privatToggle;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    ArrayList<String> name;
    ArrayList<String> profile;
    ArrayList<String> friend_id;
    private static final int VERTICAL_ITEM_SPACE = 5;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    AddGroupPublicAdapter adapterall;
    @Bind(R.id.empty_view)
    TextView emptyView;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    String filepath = "";
    String profilepic_firebase,members="",downloadurlstr="";
    String isprivatestr="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addgroup_public);
        ButterKnife.bind(this);
        enableBackButton();
        getSupportActionBar().setTitle("Group Info");
        name = getIntent().getStringArrayListExtra("name");
        profile = getIntent().getStringArrayListExtra("profile");
        friend_id=getIntent().getStringArrayListExtra("friendid");
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(AddGrouppublic.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        getdata();
        addIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageTypePost();
            }
        });
        privatToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


            }
        });
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void getdata() {
        feedsListall.clear();
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append(MyApplication.getUserID(Constants.UserID)+",");
        for (int i = 0; i < name.size(); i++) {
            Bindcontract contract = new Bindcontract();
            //clean this all try catch in everything :)
            //  contract.setFriendid(jobj.getString("friend_id"));
            try {
                contract.setProfileImg(profile.get(i));
            }
            catch (Exception e){
                Log.d("HMM", "Exception on profile  : " + e.toString());
                contract.setProfileImg("");
            }
            try {
            contract.setName(name.get(i));
        }
            catch (Exception e){
            Log.d("HMM", "Exception : " + e.toString());
        }
            try {
            contract.setFriendid(friend_id.get(i));
    }
            catch (Exception e){
        Log.d("HMM", "Exception : " + e.toString());
    }
            try {
            members=stringBuilder.append(friend_id.get(i)+",").toString();
}
            catch (Exception e){
                    Log.d("HMM", "Exception : " + e.toString());
                    }
            // contract.setEmail(jobj.getString("email"));
            //   contract.setFromid(jobj.getString("friend_id"));

            feedsListall.add(contract);
        }
        setList();
    }

    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new AddGroupPublicAdapter(AddGrouppublic.this, feedsListall);
            recycleView.setAdapter(adapterall);
            recycleView.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.send_btn:
                if (TextUtils.isEmpty(groupTitle.getText().toString())){
                    toast("please enter group title");

                }else {
                    Toast.makeText(AddGrouppublic.this,"Group Created Successfully!!!",Toast.LENGTH_SHORT).show();
                    addGroup(downloadurlstr,groupTitle.getText().toString()) ;
                  //  finish();
                }


                //   Dosome();
               // startActivity(new Intent(this,AddGrouppublic.class).putExtra("name",namesend).putExtra("profile",profilesend));
                return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Choose option");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select image from ")
                .setCancelable(false)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(this.getFilesDir(), fileName);
        }
    }

    private void openGallery() {

        /* Intent chooseIntent = new Intent(Intent.ACTION_PICK,
         MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
         chooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
         startActivityForResult(chooseIntent, REQUEST_CODE_GALLERY);*/

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);

        // startActivity(new Intent(ContainerDetailActivity.this,
        // GalleryActivity.class));

    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(this, data.getData());

                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    //  profileImage.setImageBitmap(resized);
                    filepath = getPath(this, getImageUri(this, resized));
                    // Utils.loadImageRound(getActivity(),profileImage,filepath);
                    uploadFile(filepath);

                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                //  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(this, addIcon,"file://" + mFileTemp.getPath());

                // MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {

                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                //filepath= getPath(data.getData());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filepath = getPath(this, getImageUri(this, resized));
                ///  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(this, addIcon,"file://" + mFileTemp.getPath());
                //   profileImage.setImageBitmap(resized);
                uploadFile(filepath);
            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        } else if (requestCode == Constants.LOCATION) {
            try {
               /* Bundle bundle = data.getExtras();
                EndRoute route = (EndRoute) bundle.getSerializable(Constants.object);
                latitude = route.getLat();
                longitude = route.getLng();
                address.setText(route.getAddress());*/
            } catch (Exception e) {

            }

        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void uploadFile(String bitmap) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(fbGroupIconStorage);
        StorageReference mountainImagesRef = storageRef.child(userId);

        /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);*/
        try {
            InputStream stream = new FileInputStream(new File(bitmap));
            UploadTask uploadTask = mountainImagesRef.putStream(stream);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    // sendMsg("" + downloadUrl, 2);
                    System.out.println("download url" + downloadUrl);
                    downloadurlstr=downloadUrl+"";
                    Log.d("downloadUrl-->", "" + downloadUrl);
                   // addGroup(downloadUrl + "");
                }
            });
        } catch (Exception e) {

        }

    }

    private void addGroup(String downloadurl,String title){
        try{
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable);
            String groupID = "grp_"+mDatabase.push().getKey();
            String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
            // String members = members;
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            members=members.substring(0,members.length()-1);
            String grp_title=title;

            if (privatToggle.isChecked()){
                isprivatestr="private";
            }else {
                isprivatestr="public";
            }
           // DatabaseReference mDatabase1 = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(userId);

            GroupChat sendMessage = new GroupChat(fromid, downloadurlstr, isprivatestr, members, grp_title, tsLong);
            mDatabase.child(groupID).setValue(sendMessage);

            Log.d("AddGroup", "Group Created: Key: " + groupID + "Members : " + members);

            addGroupMessage(groupID);
            //call subscribe users here
            notifySubscribers(groupID, grp_title, members, isprivatestr);

        }catch (Exception e){
            Log.d("AddGroup", " 467 - exception "+e.toString());
        }


    }

    private void notifySubscribers(String groupID, String groupName, String members, String isprivatestr) //amit added
    {
        String TAG = "Notify Subscribers";
        Log.d(TAG, "274 - Calling Api");
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "subscribe_group");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("group_id", groupID);
            data.put("members", members);
            data.put("group_name", groupName);
            data.put("chat_type", isprivatestr); //private , public or single
            data.put("Authorization", "Bearer"+ MyApplication.getAuthToken(Constants.API_TOKEN));
            jobj.put("data", data);


        } catch (Exception e) {
            Log.d(TAG, "285 - Exception Calling API");
        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        Log.d(TAG, "Calling API to notify Subscribers: " +object.toString());

        //new WebTask(getActivity(), WebUrls.API_URL, object, FriendList.this, RequestCode.CODE_SUBSCRIBE_GROUP, 1);
        //new WebTask(AddGrouppublic.this, WebUrls.API_URL, object, AddGrouppublic.this, RequestCode.CODE_SUBSCRIBE_GROUP, 1);
        new WebTask(AddGrouppublic.this, WebUrls.API_URL, object, AddGrouppublic.this, RequestCode.CODE_SUBSCRIBE_GROUP, 1);//


    }

    @Override
    public void onComplete(String response, int taskcode) { //when web task is complete.

        try {
                Log.d("Notify Subscribers", "Subscriber Notify Response: " + response.toString());

                } catch (Exception e) {
                   Log.d("Notify Subscribers", "Exception : " + e.toString());
                }
        finish(); //moved finish it was killing the app because finish was called before async task completed.


    }

    private long addGroupMessage(String key){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbGroupChatTable).child(key);
        String userId = mDatabase.push().getKey();
        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
       // String toid = feeds.getFromid();

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        GroupSendMessage sendMessage = new GroupSendMessage("Group created", fromid, tsLong, "tag");
        mDatabase.child(userId).setValue(sendMessage);

        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
        NewUser newUser = new NewUser(key);
        ref.child(key).setValue(newUser);

        for (int i=0;i<friend_id.size();i++){
            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(friend_id.get(i)).child(fbchatTable);
            NewUser newUser1 = new NewUser(key);
            ref1.child(key).setValue(newUser1);
        }


        return tsLong;
    }

}
