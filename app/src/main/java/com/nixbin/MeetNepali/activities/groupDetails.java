package com.nixbin.MeetNepali.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.AddGroupDetailsPublicAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.Helper;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.wrapper.GroupChat;
import com.nixbin.MeetNepali.wrapper.Bindcontract;
import com.nixbin.MeetNepali.wrapper.GroupSendMessage;
import com.nixbin.MeetNepali.wrapper.NewUser;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbGroupChatTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupIconStorage;
import static com.nixbin.MeetNepali.application.AppConfig.fbGroupListTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;
import static com.nixbin.MeetNepali.application.AppConfig.fbchatTable;

/**
 * Created by advosoft on 11/23/2017.
 */

public class groupDetails extends BaseActiivty
{
    @Bind(R.id.add_icon)
    ImageView addIcon;
    @Bind(R.id.group_title)
    EditText groupTitle;
    @Bind(R.id.privat_toggle)
    ToggleButton privatToggle;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    ArrayList<String> name;
    ArrayList<String> profile;
    ArrayList<String> friend_id;
    private static final int VERTICAL_ITEM_SPACE = 5;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    AddGroupDetailsPublicAdapter adapterall;
    @Bind(R.id.empty_view)
    TextView emptyView;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    String filepath = "";
    String profilepic_firebase, members = "", groupIconFbURL = "";
    String chatkey = "";
    String membersFB[];
    @Bind(R.id.adminprofile)
    ImageView adminprofile;
    @Bind(R.id.admin_name)
    TextView adminName;

    @Bind(R.id.btnUpdate)
    Button btnUpdate;
    String groupName; //will be updated by getGroupDetails function

    private static final String TAG = "Group Details";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_detail);
        ButterKnife.bind(this);
        enableBackButton();
        getSupportActionBar().setTitle("Group Details");
        chatkey = getIntent().getStringExtra("chatkey");
       /* name = getIntent().getStringArrayListExtra("name");
        profile = getIntent().getStringArrayListExtra("profile");
        friend_id=getIntent().getStringArrayListExtra("friendid");*/
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(groupDetails.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        // getdata();
        getGroupDetails();
        addIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageTypePost();
            }
        });
        privatToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
//http://www.amalhichri.net/android-firebase-realtime-database-basic-cruds/
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, Object> fbData = new HashMap<>();
                boolean update = false;

                if(!groupTitle.getText().toString().equals(groupName) && groupTitle.getText().toString().compareTo("")!=0)
                {
                    Helper.debugPrint(TAG, "Title has changed : " + groupName);
                    fbData.put("name", groupTitle.getText().toString());
                    update = true;

                }
                if(filepath.compareTo("")!= 0 )
                {
                   uploadFile(filepath, fbData);

                    update = false; //we already update the group by calling upload file function.
                    log("Image Uploaded , FbURL : " + groupIconFbURL);
                }

                if(update) //this means image was not uploaded..so lets just finish it..
                {
                    updateGroupDetail(fbData);
                }
                else
                {finish();}


                Helper.debugPrint(TAG, "FilePath: "+ filepath + "DownloadUrlStr:" + groupIconFbURL + " Title: " + groupTitle.getText().toString());
            }
        });
    }

    public void updateGroupDetail(HashMap<String, Object> groupData)
    {
        try {

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(chatkey);
            ref.updateChildren(groupData);

        } catch (Exception e) {
            log("161 - Exception:" + e.toString());
        }
        finish();
    }



    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void getdata() {
        feedsListall.clear();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < name.size(); i++) {
            Bindcontract contract = new Bindcontract();

            //  contract.setFriendid(jobj.getString("friend_id"));
            contract.setProfileImg(profile.get(i));
            contract.setName(name.get(i));

            // contract.setEmail(jobj.getString("email"));
            //   contract.setFromid(jobj.getString("friend_id"));

            feedsListall.add(contract);
        }
        setList();
    }

    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new AddGroupDetailsPublicAdapter(groupDetails.this, feedsListall);
            recycleView.setAdapter(adapterall);
            //recycleView.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //  getMenuInflater().inflate(R.menu.send, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.send_btn:
                if (TextUtils.isEmpty(groupTitle.getText().toString())) {
                    toast("please enter group title");

                } else {
                    Toast.makeText(groupDetails.this, "Group Created Successfully!!!", Toast.LENGTH_SHORT).show();
                    addGroup(groupIconFbURL, groupTitle.getText().toString());
                }


                //   Dosome();
                // startActivity(new Intent(this,AddGrouppublic.class).putExtra("name",namesend).putExtra("profile",profilesend));
                return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Choose option");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select image from ")
                .setCancelable(false)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(this.getFilesDir(), fileName);
        }
    }

    private void openGallery() {

        /* Intent chooseIntent = new Intent(Intent.ACTION_PICK,
         MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
         chooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
         startActivityForResult(chooseIntent, REQUEST_CODE_GALLERY);*/

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);

        // startActivity(new Intent(ContainerDetailActivity.this,
        // GalleryActivity.class));

    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(this, data.getData());

                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    //  profileImage.setImageBitmap(resized);
                    filepath = getPath(this, getImageUri(this, resized));
                    // Utils.loadImageRound(getActivity(),profileImage,filepath);
                    //amit commented uploadFile(filepath);

                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                //  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(this, addIcon, "file://" + mFileTemp.getPath());

                // MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {

                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                //filepath= getPath(data.getData());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filepath = getPath(this, getImageUri(this, resized));
                ///  MyApplication.loader.displayImage("file://" + mFileTemp.getPath(), profileImage, MyApplication.options);
                Utils.loadImageRound(this, addIcon, "file://" + mFileTemp.getPath());
                //   profileImage.setImageBitmap(resized);
                //amit commented, we dont want to upload before confirmation uploadFile(filepath);
            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        } else if (requestCode == Constants.LOCATION) {
            try {
               /* Bundle bundle = data.getExtras();
                EndRoute route = (EndRoute) bundle.getSerializable(Constants.object);
                latitude = route.getLat();
                longitude = route.getLng();
                address.setText(route.getAddress());*/
            } catch (Exception e) {

            }

        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void uploadFile(String bitmap, final HashMap <String, Object> groupData) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbchatTable);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(fbGroupIconStorage);
        StorageReference mountainImagesRef = storageRef.child(userId);

        /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);*/
        try {
            InputStream stream = new FileInputStream(new File(bitmap));
            UploadTask uploadTask = mountainImagesRef.putStream(stream);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    // sendMsg("" + downloadUrl, 2);
                    System.out.println("download url" + downloadUrl);
                    groupIconFbURL = downloadUrl + "";
                    Log.d("downloadUrl-->", "" + downloadUrl);
                    // addGroup(downloadUrl + "");
                    groupData.put("group_icon", groupIconFbURL);
                    updateGroupDetail(groupData);

                }
            });
        } catch (Exception e) {
                log("475- Exception uploading Icon: " + e.toString()); //if exception no update is fine..

        }





    }

    private void addGroup(String downloadurl, String title) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable);
            String userId = "grp_" + mDatabase.push().getKey();
            String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
            // String members = members;
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            members = members.substring(0, members.length() - 1);
            String grp_title = title;
            String isprivatestr = "";
            if (privatToggle.isChecked()) {
                isprivatestr = "public";
            } else {
                isprivatestr = "private";
            }
            // DatabaseReference mDatabase1 = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(userId);

            GroupChat sendMessage = new GroupChat(fromid, groupIconFbURL, isprivatestr, members, grp_title, tsLong);
            mDatabase.child(userId).setValue(sendMessage);

            addGroupMessage(userId);
           // notifyGroupMembers(members);
        } catch (Exception e) {
            System.out.println("exception " + e.toString());
        }


    }



    private void addGroupMessage(String key) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbGroupChatTable).child(key);
        String userId = mDatabase.push().getKey();
        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        // String toid = feeds.getFromid();

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        GroupSendMessage sendMessage = new GroupSendMessage("Group created", fromid, tsLong, "tag");
        mDatabase.child(userId).setValue(sendMessage);

        String currentUserName = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(currentUserName).child(fbchatTable);
        NewUser newUser = new NewUser(key);
        ref.child(key).setValue(newUser);

        for (int i = 0; i < friend_id.size(); i++) {
            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(friend_id.get(i)).child(fbchatTable);
            NewUser newUser1 = new NewUser(key);
            ref1.child(key).setValue(newUser1);
        }
        // Toast.makeText(groupDetails.this,"Group Created Successfully!!!",Toast.LENGTH_SHORT).show();
        finish();

    }

    private void getGroupDetails() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbGroupListTable).child(chatkey);

        Log.d("Firebase", "Chat key:"+chatkey+ " Ref:"+ ref.toString());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                // data[0] = dataSnapshot.getValue().toString();
                try {
                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                    //data[0] = new JSONObject(value).toString();

                          /*group information*/
                    final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));
                    groupName = jsonObject1.getString("name");
                    groupTitle.setText(groupName);
                    Utils.loadImageRoundGroup(groupDetails.this, addIcon, jsonObject1.getString("group_icon"));
                    membersFB = jsonObject1.getString("members").split(",");
                    if (jsonObject1.getString("isPrivate").compareTo("private") == 0) {
                        privatToggle.setChecked(true);
                    } else {
                        privatToggle.setChecked(false);
                    }
                    if (MyApplication.getUserID(Constants.UserID).compareTo(jsonObject1.optString("creator"))!=0 && ((jsonObject1.getString("isPrivate").compareTo("private") != 0))) {
                        privatToggle.setEnabled(false);
                        //groupTitle.setEnabled(false);
                        btnUpdate.setVisibility(View.GONE);
                    }
                        getAdminInfo(jsonObject1.optString("creator"));
                    getMemberInfo(jsonObject1.optString("creator"));
                } catch (Exception e) {
                    Helper.debugPrint("Firebase", "520-Unable to get group details.Exception:"+e.toString());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void getMemberInfo(final String admin_key) {
        for (int i = 0; i < membersFB.length; i++) {
            if (admin_key.compareTo(membersFB[i])!=0){
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(membersFB[i]).child("meet_credentials");

                final int finalI = i;
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                        // data[0] = dataSnapshot.getValue().toString();
                        try {
                            Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                            //data[0] = new JSONObject(value).toString();

                          /*group information*/
                            final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                            //    profilepic.add(jsonObject.getString("profilePicLink"));
                            // name.add(jsonObject.getString("name"));
                      /* name.add(jsonObject1.getString("name"));
                        profile.add(jsonObject1.getString("profilePicLink"));

                        if (finalI == membersFB.length - 1) {
                          getdata();
                        }
*/
                            //feedsListall.clear();

                            Bindcontract contract = new Bindcontract();
                            //  contract.setFriendid(jobj.getString("friend_id"));
                            contract.setProfileImg(jsonObject1.getString("profilePicLink"));
                            contract.setName(jsonObject1.getString("name"));
                            contract.setAdmin_key(admin_key);

                            // contract.setEmail(jobj.getString("email"));
                            //   contract.setFromid(jobj.getString("friend_id"));
                            feedsListall.add(contract);
                            emptyView.setVisibility(View.GONE);
                            adapterall = new AddGroupDetailsPublicAdapter(groupDetails.this, feedsListall);
                            recycleView.setAdapter(adapterall);

                            //  setList();


                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

        }

    }

    private void getAdminInfo(String adminkey) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(adminkey).child("meet_credentials");


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  System.out.println("credential data key for getdata " + dataSnapshot.getValue().toString());
                // data[0] = dataSnapshot.getValue().toString();
                try {
                    Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
//                    Log.i("dataSnapshot", "dataSnapshot" + new JSONObject(value));
                    //data[0] = new JSONObject(value).toString();

                          /*group information*/
                    final JSONObject jsonObject1 = new JSONObject(new JSONObject(value).toString());
                    //    profilepic.add(jsonObject.getString("profilePicLink"));
                    // name.add(jsonObject.getString("name"));
                      /* name.add(jsonObject1.getString("name"));
                        profile.add(jsonObject1.getString("profilePicLink"));

                        if (finalI == membersFB.length - 1) {
                          getdata();
                        }
*/
                    //feedsListall.clear();
                    Utils.loadImageRound(groupDetails.this, adminprofile, jsonObject1.getString("profilePicLink"));
                    adminName.setText(jsonObject1.getString("name"));

                    /*Bindcontract contract = new Bindcontract();
                    //  contract.setFriendid(jobj.getString("friend_id"));
                    contract.setProfileImg(jsonObject1.getString("profilePicLink"));
                    contract.setName(jsonObject1.getString("name"));

                    // contract.setEmail(jobj.getString("email"));
                    //   contract.setFromid(jobj.getString("friend_id"));
                    feedsListall.add(contract);
                    emptyView.setVisibility(View.GONE);
                    adapterall = new AddGroupDetailsPublicAdapter(groupDetails.this, feedsListall);
                    recycleView.setAdapter(adapterall);*/

                    //  setList();


                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    private void log(String msg)
    {
        Helper.debugPrint(TAG, msg);
    }

}
