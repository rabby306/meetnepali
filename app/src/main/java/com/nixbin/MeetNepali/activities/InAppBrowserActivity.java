package com.nixbin.MeetNepali.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.FindFriendAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.fragments.BaseFragment;
import com.nixbin.MeetNepali.fragments.FragmentChat;
import com.nixbin.MeetNepali.fragments.FragmentFindFriend;
import com.nixbin.MeetNepali.fragments.FriendList;
import com.nixbin.MeetNepali.fragments.FriendRequestList;
import com.nixbin.MeetNepali.utilities.Pref;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.ApiConfig;
import com.nixbin.MeetNepali.webutility.AppConfig;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.BindFindfriend;
import com.nixbin.MeetNepali.wrapper.SpinnerValue;
import com.nixbin.MeetNepali.wrapper.models.City;
import com.nixbin.MeetNepali.wrapper.models.CityResponse;
import com.nixbin.MeetNepali.wrapper.models.Country;
import com.nixbin.MeetNepali.wrapper.models.Data;
import com.nixbin.MeetNepali.wrapper.models.Datum;
import com.nixbin.MeetNepali.wrapper.models.Region;
import com.nixbin.MeetNepali.wrapper.models.RegionResponse;
import com.nixbin.MeetNepali.wrapper.models.RequestCity;
import com.nixbin.MeetNepali.wrapper.models.UserList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nixbin.MeetNepali.webutility.WebUrls.BASE_URL_image;
import static com.nixbin.MeetNepali.webutility.WebUrls.mApplicationType;

/**
 * Created by Advosoft2 on 3/31/2017.
 */

public class InAppBrowserActivity extends BaseFragment implements WebCompleteTask {
    WebView webV = null;
    Dialog Odialog;
    TextView locationtext,topName,recomandedFriends;
    private int SelectedCountryCode=0;
    LinearLayout newMessages,friendRequest,discussion,events,classifieds,bussiness;
    SpinnerValue[] country_sp,region_sp,city_sp;
    String countryTxT=null,regionTxT=null,cityTxT=null;
    String countryID=null,regionID=null,cityID=null;
    ImageView img_1,img_2,img_3;
    TextView fname_1,fname_2,fname_3;
    LinearLayout frequest_1,frequest_2,frequest_3;
    private List<Datum> FriendRequestData=new ArrayList<>();
    Pref pref;
    private  String APITOKEN = MyApplication.getAuthToken(Constants.API_TOKEN);


    public InAppBrowserActivity() {
    }


    public void replaceFragment(@IdRes int container, Fragment fragment) {
        replaceFragment(container, fragment, null);
    }

    public static InAppBrowserActivity getInstance(Bundle bundle) {
        InAppBrowserActivity chef = new InAppBrowserActivity();
        chef.setArguments(bundle);
        return chef;
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {

    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.inapp_browser, container, false);
        disableBackButton();
        setHasOptionsMenu(true);
        pref=new Pref(getContext());
        img_1=(ImageView)view.findViewById(R.id.img_1);
        img_2=(ImageView)view.findViewById(R.id.img_2);
        img_3=(ImageView)view.findViewById(R.id.img_3);
        fname_1=(TextView) view.findViewById(R.id.fname_1);
        fname_2=(TextView) view.findViewById(R.id.fname_2);
        fname_3=(TextView) view.findViewById(R.id.fname_3);
        webV = (WebView) view.findViewById(R.id.webView);
        newMessages = (LinearLayout) view.findViewById(R.id.newMessages);
        frequest_1 = (LinearLayout) view.findViewById(R.id.frequest_1);
        frequest_2 = (LinearLayout) view.findViewById(R.id.frequest_2);
        frequest_3 = (LinearLayout) view.findViewById(R.id.frequest_3);
        friendRequest = (LinearLayout) view.findViewById(R.id.friendRequest);
        //discussion,events,classifieds,bussiness
        discussion = (LinearLayout) view.findViewById(R.id.discussion);
        events = (LinearLayout) view.findViewById(R.id.events);
        classifieds = (LinearLayout) view.findViewById(R.id.classifieds);
        bussiness = (LinearLayout) view.findViewById(R.id.bussiness);

        topName=(TextView) view.findViewById(R.id.topName);
        recomandedFriends=(TextView) view.findViewById(R.id.recomandedFriends);
        topName.setText("Hey "+MyApplication.getUserName(Constants.UserName));
        locationtext = (TextView) view.findViewById(R.id.locationtext);
        WebSettings settings = webV.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webV.setWebViewClient(new WebViewClient());
        webV.setWebChromeClient(new WebChromeClient());
        String uri= WebUrls.DASHBOARD_URL; //amit modified originally it was hard coded.

        discussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        classifieds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        bussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Under Construction", Toast.LENGTH_SHORT).show();
            }
        });

        frequest_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FriendRequestData.size()>0)
                showAlertDialog(getContext(),0);
                else Toast.makeText(getContext(), "Not possible now ", Toast.LENGTH_SHORT).show();
            }
        });

        frequest_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FriendRequestData.size()>0)
                    showAlertDialog(getContext(),1);
                else Toast.makeText(getContext(), "Not possible now ", Toast.LENGTH_SHORT).show();
            }
        });

        frequest_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FriendRequestData.size()>0)
                    showAlertDialog(getContext(),2);
                else Toast.makeText(getContext(), "Not possible now ", Toast.LENGTH_SHORT).show();
            }
        });


        Map<String, String> extraHeaders = new HashMap<String, String>();
        extraHeaders.put("Authorization","Bearer "+ APITOKEN);
        webV.loadUrl(uri,extraHeaders);
        locationtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationDialog();
            }
        });
        locationtext.setText(pref.getString("city")+","+pref.getString("region")+","+pref.getString("country"));
        DownloadRecomandedFriends();
       // webV.loadUrl(uri);
        newMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.container, new FragmentChat());
            }
        });

        friendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.container, new FriendRequestList());
            }
        });
        recomandedFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.container, new FragmentFindFriend());
            }
        });

        return view;
    }


    public AlertDialog showAlertDialog(final Context context, final int cartId) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle("MeetNepali");
        alertBuilder.setMessage("Want to send FriendRequest ?");
        alertBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                    callapiAll(FriendRequestData.get(cartId).getUserId()+"","friend_request");

                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new FragmentFindFriend())
                        .commit();
            }
        });

        alertBuilder.setNegativeButton("Cancel", null);

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onResume() {
        super.onResume();
        setTitleOnAction("Home",true);

    }


    public AlertDialog showAlertDialog(final Context context, final String cartId, String msg, String title, final int methodchoose) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(msg);
        alertBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(methodchoose==1)
                    callapiAll(cartId,"friend_request");
                else
                    callapiAll(cartId,"delete_friend");
                /*replaceFragment(R.id.container, new FragmentFindFriend());

                Fragment currentFragment =context. getFragmentManager().find("YourFragmentTag");
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(currentFragment);
                fragmentTransaction.attach(currentFragment);
                fragmentTransaction.commit();*/

                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new FragmentFindFriend())
                        .commit();
            }
        });

        alertBuilder.setNegativeButton("Cancel", null);

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }


    private void callapiAll(String friendid,String method) {
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", method);
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("friend_id", friendid);
            jobj.put("data", data);
        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        Log.w("Rabby",  " JSON: " + new Gson().toJson(object));
        new WebTask(getContext(), WebUrls.API_URL, object, InAppBrowserActivity.this, RequestCode.CODE_AddFriend, 1);

    }

    private void showLocationDialog() {
        final Spinner CountrySp,RegionSp,CitySp;
        Odialog = new Dialog(getActivity());
        Odialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        Odialog.setContentView(R.layout.location_dialog);
        Odialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        Odialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Odialog.getWindow().setDimAmount(0.8f);
        Odialog.setCancelable(false);
        ((Button) Odialog.findViewById(R.id.cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Odialog.cancel();
            }
        });

        ((Button) Odialog.findViewById(R.id.confirm_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(countryTxT!=null && regionTxT!=null && cityTxT !=null){
                    pref.set("country",countryTxT);
                    pref.set("region",regionTxT);
                    pref.set("city",cityTxT);
                pref.set("country_id",countryID);
                    pref.set("region_id",regionID);
                    pref.set("city_id",cityID);}
                locationtext.setText(pref.getString("city")+","+pref.getString("region")+","+pref.getString("country"));
                DownloadRecomandedFriends();
                Odialog.cancel();
            }
        });

        CountrySp=(Spinner) Odialog.findViewById(R.id.country_spinner);
        RegionSp=(Spinner) Odialog.findViewById(R.id.region_spinner);
        CitySp=(Spinner) Odialog.findViewById(R.id.city_spinner);

        TextView curentAddress=(TextView) Odialog.findViewById(R.id.curentAddress);
        curentAddress.setText(pref.getString("city")+","+pref.getString("region")+","+pref.getString("country"));

        CountrySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = country_sp[position];
                countryTxT=value.displayText;
                countryID=value.valueText;
                SelectedCountryCode=Integer.parseInt(value.valueText);
                DownloadRegion(RegionSp,SelectedCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        RegionSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = region_sp[position];
                regionTxT=value.displayText;
                regionID=value.valueText;
                DownloadCity(CitySp,SelectedCountryCode,Integer.parseInt(value.valueText));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        CitySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = city_sp[position];
                cityTxT=value.displayText;
                cityID=value.valueText;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        // bind(((Spinner) Odialog.findViewById(R.id.country_spinner)),country_sp);
        DownloadCountry(CountrySp);
        /*bind(((Spinner) Odialog.findViewById(R.id.region_spinner)),region_sp);
        bind(((Spinner) Odialog.findViewById(R.id.city_spinner)),city_sp);*/
        Odialog.show();
    }

    public void bind(Spinner spinner, SpinnerValue[] spinnerValue) {
        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_drop, spinnerValue);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    private void DownloadCountry(final Spinner sp)
    {
        ArrayList<Country> cities=new ArrayList<>();
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_countries");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);

        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<List<Country>> call = getResponse.GetCountryData(mApplicationType,requestCity);
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "Country Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(getActivity());
                        db.InsertCountryBulk(response.body());*/
                        country_sp=new SpinnerValue[response.body().size()];
                        for(int i=0;i<response.body().size();i++)
                        {
                            Country country=response.body().get(i);
                            country_sp[i] =new SpinnerValue(country.getCountryName(), String.valueOf(country.getId()));
                        }
                        bind(sp,country_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                System.out.println("fail response.." + t.toString());
            }
        });
    }

    private void DownloadRegion(final Spinner sp ,int countryId)
    {
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_regions");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setCountryId(countryId);
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<RegionResponse> call = getResponse.GetRegionData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "Region Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(getActivity());
                        db.InsertCountryBulk(response.body());*/
                        region_sp=new SpinnerValue[response.body().getData().getRegions().size()];
                        for(int i=0;i<response.body().getData().getRegions().size();i++)
                        {
                            Region region=response.body().getData().getRegions().get(i);
                            region_sp[i] =new SpinnerValue(region.getRegion(), String.valueOf(region.getId()));
                        }
                        bind(sp,region_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }

    private void DownloadCity(final Spinner sp ,int countryId,int RegionId)
    {
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_cities");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setCountryId(countryId);
        data.setRegionId(RegionId);
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<CityResponse> call = getResponse.GetCityData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "City Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(getActivity());
                        db.InsertCountryBulk(response.body());*/
                        city_sp=new SpinnerValue[response.body().getData().getCities().size()];
                        for(int i=0;i<response.body().getData().getCities().size();i++)
                        {
                            City city=response.body().getData().getCities().get(i);
                            city_sp[i] =new SpinnerValue(city.getCityName(), String.valueOf(city.getId()));
                        }
                        bind(sp,city_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }

    private void DownloadRecomandedFriends()
    {
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("people_nearby");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("");
        data.setCountry(pref.getString("country_id"));
        data.setState(pref.getString("region_id"));
        data.setCity(pref.getString("city_id"));
        data.setPageNumber("1");
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<UserList> call = getResponse.GetRecomandedUserData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {

                if (response.isSuccessful()) {
                    try {
                        FriendRequestData.clear();
                        Log.w("Rabby",  "City Response: " + new Gson().toJson(response.body()));
                         Random randomGenerator = new Random();
                        int index = randomGenerator.nextInt(response.body().getData().size()-3);

                        FriendRequestData.add(response.body().getData().get(index));
                        fname_1.setText(response.body().getData().get(index).getName());
                        Utils.loadImageRound(getContext(), img_1, BASE_URL_image + response.body().getData().get(index).getProfileImg());

                        FriendRequestData.add(response.body().getData().get(index+1));
                        fname_2.setText(response.body().getData().get(index+1).getName());
                        Utils.loadImageRound(getContext(), img_2, BASE_URL_image + response.body().getData().get(index+1).getProfileImg());

                        FriendRequestData.add(response.body().getData().get(index+2));
                        fname_3.setText(response.body().getData().get(index+2).getName());
                        Utils.loadImageRound(getContext(), img_3, BASE_URL_image + response.body().getData().get(index+2).getProfileImg());


                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {

                //Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }

    @Override
    public void onComplete(String response, int taskcode) {
    }
}

