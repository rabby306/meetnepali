package com.nixbin.MeetNepali.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.User;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;

/**
 * Created by Advosoft2 on 9/5/2017.
 */
public class LoginActivity extends BaseActiivty implements WebCompleteTask {
    private static final String TAG = LoginActivity.class.getSimpleName();
    @Bind(R.id.userid)
    EditText userid;
    @Bind(R.id.pwd)
    EditText pwd;
    @Bind(R.id.submit)
    Button submit;
    @Bind(R.id.createaccount)
    TextView createaccount;
    @Bind(R.id.forgot)
    TextView forgot;
    Boolean check = false;
    private FirebaseAuth mAuth;
    String firebaseDataBAseID;
    String username, useremail;
    String[] emailid;
    String profile_img="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            Log.d(TAG, extras.toString());
            String registered = extras.getString("registration");
            Log.d(TAG, "Extras:"+registered);
            if (registered == "registered") {
                TextView regMsg = (TextView) getParent().findViewById(R.id.registermsg);
                regMsg.setVisibility(View.VISIBLE);
            }
        }
        userid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    userid.setHint("");
                else
                    userid.setHint("Email");
            }
        });
        pwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    pwd.setHint("");
                else
                    pwd.setHint("Password");
            }
        });

        mAuth = FirebaseAuth.getInstance();
        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSubmitTask();
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotDialog();
            }
        });

    }

    private void callSubmitTask() {
        //  Toast.makeText(SignUpActivity.this, finalMobile, Toast.LENGTH_SHORT).show();
        if (TextUtils.isEmpty(userid.getText().toString())) {
            userid.setError(getString(R.string.notempty));
            userid.requestFocus();
        } else if (pwd.getText().toString().isEmpty()) {
            pwd.setError(getString(R.string.enter_passowrg));
            pwd.requestFocus();

        } else {
            JSONObject jobj = new JSONObject();
            try {

                jobj.put("method", "login");
                JSONObject data = new JSONObject();
                data.put("email", userid.getText().toString().trim());
                data.put("password", pwd.getText().toString().trim());
                data.put("device_type", "android");
                data.put("device_token", MyApplication.getUserData(Constants.GCM_ID));
                data.put("device_id", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
                jobj.put("data", data);
                System.out.println("json data " + jobj.toString());

            } catch (Exception e) {
                Log.d(TAG, "Couldn't get Device Token:" + e.toString());

            }
            HashMap object = new HashMap();
            object.put("request", jobj.toString());
           /* object.put("username",userid.getText().toString().trim());
            object.put("password", pwd.getText().toString().trim());*/
            new WebTask(LoginActivity.this, WebUrls.API_URL, object, LoginActivity.this, RequestCode.CODE_Login, 1);

        }
    }

    private void forgotDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forget_pass);
        getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        RippleView forgotBtn = (RippleView) dialog.findViewById(R.id.loginBtn);
        final EditText etEmail = (EditText) dialog.findViewById(R.id.etEmail);


        /* commented because we are not using phone/email option,only email

        final Button btn_email = (Button) dialog.findViewById(R.id.btn_email);
        final Button btn_phn = (Button) dialog.findViewById(R.id.btn_phn);

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check = false;
                btn_email.setBackgroundColor(getResources().getColor(R.color.green));
                btn_phn.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                etEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail, 0, 0, 0);
                etEmail.setHint("Email Address");
            }
        });

        btn_phn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check = true;
                btn_email.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                btn_phn.setBackgroundColor(getResources().getColor(R.color.green));
                etEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.phone, 0, 0, 0);
                etEmail.setHint("Mobile Number");
            }
        });
        */

        forgotBtn.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    etEmail.setError(getString(R.string.notempty));
                    etEmail.requestFocus();
                } else {
                    callForget(etEmail.getText().toString());
                    Log.d(TAG, "Email:"+ etEmail.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void callForget(String email) {
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "forgot_password");
            JSONObject data = new JSONObject();

          /* This is for phone/email option
            if (check == false) {
                data.put("type", "email");
            } else {
                data.put("type", "phone");
            }
            */

            data.put("value", email);

            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(LoginActivity.this, WebUrls.API_URL, object, LoginActivity.this, RequestCode.CODE_Forogot, 1);

    }

    @Override
    public void onComplete(String response, int taskcode) {
        System.out.println("response " + response);
        if (taskcode == RequestCode.CODE_Login) {
            if (response != null) {
                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getString("status").compareTo("success") == 0) {
                       // Toast.makeText(LoginActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        JSONObject loginResponse = obj.getJSONObject("data");
                        MyApplication.setUserLoginStatus(LoginActivity.this, true);
                        MyApplication.setUserID(Constants.UserID, loginResponse.getString("user_id"));
                        MyApplication.setUserName(Constants.UserName, loginResponse.getString("name"));
                        MyApplication.setUserDOB(Constants.UserNameDOB, loginResponse.getString("dob"));
                        MyApplication.setUserEmail(Constants.UserEmail, loginResponse.getString("email"));
                        MyApplication.setUserMobile(Constants.UserMobile, loginResponse.getString("phone"));
                        MyApplication.setProfileImage(LoginActivity.this, loginResponse.getString("profile_img"));
                        MyApplication.saveAuthToken(Constants.API_TOKEN, loginResponse.getString("api_token"));
                        username = loginResponse.getString("name");
                        useremail = loginResponse.getString("email");
                        profile_img=loginResponse.getString("profile_img");

                        //firebase id added
                        MyApplication.setFirebaseID(Constants.FirebaseId, loginResponse.getString("user_id"));

/*
                        mAuth.signInWithEmailAndPassword(userid.getText().toString(), pwd.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {

                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d("Sucess", "createUserWithEmail:success");
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    firebaseDataBAseID = user.getUid();

                                    Log.d("firebase_id", firebaseDataBAseID);
                                    MyApplication.setFirebaseID(Constants.FirebaseId, firebaseDataBAseID);



                            }
                        });*/
                        callApi(loginResponse.getString("token").trim());


                    } else if (obj.getString("status").compareTo("fail") == 0) {
                        Toast.makeText(LoginActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        /*if (obj.getString("message").compareTo("Firebase Authentication fail") == 0) {
                            JSONObject obj2 = obj.getJSONObject("data");
                            callApi(obj2.getString("token").trim());
                        } else {
                            Toast.makeText(LoginActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        }*/

                    }


                } catch (Exception e) {
                    System.out.println("exception..." + e.toString());
                }
            }

        }
        if (taskcode == RequestCode.Code_auth) {
            if (response != null) {
                System.out.println("response auth " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    //  Toast.makeText(SignUpActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                    if (obj.getString("status").compareTo("success") == 0) {

                        createUser(userid.getText().toString(), userid.getText().toString());
                        //  JSONObject obj2 = obj.getJSONObject("data");


                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                } catch (Exception e) {

                }
            }
        }

        if (taskcode == RequestCode.CODE_Forogot) {
            if (response != null) {
                try {
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(LoginActivity.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {

                }
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        Log.d("MeetLog", "Fb User " + currentUser);
        //  updateUI(currentUser);
    }

    private void callApi(String mCustomToken) {


        mAuth.signInWithCustomToken(mCustomToken)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            firebaseDataBAseID = user.getUid();
                            createUser(userid.getText().toString(), userid.getText().toString());
                             Toast.makeText(LoginActivity.this, "Successfully Logged in !"
                                     , Toast.LENGTH_SHORT).show();
                            FirebaseMessaging.getInstance().subscribeToTopic(MyApplication.GLOBAL_TOPIC);
                            FirebaseMessaging.getInstance().subscribeToTopic(MyApplication.ANDROID_TOPIC);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                           // callApilocalauth();


                        } else {
                            // If sign in fails, display a message to the user.
                            System.out.println("auth fail error" + task.getException());
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed,Please ensure your account has been activated.",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    }

    private void callApilocalauth() {

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", "firebase_check");
            JSONObject data = new JSONObject();
            data.put("status", "true");
            data.put("user_id", firebaseDataBAseID);
            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(LoginActivity.this, WebUrls.API_URL, object, LoginActivity.this, RequestCode.Code_auth, 1);


    }


    private void createUser(String name, String email) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
      /*  if (TextUtils.isEmpty(userId)) {
            userId = mFirebaseDatabase.push().getKey();
        }*/
        emailid=email.split("@");

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.usericon);

        setUserValue(WebUrls.BASE_URL_image+profile_img);
      //  uploadFile(largeIcon);


    }


    private void uploadFile(Bitmap bitmap) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(firebaseDataBAseID);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://meetnepali-4f1d5.appspot.com/usersProfilePics/");
        StorageReference mountainImagesRef = storageRef.child(userId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                // sendMsg("" + downloadUrl, 2);
                System.out.println("download url" + downloadUrl);
                Log.d("downloadUrl-->", "" + downloadUrl);
                setUserValue(downloadUrl + "");
            }
        });

    }

    private void setUserValue(String downloadurl) {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(firebaseDataBAseID);

        User user = new User(emailid[0],  downloadurl);
        mDatabase.child("meet_credentials").setValue(user);
    }


}
