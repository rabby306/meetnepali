package com.nixbin.MeetNepali.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nixbin.MeetNepali.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.nixbin.MeetNepali.adpter.PlaceAutocompleteAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.wrapper.Config;
import com.nixbin.MeetNepali.wrapper.EndRoute;
import com.nixbin.MeetNepali.wrapper.FetchAddressIntentService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Advosoft2 on 4/13/2017.
 */

public class SearchAddressActivity extends BaseActiivty implements  View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static String TAG= "Search Location";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_address);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.serach_location));
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#7a7a7a7a\">" + getString(R.string.serach_location) + "</font>"));
      //  setTitle(TAG);
       // enableBackButton();
        initControls(savedInstanceState);
        setValueOnUI();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setValueOnUI() {
        ArrayList<EndRoute> arrRoute = new ArrayList<EndRoute>();
    }

    private void initControls(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getBundleExtra(Constants.bundleArg);
        if(bundle != null){
            position = bundle.getInt(Constants.position);
        }
        initAutoSearch();
    }


    public static final int TAKEPIC = 0X003, REQUEST_CAMERA = 0x004,
            Map_RESULT = 0x001, ADD_ADDRSS = 0x002, ADD_SUBCAT = 0x005,
            EDIT_ADDRSS = 0x006, ADD_HOURS = 0x007;
    static final String APP_KEY = "yaaohdkvb5s0b9b";
    static final int DBX_CHOOSER_REQUEST = 0;  // You can change this if needed
    public static Uri uri_name = Uri.parse("android.resource://com.findme/drawable/upload_pic");
    //public ArrayList<ImageUris> uris = new ArrayList<>();
    ArrayList<String> deletedIds = new ArrayList<>();
    private TextView navigation_title, pricerange, addmorebusinessaddress, categorytab, subcategorytab, txtUploadMenu;
    //private SimpleSideDrawer mNav;
    private ImageView menuicon, navigation_back_button;
    private ImageView map_icon;
    private GridView gridview;
    //private ImageUploadAdapter adapter;
    private Dialog dialog, imagediaog;

    private Uri fileUri;
    private int position;
    private ViewPager viewPager;
    //private CustomAdapter adapterviewpager;
    private ScrollView scroll;
    private String result;
    private JSONObject json;

    private AutoCompleteTextView locationenglish;
    private LatLng latilongi = null;
    private String selectedPrice = "";

    private int count = 0;




    private String[] arr = {"Economic", "Moderate", "Luxury"};
    private JSONArray businesshedule;
    private JSONArray businessaddressarray;
    private JSONArray businessCat;

    private RelativeLayout locationlayout;
    private RelativeLayout addmorelayout;



    private String LOGTAG = "";
    private boolean isLocPermAsked = false;
    private JSONArray jsonArray = null;

    private String selectedAddress = "";
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    /**
     * Tracks whether the user has requested an address. Becomes true when the user requests an
     * address and false when the address (or an error message) is delivered.
     * The user requests an address by pressing the Fetch Address button. This may happen
     * before GoogleApiClient connects. This activity uses this boolean to keep track of the
     * user's intent. If the value is true, the activity tries to fetch the address as soon as
     * GoogleApiClient connects.
     */
    protected boolean mAddressRequested;
    /**
     * The formatted location address.
     */
    protected String mAddressOutput;
    /**
     * Receiver registered with this activity to get the response from FetchAddressIntentService.
     */
    private AddressResultReceiver mResultReceiver;
    private boolean isCurrentAddressFoundOnce = false;
    private long searchActionCounter1 = 0;
    private int mGeocoderRetryCount = -1;
    private PlaceAutocompleteAdapter mPlaceAdapter;
    private boolean isEditMode = false;

    private void initAutoSearch(){
        buildGoogleApiClient();

        mResultReceiver = new AddressResultReceiver(new Handler());
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        verifyLocationAvailability();
        initializeEdittext();
    }

    private void verifyLocationAvailability() {

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Check if location settings are enabled and if not send user to the settings page
        if (!isGPSEnabled && !isNetworkEnabled) {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.KITKAT) {
                int mode;
                try {
                    mode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
                    if (mode != 3) {
                        showSettingsAlert();
                    }
                } catch (Settings.SettingNotFoundException e) {
                    Log.e(TAG, e.getMessage());
                }
            } else {
                showSettingsAlert();
            }
        } else if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (!isLocPermAsked) {
                    ActivityCompat.requestPermissions(this, Config.PERMISSIONS_LOCATION, Config.PERMISSIONS_REQUEST_LOCATION);
                    isLocPermAsked = true;
                }
            } else {
                startLocationSearchTask();
            }
        } else {
            startLocationSearchTask();
        }

    }

    private void startLocationSearchTask() {
        if (!isEditMode) {
            isCurrentAddressFoundOnce = false;
            mAddressRequested = false;
            mAddressOutput = "";
            if(locationenglish != null)
                locationenglish.setText("");
            fetchAddressHandler();
        }
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will launch Settings Options
     */
    public void showSettingsAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                SearchAddressActivity.this.startActivityForResult(intent, 10);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                SearchAddressActivity.this.finish();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void initializeEdittext() {
        locationenglish = (AutoCompleteTextView) findViewById(R.id.address);

        AutocompleteFilter.Builder builder = new AutocompleteFilter.Builder();
        builder.setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE);
        builder.build();
        mPlaceAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, null, builder.build());
        locationenglish.setAdapter(mPlaceAdapter);

        // Register a listener that receives callbacks when a suggestion has been selected
        locationenglish.setOnItemClickListener(mAutocompleteClickListener);
    }

    private void openMap() {

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Check if location settings are enabled and if not send user to the settings page
        if (!isGPSEnabled && !isNetworkEnabled) {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.KITKAT) {
                int mode;
                try {
                    mode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
                    if (mode != 3) {
                        showSettingsAlert();
                    }
                } catch (Settings.SettingNotFoundException e) {
                    Log.e(TAG, e.getMessage());
                }
            } else {
                showSettingsAlert();
            }
        } else {
            Intent i = new Intent(this, SearchAddressActivity.class);
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            i.putExtra("location_details", latLng);
            startActivityForResult(i, Map_RESULT);
        }

    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput() {

        if (TextUtils.isEmpty(locationenglish.getText().toString()) && !isCurrentAddressFoundOnce) {
            selectedAddress = mAddressOutput;
            locationenglish.setFocusable(false);
            locationenglish.setFocusableInTouchMode(false);
            locationenglish.setText(selectedAddress);
            locationenglish.setFocusable(true);
            locationenglish.setFocusableInTouchMode(true);
            Utils.hideKeyBoardLocation(SearchAddressActivity.this, locationenglish);
            Log.i(TAG, mAddressOutput);
            isCurrentAddressFoundOnce = true;
        }
    }

    /**
     * Builds a GoogleApiClient. Uses {@code #addApi} to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService() {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Config.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Config.LOCATION_DATA_EXTRA, mLastLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    /**
     * Starts the service to fetch the address if GoogleApiClient is connected.
     */
    public void fetchAddressHandler() {
        // We only start the service to fetch the address if GoogleApiClient is connected.
        if (mGoogleApiClient.isConnected() && mLastLocation != null) {
            mGeocoderRetryCount = 3;
            startIntentService();
        } else {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        }
        // If GoogleApiClient isn't connected, we process the user's request by setting
        // mAddressRequested to true. Later, when GoogleApiClient connects, we launch the service to
        // fetch the address. As far as the user is concerned, pressing the Fetch Address button
        // immediately kicks off the process of getting the address.
        mAddressRequested = true;
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        try {
            // Gets the best and most recent location currently available, which may be null
            // in rare cases when a location is not available.
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(1000);
            mLocationRequest.setFastestInterval(100);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(1);
			/*LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
				@Override
				public void onLocationChanged(Location location) {
					mLastLocation = location;
					LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

					if (mLastLocation != null) {
						// Determine whether a Geocoder is available.
						if (!Geocoder.isPresent()) {
							//Toast.makeText(AddBusinessActivity.this, R.string.no_geocoder_available, Toast.LENGTH_LONG).show();
							return;
						}
						// It is possible that the user presses the button to get the address before the
						// GoogleApiClient object successfully connects. In such a case, mAddressRequested
						// is set to true, but no attempt is made to fetch the address (see
						// fetchAddressButtonHandler()) . Instead, we start the intent service here if the
						// user has requested an address, since we now have a connection to GoogleApiClient.
						if (mAddressRequested && !isCurrentAddressFoundOnce) {
							startIntentService();
						}
					}
				}
			});*/
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLastLocation = location;
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    if (mLastLocation != null) {
                        // Determine whether a Geocoder is available.
                        if (!Geocoder.isPresent()) {
                            Utils.showToast(SearchAddressActivity.this, getString(R.string.notAvilable));
                            return;
                        }
                        // It is possible that the user presses the button to get the address before the
                        // GoogleApiClient object successfully connects. In such a case, mAddressRequested
                        // is set to true, but no attempt is made to fetch the address (see
                        // fetchAddressButtonHandler()) . Instead, we start the intent service here if the
                        // user has requested an address, since we now have a connection to GoogleApiClient.
                        if (mAddressRequested && !isCurrentAddressFoundOnce) {
                            startIntentService();
                        }
                    }
                }
            });
        } catch (SecurityException se) {
            Log.e(LOGTAG, se.toString());
        } catch (Exception e) {
            Log.e(LOGTAG, e.toString());
        }



    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        if (result.getErrorCode() == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(this).create();
            alertDialog.setMessage("The installed version of Google Play services is out of date.");
            alertDialog.setCancelable(false);
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (mGoogleApiClient != null) {
                        if (mGoogleApiClient.isConnected()) {
                            mGoogleApiClient.disconnect();
                        }
                    }
                }
            });
            alertDialog.show();
        }

    }

    @Override
    public void onClick(View v) {

    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     **/
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(Config.RESULT_DATA_KEY);

            // Show a toast message if an address was found.
            if (resultCode == Config.SUCCESS_RESULT) {
                displayAddressOutput();
            } else if (resultCode == Config.FAILURE_RESULT) {
                //TODO : Handle Retry.............
                if (mGeocoderRetryCount > 0) {
                    if (mAddressRequested && !isCurrentAddressFoundOnce) {
                        startIntentService();
                    }
                } else {
                    selectedAddress = "";
                    locationenglish.setFocusable(false);
                    locationenglish.setFocusableInTouchMode(false);
                    locationenglish.setText("Failed to find current location");
                    locationenglish.setFocusable(true);
                    locationenglish.setFocusableInTouchMode(true);
                    Utils.hideKeyBoardLocation(SearchAddressActivity.this, locationenglish);
                    isCurrentAddressFoundOnce = true;
                    //loc_mini_progressbar.setVisibility(View.GONE);
                }
                mGeocoderRetryCount--;
            }
        }
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mPlaceAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
            */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);


        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
            selectedAddress = String.valueOf(place.getAddress());
            locationenglish.setText(selectedAddress);
            locationenglish.setSelection(0);
            new GetLocationFromAddress(SearchAddressActivity.this).execute();
            Log.i(TAG, "Place details received: " + place.getName());
            places.release();
        }
    };

    private class GetLocationFromAddress extends AsyncTask<Void, Void, EndRoute> {

        ProgressDialog mPrgDialog;

        public GetLocationFromAddress(Activity activity) {
            super();
            mPrgDialog = new ProgressDialog(activity);
            mPrgDialog.setMessage("Loading Address...");
        }

        @Override
        protected void onPreExecute() {
            try {
                if (mPrgDialog != null) {
                    if (!mPrgDialog.isShowing()) {
                        mPrgDialog.show();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        };

        @Override
        protected EndRoute doInBackground(Void... params) {
            try {
                String locUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + selectedAddress.replace(" ", "") + "&sensor=false";
                String data = downloadUrl(locUrl);
                if (data != null) {
                    JSONObject jObject;
                    JSONArray jPlaces = null;
                    jObject = new JSONObject(data);
                    jPlaces = jObject.getJSONArray("results");
                    JSONObject jObj = jPlaces.getJSONObject(0);
                    JSONObject location = jObj.getJSONObject("geometry").getJSONObject("location");

                    double lat = location.getDouble("lat");
                    double lng = location.getDouble("lng");


                    Log.d("latlongonmap",""+lat);
                    Log.d("latlongonmap2",""+lng);


                    MyApplication.getInstance().setCurrentLatitude(lat);
                    MyApplication.getInstance().setCurrentLongitude(lng);
                    System.out.println("By best provider lat : " + lat + " lon : " + lng);
                    Log.d("lat","" + MyApplication.getInstance().getCurrentLatitude());
                    Log.d("long","" + MyApplication.getInstance().getCurrentLongitude());


                    mLastLocation.setLatitude(lat);
                    mLastLocation.setLongitude(lng);

                    EndRoute route  = new EndRoute();
                    route.setAddress(selectedAddress);
                    route.setLat(lat);
                    route.setLng(lng);

                    return  route;
                }
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(EndRoute route) {
            try {
                if (mPrgDialog != null) {
                    if (mPrgDialog.isShowing()) {
                        mPrgDialog.dismiss();
                    }
                }

                if(route != null){
                    Intent intent = new Intent();
                    intent.putExtra(Constants.object, route);
                    intent.putExtra(Constants.position, position);
                    setResult(Activity.RESULT_OK, intent);
                    Utils.hideKeyBoardLocation(SearchAddressActivity.this, locationenglish);
                    hideKeyboard();
                    finish();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        };
    };

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            if (br != null)
                br.close();
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        } finally {
            if (iStream != null)
                iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
