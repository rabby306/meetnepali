package com.nixbin.MeetNepali.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.fragments.FragmentFindFriend;
import com.nixbin.MeetNepali.fragments.FriendList;
import com.nixbin.MeetNepali.fragments.FriendRequestList;
import com.nixbin.MeetNepali.fragments.FriendRequestReceivedList;

import java.util.ArrayList;
import java.util.List;


public class FriendsActivity extends BaseActiivty {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_request_tablayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back_arrow));
        enableBackButton();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }


    private void setupViewPager(ViewPager viewPager) {
        FriendsActivity.ViewPagerAdapter adapter = new FriendsActivity.ViewPagerAdapter(getSupportFragmentManager());
        // adapter.addFragment(new FriendList(), "Friend");
        adapter.addFragment(new FragmentFindFriend(), "Recommended");
        adapter.addFragment(new FriendList(), "My Friends");
        adapter.addFragment(new FriendRequestList(), "Friend Requests");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                /*if (getIntent().getExtras()!=null){
                    if (getIntent().getExtras().getString("activity").compareTo("mainactivity")==0){
                        replaceFragment(R.id.container, new HomepageNew());
                    }else if (getIntent().getExtras().getString("activity").compareTo("fromchat")==0){
                        finish();
                    }else {
                        finish();
                    }
                }else {
                    finish();
                }*/
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
