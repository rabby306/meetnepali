package com.nixbin.MeetNepali.activities;

import android.Manifest.permission;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.views.ValidationClass;
import com.nixbin.MeetNepali.wrapper.EndRoute;
import com.nixbin.MeetNepali.wrapper.MyLocation;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Advosoft2 on 4/13/2017.
 */

public class MapGetLocActivity extends AppCompatActivity
        implements

        OnMapReadyCallback,
        LocationListener,
        /*GoogleMap.OnCameraChangeListener,*/
        View.OnClickListener {

    private final static String ARG_LAT = "_latitude";
    private final static String ARG_LGN = "_longitude";
    private final static String ARG_ADD = "_address";

    private final static String DEBUG_TAG = "MainActivity";
    private static int CAMERA_MOVE_REACT_THRESHOLD_MS = 750;
    private long lastCallMs = Long.MIN_VALUE;

    private GoogleMap mMap;
    @Bind(R.id.address)
    TextView etAddress;


    private double latitude;
    private double longitude;

    private boolean firstUpdate = false;
    private Geocoder geocoder;
    private CameraPosition cameraPosition;
    //private Address cAddress;

    // Pin Update

    // private ProgressWheel   wheel;
    //   private TextView nbTaxi;
    private String _nbTaxi;
    private String _minTaxi;

    //Search Bar
    //private TextView        pinLocation;
    //private TextView        currentLocation;

    //Zoom Mode
    //private boolean isSelected;
    private float cZoom = 15;
    //private Drawable cToolbarIcon;
    ProgressDialog pro = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        ButterKnife.bind(this);
       getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.ractangle_corner_gray_actionbar_cut));
        getSupportActionBar().setTitle(getResources().getString(R.string.serach_location));
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#7a7a7a7a\">" + getString(R.string.serach_location) + "</font>"));

       /* Intent i = getIntent();
        LatLng ll = i.getParcelableExtra("longLat_dataPrivider");
        latitude = ll.latitude;
        longitude = ll.longitude;
        moveCamera(latitude, longitude);*/

        firstUpdate = false;
        //isSelected = false;
        geocoder = new Geocoder(this, Locale.getDefault());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        pro = ValidationClass.showProgress(this, "", "Please wait...");
        pro.setCancelable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.filter, menu);
        return  true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.actionDone:
                setResultToLocation();
                return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        boolean isFine = ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean isCoarse = ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (isFine && isCoarse) {
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION}, fineReq);
        }


        //mMap.setOnCameraChangeListener(this);

        Intent intent = getIntent();
        if(intent != null &&  intent.getExtras() != null) {
            latitude = intent.getExtras().getDouble(ARG_LAT);
            longitude = intent.getExtras().getDouble(ARG_LGN);
            LatLng cPos = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cPos));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }
        /*else  {
            LatLng ll = intent.getParcelableExtra("longLat_dataPrivider");
            latitude = ll.latitude;
            longitude = ll.longitude;
            moveCamera(latitude, longitude);
        }*/
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                getAddressName(latLng.latitude, latLng.longitude);

                Log.d("latlongonmap",""+latLng.latitude);
                Log.d("latlongonmap2",""+latLng.longitude);


                MyApplication.getInstance().setCurrentLatitude(latLng.latitude);
                MyApplication.getInstance().setCurrentLongitude(latLng.longitude);
                System.out.println("By best provider lat : " + latLng.latitude + " lon : " + latLng.longitude);
                Log.d("lat","" +MyApplication.getInstance().getCurrentLatitude());
                Log.d("long","" + MyApplication.getInstance().getCurrentLongitude());

            }
        });

        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);
    }

    /*public void onCameraChange(CameraPosition _cameraPosition) {
        final long snap = System.currentTimeMillis();
        if (lastCallMs + CAMERA_MOVE_REACT_THRESHOLD_MS > snap) {
            //Log.d(DEBUG_TAG, "Delay");
            lastCallMs = snap;
            return;
        }

        lastCallMs = snap;
        cameraPosition = _cameraPosition;

       *//* if (cameraPosition.target.latitude != 0.0 && cameraPosition.target.longitude != 0.0) {
            getAddressName(cameraPosition.target.latitude, cameraPosition.target.longitude);
        }*//*
    }*/
    com.nixbin.MeetNepali.wrapper.Address addressObj = null;
    private void getAddressName(double latitude, double longitude){




        List<Address> addresses = null;
        try {
            addressObj = new com.nixbin.MeetNepali.wrapper.Address();
            final StringBuffer sb= new StringBuffer();
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0);
            sb.append(address);
            addressObj.setLocationName(address);

            String subAdd = addresses.get(0).getAddressLine(1);// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            if(subAdd != null && !subAdd.isEmpty()) {
                sb.append(", ");
                sb.append(subAdd);
            }
            addressObj.setStreetName(subAdd);

            String city = addresses.get(0).getLocality();
            if(city != null && !city.isEmpty()) {
                sb.append(", ");
                sb.append(city);
            }

            addressObj.setCity(city);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String s = sb.toString();
                    etAddress.setText(s);
                }
            });

            // currentLocation.setText(address+", "+city);
            //if (!isSelected) {
            //  nbTaxi.setVisibility(View.INVISIBLE);
            //   wheel.setVisibility(View.VISIBLE);
            //   getTaxi();
            moveCamera(latitude , longitude);
            //}
            Log.d("setOnCameraChange", address + " " + city);
            // Log.d("setOnCameraChange", "Latitude : " + cameraPosition.target.latitude + " Longitude : " + cameraPosition.target.longitude);
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // onClick Selector
    @OnClick({R.id.address})
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.address:{
                startActivityForResult(new  Intent(this, SearchAddressActivity.class), Constants.LOCATION);
            }
            break;
        }
    }

    private  void setResultToLocation(){
        if(etAddress.getText().toString().length() == 0)
            getAddressName(latitude, longitude);

        if(etAddress.getText().toString().length() > 0) {
            Intent intent = new Intent();
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            intent.putExtra("add", etAddress.getText().toString());
            intent.putExtra(Constants.object, addressObj);
            setResult(Activity.RESULT_OK, intent);
            MyApplication.getInstance().setCurrentLatitude(latitude);
            MyApplication.getInstance().setCurrentLongitude(longitude);
            finish();
        }else{
            MyApplication.showMessage(this, getResources().getString(R.string.select_location));
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(DEBUG_TAG, "onBackPressed()");
        //super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //exitZoom();
        if(!isResult) {
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);

                    if(checkAndCallLocation())
                        getLocation();

                    pro.dismiss();
                }
            }, 4000);
        }
        isResult = false;
    }

    private boolean checkAndCallLocation(){
        boolean isFine = ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean isCoarse = ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (isFine && isCoarse) {
            if (Utils.checkGPS(this))
                return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION}, fineReq);
        }
        return  false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //lm.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {

        // moveCamera(location.getLatitude(), location.getLongitude());
    }

    private void moveCamera(final double latitude, final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawMarker(true, new LatLng(latitude, longitude), "", "");
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
            }
        });

    }

    Marker currentMarker = null;
    private void drawMarker(Boolean isCurrentLocation, LatLng dest, String title, String snippet){
        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();
        // Setting the position of the marker
        options.position(dest);



       /* if(!isCurrentLocation){
            if(title != null)
                options.title(title);
            options.snippet(snippet);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.get_location));
            mMap.addMarker(options).setDraggable(false);
        }
        else{*/
        if(currentMarker != null)
            currentMarker.remove();
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.get_location));

        currentMarker = mMap.addMarker(options);
        //currentMarker.setDraggable(true);
        // }
        //options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        String newStatus = "";
        switch (status) {
            case LocationProvider.OUT_OF_SERVICE:
                newStatus = "OUT_OF_SERVICE";
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                newStatus = "TEMPORARILY_UNAVAILABLE";
                break;
            case LocationProvider.AVAILABLE:
                newStatus = "AVAILABLE";
                break;
        }
        String msg = String.format("Provide New Status", provider, newStatus);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        String msg = String.format("provider_enabled", provider);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        String msg = String.format("provider_disabled", provider);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


   /* public void lockLocation(){
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(status!= ConnectionResult.SUCCESS){ // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }else { // Google Play Services are available
            // Getting reference to SupportMapFragment of the activity_main
            GpsTraker gps = new GpsTraker(this);
            if (gps.canGetLocation()) {
                getLocation();
            } else {
                gps.showSettingsAlert();
            }
        }
    }*/

    int tillTimeToWait_in_min = 1;
    private void getLocation(){
        final MyLocation myLocation = new MyLocation();
        MyLocation.LocationResult locationResult = new MyLocation.LocationResult()
        {
            @Override
            public void gotLocation(Context context, Location location)
            {
                if(location != null)
                {
                    firstUpdate = true;
                    // MyApplication.getApplication().setCurrentLatitude(location.getLatitude());
                    //MyApplication.getApplication().setCurrentLongitude(location.getLongitude());
                    MapGetLocActivity.this.latitude = location.getLatitude();
                    MapGetLocActivity.this.longitude = location.getLongitude();
                    System.out.println("By best provider lat : "+location.getLatitude()+" lon : "+location.getLongitude());
                    myLocation.removeUpdate();
                    if(firstUpdate) {
                        firstUpdate = false;
                        getAddressName(location.getLatitude(), location.getLongitude());
                    }
                }
            }
        };

        myLocation.getLocation(this, locationResult, (int) tillTimeToWait_in_min);
    }

    final int fineReq =101;
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case fineReq: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getLocation();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    boolean isResult =false;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == Constants.LOCATION){
            Bundle bundle = data.getExtras();
            EndRoute route = (EndRoute) bundle.getSerializable(Constants.object);
            //int pos = bundle.getInt(Constants.position);
            etAddress.setText(route.getAddress());
            //addMarker(route.getLat()+"", route.getLng()+"", route.getAddress(), "");
            //adapter.setAddress(pos, route);
            latitude = route.getLat();
            longitude = route.getLng();
            Constants.lat = latitude;
            Constants.lng = longitude;
            addressObj = new com.nixbin.MeetNepali.wrapper.Address();
            addressObj.setLocationName(route.getAddress());
            //getAddressName(latitude, longitude);
            //setCurrentLocationMarker();
            moveCamera(latitude, longitude);
            isResult = true;
        }
    }
}
