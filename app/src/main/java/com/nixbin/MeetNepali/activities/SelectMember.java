package com.nixbin.MeetNepali.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.common.StringUtils;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.SelectMemberAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.ServerCallBack;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.webutility.ApiCall;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 11/23/2017.
 */

public class SelectMember extends BaseActiivty implements WebCompleteTask, DataTransferInterface {
    @Bind(R.id.search)
    EditText search;
    @Bind(R.id.listview)
    RecyclerView listview;
    @Bind(R.id.empty_view)
    TextView emptyView;
    Bundle extras;
    ArrayList<Bindcontract> feedsListall = new ArrayList();
    ArrayList<Bindcontract> feedsListall1 = new ArrayList();
    SelectMemberAdapter adapterall;
    String urlAllJob = "";
    private static final int VERTICAL_ITEM_SPACE = 5;
    private Menu mMenu;
    ArrayList<String> namesend=new ArrayList<>();
    ArrayList<String> profilesend=new ArrayList<>();
    ArrayList<String> friend_id=new ArrayList<>();
    JSONObject postData = new JSONObject();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_member);
        ButterKnife.bind(this);
        enableBackButton();
        getSupportActionBar().setTitle("Select Members");
         extras = getIntent().getExtras();





        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                filter(text);
            }
        });

        listview.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(SelectMember.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listview.setLayoutManager(llm);
        listview.addItemDecoration(new SelectMember.VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        callapiAll();

    }

    @Override
    public void setValues(ArrayList<String> alname, ArrayList<String> alemail,ArrayList<String> friendid) {
        namesend=alname;
        profilesend=alemail;
        friend_id=friendid;
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void callapiAll() {
        JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "my_friends");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));

            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(SelectMember.this, WebUrls.API_URL, object, SelectMember.this, RequestCode.CODE_Myfriend, 1);


    }

    public void onComplete(String response, int taskcode) {
        Log.d("response", response);

        try {
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_Myfriend) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONObject outerObj = new JSONObject(object.getString("data"));
                    JSONArray data = null;
                    data = outerObj.getJSONArray("friend_already");

                    JSONObject jobj = null;
                    feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        Bindcontract contract = new Bindcontract();
                        jobj = data.getJSONObject(i);
                        //later add filter here so you don't show users that are already in the list.

                        contract.setFriendid(jobj.getInt("friend_id")+"");
                        contract.setProfileImg(WebUrls.BASE_URL_image + jobj.getString("profile_img"));
                        contract.setName(jobj.getString("first_name"));
                        contract.setEmail(jobj.getString("email"));
                        contract.setFromid(jobj.getInt("friend_id")+"");

                        feedsListall.add(contract);
                        // emptyView.setVisibility(View.GONE);

                    }
                    //  empty_view.setVisibility(View.VISIBLE);
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            //  swipeRefreshLayout.setRefreshing(false);
        }

    }


    public void setList() {
        System.out.println("service list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new SelectMemberAdapter(SelectMember.this, feedsListall, SelectMember.this);
            listview.setAdapter(adapterall);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.send_btn:
                //   Dosome();
                String toastMsg = "An error occurred, please try again.";
                String action = "";

                if (extras != null) {

                    action = extras.getString("ACTION");
                    Log.d("Select Member", "extra is not empty:" + extras.get("ACTION") + "action:" + action);
                }
                    if (action.equalsIgnoreCase("add_members")) {
                        //Log.d("Select Member", "adding members to existing group"+extras.toString());
                        String groupID = extras.getString("GROUP_ID");
                        String members = extras.getString("GROUP_MEMBERS");
                        Integer creatorID = extras.getInt("CREATOR_ID");
                        String chatType = extras.getString("GROUP_TYPE");
                        String groupName = extras.getString("GROUP_NAME");

                        ApiCall apiCall = new ApiCall();
                        try {
                            postData.put("action", "add_members");
                            postData.put("group_id", groupID);
                            postData.put("creator_id", creatorID);
                            postData.put("group_members", members);
                            postData.put("update_ids", TextUtils.join( ",",friend_id));

                            postData.put("update_names", TextUtils.join( ",",namesend)); //else namesend and friend id are arrays ?
                            postData.put("group_name", groupName);
                            postData.put("chat_type", chatType);

                        } catch (Exception e) {
                            Log.d("Select Member", "245-Exception occurred"+e.toString());
                        }
                        apiCall.apiPost(this, "manage_group", postData, new ServerCallBack() {
                                    @Override
                                    public void onSuccess(String responseMessage, String responseData) {

                                        Toast.makeText(SelectMember.this, responseMessage, Toast.LENGTH_LONG).show();
                                        finish();
                                    }

                                }
                        );



                    } else if (namesend.size() >= 2) {
                        //Log.d("Select Member", "inside namesend size greater than 2, Extras:"+extras.toString());


                        startActivity(new Intent(this, AddGrouppublic.class).putExtra("name", namesend).putExtra("profile", profilesend).putExtra("friendid", friend_id));
                        finish();

                    } else {
                        toastMsg = "Please select at least two members to create group";
                        Toast.makeText(SelectMember.this, toastMsg, Toast.LENGTH_LONG).show();

                    }

                    return true;
                }




        return super.onOptionsItemSelected(item);
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        // map.clear();
        if (charText.length() == 0) {
            if (feedsListall.size() == 0) {
                emptyView.setVisibility(View.VISIBLE);
            } else {
               callapiAll();
                emptyView.setVisibility(View.GONE);
            }

        } else {
            feedsListall1.clear();
            for (Bindcontract wp : feedsListall) {
                if (wp.getName().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    Bindcontract wrapper = new Bindcontract();

                    wrapper.setFriendid(wp.getFriendid());
                    wrapper.setProfileImg(wp.getProfileImg());
                    wrapper.setName(wp.getName());
                    wrapper.setEmail(wp.getEmail());
                    wrapper.setFromid(wp.getFromid());
                    feedsListall1.add(wrapper);

                }
                emptyView.setVisibility(View.GONE);
                listview.setAdapter(new SelectMemberAdapter(SelectMember.this, feedsListall1, SelectMember.this));
                new SelectMemberAdapter(SelectMember.this, feedsListall1, SelectMember.this).notifyDataSetChanged();
            }

        }


    }
}
