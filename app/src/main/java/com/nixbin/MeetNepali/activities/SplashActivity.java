package com.nixbin.MeetNepali.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;

import com.google.firebase.messaging.FirebaseMessaging;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.ServerCallBack;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.webutility.ApiCall;
import com.nixbin.MeetNepali.wrapper.Bindcontract;
import com.nixbin.MeetNepali.wrapper.MyLocation;

import org.json.JSONObject;

import static com.nixbin.MeetNepali.application.MyApplication.GLOBAL_TOPIC;

/**
 * Created by Advosoft2 on 6/27/2017.
 */

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //amit mod
        SharedPreferences mPrefs;
        final String firstRun = "firstRun";
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean isFirstRun = mPrefs.getBoolean(firstRun, false);
        final int splashDelay = isFirstRun ? 2000 : 200;
        Log.d("Splash Delay", "" + splashDelay);
        //amit mod end

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();


        if (getIntent().getExtras()!=null)
        {
            Log.d("SPLASH","datafromnotification"+getIntent().getExtras().getString("title")+" "+getIntent().getExtras().getString("userid")+" "+getIntent().getExtras().getString("click_action"));
        }
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (MyApplication.getUserLoginStatus(getApplicationContext()))
                {
                    //MyApplication.saveAuthToken(Constants.API_TOKEN, "");
                    if(MyApplication.getUserData(Constants.API_TOKEN).isEmpty())
                    {
                        //MyApplication.saveAuthToken(Constants.API_TOKEN, loginResponse.getString("api_token"));

                        JSONObject apiCallData = new JSONObject();
                        ApiCall apiCall = new ApiCall();
                        try {

                            apiCallData.put("user_id", MyApplication.getUserID(Constants.UserID));
                            apiCallData.put("user_name", MyApplication.getUserName(Constants.UserName));
                            apiCallData.put("device_token", MyApplication.getUserData(Constants.GCM_ID));

                        } catch (Exception e) {
                            Log.d("Splash Activity", "513- Error"+e.toString());
                        }
                        apiCall.apiPost(getApplicationContext(), "get_api_token", apiCallData, new ServerCallBack() {
                                    @Override
                                    public void onSuccess(String responseMessage, String responseData) {

                                        if(responseMessage.equalsIgnoreCase("success"))
                                        {
                                            MyApplication.saveAuthToken(Constants.API_TOKEN, responseData);
                                        }
                                    }

                                }
                        );


                    }

                    if (getIntent().getExtras()!=null&&getIntent().getExtras().getString("click_action")!=null)
                    {
                        String notificationType = getIntent().getExtras().getString("notification_type");
                        Log.d("PUSH", "Notification Type:" + notificationType);
                        if (notificationType.compareToIgnoreCase("friendRequest")==0){
                            intent = new Intent(SplashActivity.this, MainActivity.class);
                            intent.putExtra("dataaction","1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }else  if (notificationType.compareToIgnoreCase("chat")==0) //changed to ignore case
                        {
                            Log.d("PUSH", "Inside Chat");
                                Bindcontract feeds=new Bindcontract();
                                feeds.setFriendid(getIntent().getExtras().getString("userid"));
                                feeds.setFromid(getIntent().getExtras().getString("userid"));
                                if (getIntent().getExtras().getString("userid").startsWith("grp")){
                                    feeds.setChattype("grp");
                                    //feeds.setGrpcreateddate(Integer.parseInt(getIntent().getExtras().getString("grp_date")));
                                    feeds.setGrpisPrivate("public"); //need to send this from push notification.
                                    feeds.setChatkey(getIntent().getExtras().getString("userid"));
                                }else {
                                    feeds.setChattype("single");
                                }

                                feeds.setName(getIntent().getExtras().getString("title"));
                                feeds.setProfileImg(getIntent().getExtras().getString("profile_img"));

                                intent = new Intent(SplashActivity.this, MainActivity.class);
                                intent.putExtra("dataaction","2");
                                intent.putExtra("data",feeds);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                Log.d("SPLASH", "Launching Activity:"+notificationType);
                                finish();

                        }
                        else {
                            intent = new Intent(SplashActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }

                    }else {
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        }, splashDelay);  //amit changed 2500 to spalshDelay
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation();

    }

    int tillTimeToWait_in_min = 2;

    private void getLocation() {
        MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
            @Override
            public void gotLocation(Context context, Location location) {
                if (location != null) {
                    Log.d("location392334", "" + location);
                    MyApplication.setLatitude(getApplicationContext(), location.getLatitude() + "");
                    MyApplication.setLongitude(getApplicationContext(), location.getLongitude() + "");
                    System.out.println("By best provider lat : " + location.getLatitude() + " lon : " + location.getLongitude());
                    Log.d("lat", "" + MyApplication.getLatitude(getApplicationContext()));
                    Log.d("long", "" + MyApplication.getLongitude(getApplicationContext()));

                }
            }
        };

        MyLocation myLocation = new MyLocation();
        myLocation.getLocation(this, locationResult, (int) tillTimeToWait_in_min);
    }

}
