package com.nixbin.MeetNepali.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.localDatabase.SQLiteHandler;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.ValidationClass;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nixbin.MeetNepali.application.AppConfig.fbUsersTable;

/**
 * Created by Advosoft2 on 9/5/2017.
 */

public class SignUpActivity extends BaseActiivty implements WebCompleteTask {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.pwd)
    EditText pwd;
    @Bind(R.id.con_pwd)
    EditText conPwd;
    @Bind(R.id.submit)
    Button submit;
    private FirebaseAuth mAuth;
    private SQLiteHandler db;
    String firebaseDataBAseID;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private StorageReference mStorageRef;
    private String userId;
    String[] emailid;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        ButterKnife.bind(this);

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    email.setHint("");
                else
                    email.setHint("Email");
            }
        });
        pwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    pwd.setHint("");
                else
                    pwd.setHint("Password");
            }
        });
        conPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    conPwd.setHint("");
                else
                    conPwd.setHint("Confirm Password");
            }
        });
        mAuth = FirebaseAuth.getInstance();
        db = new SQLiteHandler(getApplicationContext());
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference(fbUsersTable);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError(getString(R.string.notempty));
                    email.requestFocus();
                } else if (!ValidationClass.validateEmail(email.getText().toString())) {
                    email.setError(getString(R.string.email_validation));
                    email.requestFocus();
                } else if (pwd.getText().toString().isEmpty()) {
                    pwd.setError(getString(R.string.enter_passowrg));
                    pwd.requestFocus();

                } else if (conPwd.getText().toString().isEmpty()) {
                    conPwd.setError(getString(R.string.confrm_passs));
                    conPwd.requestFocus();

                } else if (pwd.getText().toString().length() < 6) {
                    pwd.setError(getString(R.string.error_password_length));
                    pwd.requestFocus();

                } else if (!pwd.getText().toString().trim().equals(conPwd.getText().toString().trim())) {
                    MyApplication.showMessage(SignUpActivity.this, getString(R.string.error_message_password_not_match));
                } else {
                   // callApi();
                    callApilocal();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        Log.d("MeetLog", "Fb User " + currentUser);
        //  updateUI(currentUser);
    }


private void callApi(String mCustomToken){


    mAuth.signInWithCustomToken(mCustomToken)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCustomToken:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        firebaseDataBAseID = user.getUid();
                        Toast.makeText(SignUpActivity.this,"Registration Successful, Please Login..",Toast.LENGTH_LONG).show();

                        createUser(email.getText().toString(),email.getText().toString());

                        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                        String registration = "";
                        intent.putExtra("registered", registration);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                     //   callApilocalauth();


                    } else {
                        // If sign in fails, display a message to the user.
                        System.out.println("auth fail error"+task.getException());
                        Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                        Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();

                    }
                }
            });




}

    private void callApilocal(){

        JSONObject jobj = new JSONObject();
        String FIREBASE_TOKEN= null;
        try {
            jobj.put("method", "register");
            JSONObject data = new JSONObject();
            data.put("email", email.getText().toString());
            data.put("password", pwd.getText().toString());
            try {
                FIREBASE_TOKEN = FirebaseInstanceId.getInstance().getToken();
            }
            catch (Exception e)
            {
                Log.d(TAG , "Could not get Token during sign up:" + e.toString());
            }

            data.put("device_token", FIREBASE_TOKEN);
            jobj.put("data", data);
            Log.d(TAG, "REgister Request:" + jobj.toString() + "URL: " + WebUrls.API_URL);
        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(SignUpActivity.this, WebUrls.API_URL, object, SignUpActivity.this, RequestCode.Code_addgroup, 1);


    }

    private void callApilocalauth(){

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", "firebase_check");
            JSONObject data = new JSONObject();
            data.put("status", "true");
            data.put("user_id", firebaseDataBAseID);
            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());

        new WebTask(SignUpActivity.this, WebUrls.API_URL, object, SignUpActivity.this, RequestCode.Code_auth, 1);


    }
    @Override
    public void onComplete(String response, int taskcode) {
        System.out.println("response " + response);
        try {
            if (taskcode == RequestCode.Code_addgroup) {
                if (response != null) {
                    try {

                        JSONObject obj = new JSONObject(response);
                       // Toast.makeText(SignUpActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        if (obj.getString("status").compareTo("success") == 0)
                        {
                            Log.d(TAG, "Inside Success");

                            JSONObject obj2 = obj.getJSONObject("data");
                            MyApplication.setUserID(Constants.UserID, obj2.getString("user_id"));
                            MyApplication.setUserName(Constants.UserName, obj2.getString("first_name"));
                            MyApplication.setUserEmail(Constants.UserEmail, obj2.getString("email"));
                            // Inserting row in users table
                            //  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                           // db.addUser(obj2.getString("first_name"), obj2.getString("email"), pwd.getText().toString().trim(), obj2.getString("user_id"), "", firebaseDataBAseID);

                            callApi(obj2.getString("token").trim());

                        }else {
                             Toast.makeText(SignUpActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Inside Toast");
                        }
                    }catch (Exception e) {
                        Log.d(TAG, "Inside Exception" + e.toString());
                    }
                }

            }else if (taskcode == RequestCode.Code_auth){
                if (response != null) {
                    System.out.println("response auth " + response);

                    try {
                        JSONObject obj = new JSONObject(response);
                      //  Toast.makeText(SignUpActivity.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        if (obj.getString("status").compareTo("success") == 0) {

                            createUser(email.getText().toString(),email.getText().toString());
                            JSONObject obj2 = obj.getJSONObject("data");


                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }catch (Exception e) {

                    }
                }
            }
        } catch (Exception e) {

        }
    }



    /**
     * Creating new user node under 'users'
     */
    private void createUser(String name, String email) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
        if (TextUtils.isEmpty(userId)) {
            userId = mFirebaseDatabase.push().getKey();
        }
         emailid=email.split("@");

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.usericon);


        //uploadFile(largeIcon);
        setUserValue("");



        addUserChangeListener();
    }

    /**
     * User data change listener
     */
    private void addUserChangeListener() {
        // User data change listener
        mFirebaseDatabase.child(firebaseDataBAseID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

             //   Log.e(TAG, "User data is changed!" + user.name + ", " + user.email);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void uploadFile(Bitmap bitmap) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(firebaseDataBAseID);
        String userId = mDatabase.push().getKey();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://meetnepali-4f1d5.appspot.com/usersProfilePics/");
        StorageReference mountainImagesRef = storageRef.child(userId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                // sendMsg("" + downloadUrl, 2);
                System.out.println("download url" + downloadUrl);
                Log.d("downloadUrl-->", "" + downloadUrl);
                setUserValue(downloadUrl + "");
            }
        });

    }

    private void setUserValue(String downloadurl){

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbUsersTable).child(firebaseDataBAseID);

        User user = new User(emailid[0],"");
        mDatabase.child("meet_credentials").setValue(user);
    }


}
