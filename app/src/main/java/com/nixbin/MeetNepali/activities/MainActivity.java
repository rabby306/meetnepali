package com.nixbin.MeetNepali.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.adpter.MenuAdapter;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.chat.ChatScreenActivity;
import com.nixbin.MeetNepali.fragments.FragmentUpdateProfile;
import com.nixbin.MeetNepali.fragments.FriendsNew;
import com.nixbin.MeetNepali.fragments.HomepageNew;
import com.nixbin.MeetNepali.fragments.MeetMainFragment;
import com.nixbin.MeetNepali.localDatabase.SQLiteHandler;
import com.nixbin.MeetNepali.utilities.Pref;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.views.Utils;
import com.nixbin.MeetNepali.webutility.ApiConfig;
import com.nixbin.MeetNepali.webutility.AppConfig;
import com.nixbin.MeetNepali.webutility.RequestCode;
import com.nixbin.MeetNepali.webutility.WebCompleteTask;
import com.nixbin.MeetNepali.webutility.WebTask;
import com.nixbin.MeetNepali.webutility.WebUrls;
import com.nixbin.MeetNepali.wrapper.OnlineStatus;
import com.nixbin.MeetNepali.wrapper.SpinnerValue;
import com.nixbin.MeetNepali.wrapper.models.City;
import com.nixbin.MeetNepali.wrapper.models.CityResponse;
import com.nixbin.MeetNepali.wrapper.models.Country;
import com.nixbin.MeetNepali.wrapper.models.Data;
import com.nixbin.MeetNepali.wrapper.models.Region;
import com.nixbin.MeetNepali.wrapper.models.RegionResponse;
import com.nixbin.MeetNepali.wrapper.models.RequestCity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.nixbin.MeetNepali.application.AppConfig.fbOnlineStatusTable;
import static com.nixbin.MeetNepali.webutility.WebUrls.mApplicationType;

public class MainActivity extends BaseActiivty implements NavigationView.OnNavigationItemSelectedListener, WebCompleteTask {
    private static final String TAG = "MainActivity";
    ActionBarDrawerToggle toggle;
    MenuAdapter adapter;
    Dialog Odialog;
    private int SelectedCountryCode=0;
    TextView t1,t2,t3,t4,t5;
    SpinnerValue[] country_sp,region_sp,city_sp;
    String countryTxT=null,regionTxT=null,cityTxT=null;
    Pref pref;

/*    public SpinnerValue[] country_sp = new SpinnerValue[]{
            new SpinnerValue("USA", "1"),
            new SpinnerValue("NEPAL", "2"),
            new SpinnerValue("SINGAPORE", "3")
    };
    public SpinnerValue[] region_sp = new SpinnerValue[]{
            new SpinnerValue("Region 1", "1"),
            new SpinnerValue("Region 2", "2"),
            new SpinnerValue("Region 3", "3")
    };

    public SpinnerValue[] city_sp = new SpinnerValue[]{
            new SpinnerValue("city 1", "1"),
            new SpinnerValue("city 2", "2"),
            new SpinnerValue("city 3", "3")
    };*/

    @Bind(R.id.title_header)
    TextView titleHeader;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.headerId)
    AppBarLayout headerId;
    @Bind(R.id.container)
    FrameLayout container;
    @Bind(R.id.listview)
    ListView listview;
    @Bind(R.id.nav_view)
    NavigationView navView;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.firstNameEdt)
    TextView firstNameEdt;
    @Bind(R.id.chat_profile)
    ImageView chatProfile;

    @Bind(R.id.profile_pic_drawer)
    ImageView profileImage;

    @Bind(R.id.chats)
    TextView chats;

    //@Bind(R.id.friendRequests)
    //TextView friendRequests;

    @Bind(R.id.myfriends)
    TextView myfriends;

    @Bind(R.id.notifications)
    TextView notifications;
    @Bind(R.id.addmember)
    TextView addmember;

    private String mNavTitles[];
    private int mIcons[];
    public static int idcontaner;
    private String startUpFragment = "webview";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // bindfooter = (View) findViewById(R.id.bottom_footer);
        // bindfooter.setVisibility(View.GONE);
        idcontaner=R.id.container;
        setBotomText();
        pref=new Pref(this);
        Odialog=new Dialog(this);
        if (getIntent().getExtras()!=null){
            Log.d(TAG,"Main Activity, Data Action: " + getIntent().getStringExtra("dataaction") );
            Log.d(TAG,"Main Activity, GoTo Intent: " + getIntent().getStringExtra("goToIntent") );
            if (getIntent().getStringExtra("dataaction").compareTo("1")==0){
                startActivity(new Intent(MainActivity.this,FriendRequestActivity.class).putExtra("activity","mainactivity"));
            }else if (getIntent().getStringExtra("dataaction").compareTo("2")==0){
                startActivity(new Intent(MainActivity.this,ChatScreenActivity.class).putExtra("data",getIntent().getSerializableExtra("data")).putExtra("activity","mainactivity"));
            }
            else if(getIntent().getStringExtra("dataaction").equalsIgnoreCase("chatFragment"))
            {
               Log.d("Main Activity", "Inside Display Chat Fragment");
               startUpFragment = "chatFragment";

            }

        }
        else
        {
            Log.d(TAG, "Main Activity, No extras");
        }

        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);

        System.out.println("firebase id " + fromid);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    // run some code
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
                    mDatabase.child(fromid).setValue(onlineStatus);

                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
                    // mDatabase.push().setValue(fromid);
                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
                    mDatabase2.setValue(onlineStatus);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        setActionToolbar();
        setNavigationDrawer();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FriendRequestActivity.class).putExtra("activity","mainactivity"));
            }
        });

        chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChatFragment();
            }
        });

        myfriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //replaceFragment(R.id.container, new FriendList());
                showLocationDialog();

            }
        });

        addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SelectMember.class));
            }
        });
        /*friendRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FriendRequestActivity.class));
            }
        });*/
        //  Utils.loadImageRound(MainActivity.this, chatProfile, "https://"+ MyApplication.getProfileImage(MainActivity.this,"profileImage"));
        firstNameEdt.setText(MyApplication.getUserName(Constants.UserName));
        //Utils.loadImageRound(context, profileImage, chatInfo.getProfileImg());
        Utils.loadImageRound(MainActivity.this, profileImage,WebUrls.BASE_URL_image+ MyApplication.getProfileImage(MainActivity.this,Constants.name));

    }

    private void showLocationDialog() {
        final Spinner CountrySp,RegionSp,CitySp;
        Odialog = new Dialog(MainActivity.this);
        Odialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        Odialog.setContentView(R.layout.location_dialog);
        Odialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        Odialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Odialog.getWindow().setDimAmount(0.8f);
        Odialog.setCancelable(false);
        ((Button) Odialog.findViewById(R.id.cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Odialog.cancel();
            }
        });

        ((Button) Odialog.findViewById(R.id.confirm_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(countryTxT!=null && regionTxT!=null && cityTxT !=null){
                    pref.set("country",countryTxT);
                    pref.set("region",regionTxT);
                    pref.set("city",cityTxT);}
                Odialog.cancel();
            }
        });
        TextView curentAddress=(TextView) Odialog.findViewById(R.id.curentAddress);
        curentAddress.setText(pref.getString("city")+","+pref.getString("region")+","+pref.getString("country"));

        CountrySp=(Spinner) Odialog.findViewById(R.id.country_spinner);
        RegionSp=(Spinner) Odialog.findViewById(R.id.region_spinner);
        CitySp=(Spinner) Odialog.findViewById(R.id.city_spinner);
        CountrySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = country_sp[position];
                SelectedCountryCode=Integer.parseInt(value.valueText);
                countryTxT=value.displayText;

                DownloadRegion(RegionSp,SelectedCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        RegionSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = region_sp[position];
                regionTxT=value.displayText;
                DownloadCity(CitySp,SelectedCountryCode,Integer.parseInt(value.valueText));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        CitySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                SpinnerValue value = city_sp[position];
                cityTxT=value.displayText;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

       // bind(((Spinner) Odialog.findViewById(R.id.country_spinner)),country_sp);
        DownloadCountry(CountrySp);
        /*bind(((Spinner) Odialog.findViewById(R.id.region_spinner)),region_sp);
        bind(((Spinner) Odialog.findViewById(R.id.city_spinner)),city_sp);*/
        Odialog.show();
    }

    private void setNavigationDrawer() {
        mIcons = new int[5];
        mNavTitles = new String[5];
        mIcons[0] = R.drawable.home;
        mNavTitles[0] = getResources().getString(R.string.home);
        mIcons[1] = R.drawable.chat;
        mNavTitles[1] = getResources().getString(R.string.chatscreen);
        mIcons[2] = R.drawable.friend;
        mNavTitles[2] = getResources().getString(R.string.findfriend);
        mIcons[3] = R.drawable.setting;
        mNavTitles[3] = getResources().getString(R.string.setting);
        mIcons[4] = R.drawable.logout;
        mNavTitles[4] = getResources().getString(R.string.logout);


        adapter = new MenuAdapter(mNavTitles, mIcons, this);
        adapter.setSelectedPosition(0);
        listview.setAdapter(adapter);
        if(startUpFragment.equalsIgnoreCase("chatfragment"))
        {
            displayChatFragment();
        }else
            {
            //displayWebViewFragment(); //default startup
               // startActivity(new Intent(MainActivity.this, MeetMain.class).putExtra("activity","mainactivity"));
                //displayMainFragment();
                displayWebViewFragment();
            }


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelectedPosition(position);
                adapter.notifyDataSetChanged();

                switch (position) {
                    case 0:
                        displayWebViewFragment();
                        //  relativeLl.setVisibility(View.GONE);
                        break;
                    case 1:
                        displayChatFragment();
                        // startActivity(new Intent(MainActivity.this,FriendRequestActivity.class));
                        // relativeLl.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        displayFindFriendFragment();
                        // relativeLl.setVisibility(View.GONE);
                        // startMyActivity(YourAllPropertyActivity.class, null);
                        break;
                    case 3:
                        displayUpdateProfileFragment();
                        break;
                    case 4:
                        showAlertDialog(MainActivity.this, position, getResources().getString(R.string.logout_warning), "Alert");
                        // relativeLl.setVisibility(View.GONE);

                        break;

                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

       /* nav_view.getMenu().clear(); //clear old inflated items.
        nav_view.inflateMenu(R.menu.activity_main_drawer); //inflate new items.*/

    }

    @Override
    public void onResume() {
        super.onResume();
        firstNameEdt.setText(MyApplication.getUserName(Constants.UserName));
        Utils.loadImageRound(MainActivity.this, chatProfile, WebUrls.BASE_URL_image + MyApplication.getProfileImage(MainActivity.this, "profileImage"));


        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);

        System.out.println("firebase id " + fromid);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    // run some code
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
                    mDatabase.child(fromid).setValue(onlineStatus);

                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
                    //   mDatabase.push().setValue(fromid);
                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
                    mDatabase2.setValue(onlineStatus);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        setTitleOnAction("Home");
    }

    private void setActionToolbar() {
        setSupportActionBar(toolbar);
        setTitleOnAction(getResources().getString(R.string.app_name));
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                int stackCount = getBackStackCount();
                if (stackCount > 1) {
                    toggle.onDrawerClosed(drawerView);
                    // removeTopFragement();
                    if (stackCount == 2) {
                        disableBackButton();
                        removeTopFragement();
                    } else
                        removeTopFragement();
                } else {
                    super.onDrawerOpened(drawerView);
                }
            }
        };
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Intent intent;
     /*   switch (id) {
            case R.id.nav_home:
                displayWebViewFragment();
                break;
            case R.id.nav_transfer:
                displayWebViewFragment();
                break;
            case R.id.nav_money:
                displayWebViewFragment();
                break;
            case R.id.nav_topup:
                displayWebViewFragment();
                break;
            case R.id.nav_news:
                displayWebViewFragment();
                break;
            case R.id.nav_forex:
                displayWebViewFragment();
                break;
            case R.id.nav_feedback:
                displayWebViewFragment();
                break;
            case R.id.sign_out:
                displayWebViewFragment();
                break;
            case R.id.call:
                displayWebViewFragment();
                break;

        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void hideHomeActionButtons()
    {
        myfriends.setVisibility(GONE);
        chats.setVisibility(GONE);

        //friendRequests.setVisibility(GONE);

    }
    private void hideFragmentActionButtons()
    {
        //notifications.setVisibility(GONE);
        chatProfile.setVisibility(GONE);
        addmember.setVisibility(GONE);
    }
    private void hideAllActionButtons()
    {
        hideFragmentActionButtons();
        hideHomeActionButtons();

    }

    public void displayMainFragment() {
        replaceFragment(R.id.container, new MeetMainFragment());
        titleHeader.setVisibility(GONE);
        //titleHeader.setText("Home");

        myfriends.setVisibility(View.VISIBLE);
        chats.setVisibility(View.VISIBLE);
        //friendRequests.setVisibility(View.VISIBLE);
        notifications.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)myfriends.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, R.id.chats);


        //RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)chats.getLayoutParams();
        //params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //layout_alignParentRight="true"
        hideFragmentActionButtons();

        //  bindfooter.setVisibility(View.GONE);

    }

    public void displayWebViewFragment() {
        replaceFragment(R.id.container, new InAppBrowserActivity());
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText("Dashboard");

        myfriends.setVisibility(View.VISIBLE);
        chats.setVisibility(View.VISIBLE);
        //friendRequests.setVisibility(View.VISIBLE);
        notifications.setVisibility(View.VISIBLE);
        /*RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)myfriends.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, R.id.chats);*/


        //RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)chats.getLayoutParams();
        //params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //layout_alignParentRight="true"
        hideFragmentActionButtons();

        //  bindfooter.setVisibility(View.GONE);

    }

    public void displayChatFragment() {
        replaceFragment(R.id.container, new HomepageNew()); //replaces the HomepageNew container. that is the chat container
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText("Chat");
        notifications.setVisibility(View.VISIBLE);
        //chatProfile.setVisibility(View.VISIBLE);
        addmember.setVisibility(View.VISIBLE);
        //  bindfooter.setVisibility(View.VISIBLE);
        //RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)myfriends.getLayoutParams();
        //params.addRule(RelativeLayout.LEFT_OF, R.id.addmember);
        //myfriends.setLayoutParams(params);
        hideHomeActionButtons();
        overridePendingTransition (0, 0);

    }

    public void displaySearchFragment() {
        replaceFragment(R.id.container, new HomepageNew()); //replaces the HomepageNew container. that is the chat container
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText("Chat");
        notifications.setVisibility(View.VISIBLE);
        //chatProfile.setVisibility(View.VISIBLE);
        addmember.setVisibility(View.VISIBLE);
        //  bindfooter.setVisibility(View.VISIBLE);
        //RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)myfriends.getLayoutParams();
        //params.addRule(RelativeLayout.LEFT_OF, R.id.addmember);
        //myfriends.setLayoutParams(params);
        hideHomeActionButtons();
        overridePendingTransition (0, 0);

    }

    public void displayFindFriendFragment() {
        replaceFragment(R.id.container, new FriendsNew());
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText("Discover");
        hideAllActionButtons();
        //  bindfooter.setVisibility(View.GONE);

    }

    public void displayUpdateProfileFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("message", "Alo Elena!");

        replaceFragment(R.id.container, new FragmentUpdateProfile());
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText("Profile");
        hideAllActionButtons();

        //   bindfooter.setVisibility(View.GONE);

    }

    AlertDialog alertDialog = null;

    public AlertDialog showAlertDialog(final Context context, final int position, String msg, String title) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(msg);
        alertBuilder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                Logout(MyApplication.getUserID(Constants.UserID));
            }
        });
        alertBuilder.setNegativeButton("Cancel", null);
        alertDialog = alertBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

    private void Logout(String userid) {
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("method", "api_logout");
            JSONObject data = new JSONObject();
            data.put("user_id", userid);
            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
        new WebTask(MainActivity.this, WebUrls.API_URL, object, MainActivity.this, RequestCode.CODE_LogOut, 1);

    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response ", response);
        if (taskcode == RequestCode.CODE_LogOut) {
            if (response != null) {
                // SharedPreferences prefs = getSharedPreferences("MEETNEPALI", Context.MODE_PRIVATE);
               /* SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(Constants.isLOgin);*/
                MyApplication.setUserLoginStatus(MainActivity.this, false);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();


                // App is not running
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                Long tsLong = System.currentTimeMillis() / 1000;
                String ts = tsLong.toString();
                OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
                mDatabase.child(fromid).setValue(onlineStatus);

            }

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

       /* DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
        mDatabase.child(fromid).setValue(onlineStatus);
*/
        /*if (Helper.isAppRunning(MainActivity.this, "com.nixbin.MeetNepali")) {
            // App is running
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("online_status");
            String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            OnlineStatus onlineStatus = new OnlineStatus(tsLong, "online");
            mDatabase.child(fromid).setValue(onlineStatus);

        } else {
            // App is not running
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("online_status");
            String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
            mDatabase.child(fromid).setValue(onlineStatus);
        }*/


        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            { //this is where user online / offline is created.
                if (snapshot.exists()) {
                    // run some code
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
                    mDatabase.child(fromid).setValue(onlineStatus);

                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
                    //  mDatabase.push().setValue(fromid);
                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
                    mDatabase2.setValue(onlineStatus);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onStop()
    { //when app is stopped.
        super.onStop();
       /* DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
        mDatabase.child(fromid).setValue(onlineStatus);*/

        String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    // run some code
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
                    mDatabase.child(fromid).setValue(onlineStatus);

                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable);
                    String fromid = MyApplication.getFirebaseID(Constants.FirebaseId);
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    OnlineStatus onlineStatus = new OnlineStatus(tsLong, "offline");
                    // mDatabase.push().setValue(fromid);
                    DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference().child(fbOnlineStatusTable).child(fromid);
                    mDatabase2.setValue(onlineStatus);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void bind(Spinner spinner, SpinnerValue[] spinnerValue) {

        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(MainActivity.this, R.layout.spinner_drop, spinnerValue); //android.R.layout.simple_spinner_item
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    private void setBotomText()
    {
        t1=(TextView) findViewById(R.id.t1);
        t2=(TextView) findViewById(R.id.t2);
        t3=(TextView) findViewById(R.id.t3);
        t4=(TextView) findViewById(R.id.t4);
        t5=(TextView) findViewById(R.id.t5);
        t1.setVisibility(GONE);
        t2.setVisibility(GONE);
        t3.setVisibility(GONE);
        t4.setVisibility(GONE);
        t5.setVisibility(GONE);
    }

    public void botomClick(View view) {
        setBotomText();
        LinearLayout lin =(LinearLayout) findViewById(view.getId());
        for (int i = 0, count = lin.getChildCount(); i < count; ++i) {
            View childView = lin.getChildAt(i);
            if (childView instanceof TextView)
                ((TextView) childView).setVisibility(View.VISIBLE);
        }
        if(view.getId()==R.id.dashboard_btn)displayWebViewFragment();
        if(view.getId()==R.id.discover)displayFindFriendFragment();
        if(view.getId()==R.id.chat)displayChatFragment();
        if(view.getId()==R.id.group) Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();
        if(view.getId()==R.id.star)Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();

    }

    private void DownloadCountry(final Spinner sp)
    {
        ArrayList<Country> cities=new ArrayList<>();
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_countries");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);

        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<List<Country>> call = getResponse.GetCountryData(mApplicationType,requestCity);
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "Country Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(MainActivity.this);
                        db.InsertCountryBulk(response.body());*/
                        country_sp=new SpinnerValue[response.body().size()];
                    for(int i=0;i<response.body().size();i++)
                    {
                        Country country=response.body().get(i);
                        country_sp[i] =new SpinnerValue(country.getCountryName(), String.valueOf(country.getId()));
                    }
                        bind(sp,country_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {

                Toast.makeText(MainActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                System.out.println("fail response.." + t.toString());
            }
        });
    }

    private void DownloadRegion(final Spinner sp ,int countryId)
    {
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_regions");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setCountryId(countryId);
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<RegionResponse> call = getResponse.GetRegionData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "Region Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(MainActivity.this);
                        db.InsertCountryBulk(response.body());*/
                        region_sp=new SpinnerValue[response.body().getData().getRegions().size()];
                        for(int i=0;i<response.body().getData().getRegions().size();i++)
                        {
                            Region region=response.body().getData().getRegions().get(i);
                            region_sp[i] =new SpinnerValue(region.getRegion(), String.valueOf(region.getId()));
                        }
                        bind(sp,region_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {

                Toast.makeText(MainActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }


    private void DownloadCity(final Spinner sp ,int countryId,int RegionId)
    {
        final RequestCity requestCity=new RequestCity();
        requestCity.setMethod("list_cities");
        Data data=new Data();
        data.setCity("Phoenix");
        data.setSearch("krish");
        data.setCountryId(countryId);
        data.setRegionId(RegionId);
        data.setUserId(Integer.parseInt(MyApplication.getUserID(Constants.UserID)));
        data.setRegion("AZ");
        requestCity.setData(data);
        Log.w("Rabby",  "Region Request: " + new Gson().toJson(requestCity));
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<CityResponse> call = getResponse.GetCityData(new Gson().toJson(requestCity));
        call.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        Log.w("Rabby",  "City Response: " + new Gson().toJson(response.body()));
                        System.out.println("update data " + response.body().toString());
/*                        SQLiteHandler db=new SQLiteHandler(MainActivity.this);
                        db.InsertCountryBulk(response.body());*/
                        city_sp=new SpinnerValue[response.body().getData().getCities().size()];
                        for(int i=0;i<response.body().getData().getCities().size();i++)
                        {
                            City city=response.body().getData().getCities().get(i);
                            city_sp[i] =new SpinnerValue(city.getCityName(), String.valueOf(city.getId()));
                        }
                        bind(sp,city_sp);
                    } catch (Exception je) {

                    }
                } else {
                    System.out.println(" " + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

                Toast.makeText(MainActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.w("Rabby",  "Error: " +t.toString() );
            }
        });
    }
}
