package com.nixbin.MeetNepali.webutility;

import com.google.firebase.database.DataSnapshot;

import org.json.JSONObject;

/**
 * Created by advosoft on 12/6/2017.
 */

public interface FirebaseComplete {
    public void onComplete(DataSnapshot dataSnapshot, JSONObject obj);


}
