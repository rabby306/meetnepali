package com.nixbin.MeetNepali.webutility;

/**
 * Created by suarebits on 3/12/15.
 */
public class RequestCode {
    public static final int CODE_Forogot = 2;
    public static final int CODE_Login = 3;
    public static final int CODE_LogOut = 4;
    public static final int CODE_FriendList = 5;
    public static final int Code_Otp = 5;
    public static final int Code_Resend_Otp = 6;
    public static final int CODE_GetCat = 6;
    public static final int CODE_GetPost= 7;
    public static final int CODE_AddFriend = 8;
    public static final int CODE_SubmitSurvay= 8;
    public static final int CODE_Myfriend = 9;
    public static final int CODE_AcceptFriend = 10;
    public static final int CODE_CancelFriend = 11;
    public static final int CODE_DeactivateAccount = 12;
    public static final int CODE_ACCOUNT = 13;
    public static final int CODE_ACCEdit = 14;
    public static final int CODE_Archive = 15;
    public static final int CoDE_Discard = 16;
    public static final int CoDE_Changelang = 17;
    public static final int Code_tDo = 18;
    public static final int Code_getAllChatmsg = 19;
    public static final int Code_SendMsg = 20;
    public static final int Code_addgroup = 21;
    public static final int Code_Sendnotification = 22;
    public static final int Code_getResponse = 23;
    public static final int Code_viewattend = 24;
    public static final int Code_ChatIDo = 25;
    public static final int Code_Blockuser = 26;
    public static final int Code_auth = 27;
    public static final int Code_deleteFriend = 28;
    public static final int CODE_SUBSCRIBE_GROUP = 29;
    public static final int CODE_FRIEND_REQUEST_ACTION = 30;


}
