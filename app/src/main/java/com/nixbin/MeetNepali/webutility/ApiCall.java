package com.nixbin.MeetNepali.webutility;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nixbin.MeetNepali.BuildConfig;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.utilities.ServerCallBack;
import com.nixbin.MeetNepali.views.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amit on 2/24/2018.
 */

/*
public class ApiCall  {

    HashMap<String, String> params;
    private  String APITOKEN = MyApplication.getAuthToken(Constants.API_TOKEN);
    String responseMessage;
    String responseData;
    public void apiget(Context callerContext) {

        String url = "https://www.youraddress.com/";

        Map<String, String> params = new HashMap();
        params.put("first_param", "1");
        params.put("second_param", "2");

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                TODO: handle success
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                TODO: handle failure
            }
        });

        Volley.newRequestQueue(callerContext).add(jsonRequest);


    } */

    /*
     JSONObject jobj = new JSONObject();
        try {

            jobj.put("method", "my_friends");
            JSONObject data = new JSONObject();
            data.put("user_id", MyApplication.getUserID(Constants.UserID));

            jobj.put("data", data);

        } catch (Exception e) {

        }
        HashMap object = new HashMap();
        object.put("request", jobj.toString());
     */
 public class ApiCall{

    HashMap<String, String> params;
    private  String APITOKEN = MyApplication.getAuthToken(Constants.API_TOKEN);
    private String responseMessage;
    private String responseData;

    public void test()
    {
        Log.d("TEst", "Test");
    }

    public  void apiPost(Context callerContext, String method, JSONObject data,final ServerCallBack callback )
    {
        String url = WebUrls.API_URL;
        //final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.api_hiting));
        Log.w("Rabby","ApiLink:"+url);

        JSONObject requestObject = new JSONObject();
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
       /* JSONObject data = new JSONObject();*/
        Log.d("APICALL", "Data:" + data.toString());
        try {

            //all the data json items
            data.put("user_id", MyApplication.getUserID(Constants.UserID));
            data.put("user_name",MyApplication.getUserName(Constants.UserName));
            data.put("device_type", "Android");
            //data.put("version_code", versionCode);
            data.put("version", versionName);

            //method
            requestObject.put("method", method);
            //request is going to be total
            requestObject.put("data", data);

        } catch (Exception e) {
            Log.d("APICALL", "Error Making JSON" + e.toString());
        }
        Map<String, String> request = new HashMap();
        request.put("request", requestObject.toString());

        JSONObject parameters = new JSONObject(request);
        Log.d("APICALL" , "Request Object:" + parameters.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("APICALL", "Response:" + response.toString());

                try {

                    //JSONObject outerObj = new JSONObject(response.getString("data"));
                    responseMessage = response.getString("message");
                    responseData = response.getString("data");


                    //JSONArray data = null;
                    //data = outerObj.getJSONArray("friend_sentrequest");

                    //JSONObject jobj = null;

                    /*for (int i = 0; i < data.length(); i++) {
                        Bindcontract contract = new Bindcontract();
                        jobj = data.getJSONObject(i);

                        contract.setFriendid(jobj.getInt("friend_id") + "");
                        contract.setName(jobj.getString("first_name"));
                        contract.setEmail(jobj.getString("email"));
                        contract.setProfileImg(jobj.getString("profile_img"));


                        feedsListall.add(contract);
                        empty_view.setVisibility(View.GONE);

                    }*/
                    //  empty_view.setVisibility(View.VISIBLE);


                } catch (JSONException e1) {
                    e1.printStackTrace();
                }









                callback.onSuccess(responseMessage, responseData); // call call back function here
                //TODO: handle success
                //https://stackoverflow.com/questions/23833977/android-wait-for-volley-response-to-return
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
            }


        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header_param = new HashMap<>();
                Log.d("Volley", "Api Token: "+ APITOKEN);
                //header_param.put("Authorization",MyApplication.getInstance().getSid(Constants.AccessToken));
                header_param.put("Authorization", "Bearer "+APITOKEN);
                return header_param;
            }
        };

        Volley.newRequestQueue(callerContext).add(jsonRequest);


    }


}
