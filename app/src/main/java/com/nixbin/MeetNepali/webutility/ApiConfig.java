package com.nixbin.MeetNepali.webutility;


import com.google.gson.JsonObject;
import com.nixbin.MeetNepali.wrapper.models.CityResponse;
import com.nixbin.MeetNepali.wrapper.models.Country;
import com.nixbin.MeetNepali.wrapper.models.RegionResponse;
import com.nixbin.MeetNepali.wrapper.models.RequestCity;
import com.nixbin.MeetNepali.wrapper.models.UserList;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiConfig {

    @Multipart
    @POST("retrofit_example/upload_image.php")
    Call<JsonObject> uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);
    //String  token=PostPropertyFragment.accesstokrn;



    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> PostEditprofile(@Part MultipartBody.Part profileImage,
                                     @Part("data") RequestBody lookingFor,
                                     @Query("access_token") String accessToken
    );
    @Multipart
    @POST(WebUrls.Add_SendImage)
    Call<JsonObject> PostChatImage(@Part MultipartBody.Part profileImage,
                                   @Part("data") RequestBody lookingFor,
                                   @Query("access_token") String accessToken
    );

    @Multipart
    @POST(WebUrls.Add_Post)
    Call<JsonObject> PostContract(@Part MultipartBody.Part postImage,
                                  @Part("data") RequestBody lookingFor,
                                  @Query("access_token") String accessToken
    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> Updatepost(@Part MultipartBody.Part postImage,
                                @Part("request") RequestBody lookingFor

    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> Updatepostdata(
            @Part("request") RequestBody lookingFor

    );


    @POST(WebUrls.UpdateProfile)
    Call<List<Country>> GetCountryData(
            @Header("Content-Type") String contentType,
            @Body RequestCity requestCity
    );

    @POST(WebUrls.UpdateProfile)
    Call<RegionResponse> GetRegionData(
            @Query("request") String requestCity
    );

    @POST(WebUrls.UpdateProfile)
    Call<CityResponse> GetCityData(
            @Query("request") String requestCity
    );

    @POST(WebUrls.UpdateProfile)
    Call<UserList> GetRecomandedUserData(
            @Query("request") String requestCity
    );


}
