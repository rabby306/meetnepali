package com.nixbin.MeetNepali.webutility;

import android.os.Build;
import android.util.Log;

import com.nixbin.MeetNepali.application.*;
import com.nixbin.MeetNepali.application.AppConfig;

/**
 * Created by Advosoft2 on 6/28/2017.
 */
public class WebUrls
{

    private final static boolean devMode = AppConfig.devMode;
    private final static boolean debug = AppConfig.debug;
    public final static boolean firebaseDevMode = AppConfig.firebaseDevMode;
    public static final String mApplicationType = "application/json";


    private final static boolean https = false;

    //LOCAL URLS
    private final static String DEV_URL = "meet.laravel";
    public static final String DEV_DASHBOARD_URL = "http://meet.laravel/home/mobileDashboard";
    //DEV URLS
   /* private final static String DEV_URL = "dev.meetnepali.com";
    public static final String DEV_DASHBOARD_URL = "http://dev.meetnepali.com";*/


    //PROD URLS
    //private final static String PROD_URL = "www.meetnepali.com";
    private final static String PROD_URL = "dev.meetnepali.com";
    public static final String PROD_DASHBOARD_URL = "http://www.meetnepali.com/home/mobileDashboard";

   // private final static String APP_URL =  devMode ? DEV_URL : PROD_URL; //this needs to be first isEmulator() && devmode caused non emulators to go to prod
    private final static String APP_URL =   PROD_URL; //this needs to be first isEmulator() && devmode caused non emulators to go to prod
    public static final String BASE_URL = getBaseUrl();

    public static final String DASHBOARD_URL =  devMode ? DEV_DASHBOARD_URL : PROD_DASHBOARD_URL; //isEmulator() && caused to load prod url in non emulators


    public static final String BASE_URL_image = BASE_URL + "/profile-pictures/";
    public final static String Add_Post = "/api/Posts/addPost";
    public static final String UpdateProfile = "/api";
    public final static String Add_SendImage = "/api/Chats/sendImgVdo";

    public static final String API_URL = BASE_URL + "/api";



    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    private static String getBaseUrl()
    {
        String  baseURL;
        String protocol;
        if(https){ protocol = "https"; } else {protocol = "http";}
        if(devMode){ baseURL = protocol + "://" + APP_URL; }
        else { baseURL = protocol + "://" + APP_URL;}

       // Log.d("Udo", "BASE URL : "+ baseURL);
        return baseURL;
    }

    public static boolean debugPrint(String TAG, String logMessage)
    {
        if(debug)
            Log.d(TAG, logMessage);
        return true;
    }
}
