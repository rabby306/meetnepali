package com.nixbin.MeetNepali.fcm;///**

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.activities.MainActivity;
import com.nixbin.MeetNepali.application.MyApplication;
import com.nixbin.MeetNepali.views.Constants;
import com.nixbin.MeetNepali.wrapper.Bindcontract;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String contract_id;
    private static final String TAG = "PushNotification";
    private int count = 0;
    //  int badgeCount = 1;
    public static Boolean eventb = false;
    String CUSTOM_ACTION = "1";
    String CUSTOM_ACTION1 = "2";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Called when message is received.
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        //amit mod
        Map<String, String> pushData = remoteMessage.getData();

        if(remoteMessage.getData()!= null && remoteMessage.getNotification() == null) //data only notification for subscribing to topic, pushData has the data
        {
            if(pushData.get("subscribe_topic") != null)
            {
                Log.d(TAG, "Subscribing to Topic: "+ pushData.get("subscribe_topic"));
                FirebaseMessaging.getInstance().subscribeToTopic(pushData.get("subscribe_topic"));
            }
        }

        if(remoteMessage.getNotification() != null) //General Notification with or without data payload.
        {
            Log.d(TAG, "Notification and Data Message Received, Data: " + remoteMessage.getData().toString());
           // later skip everything below and just call sendNotification New
            String senderID = pushData.get("sender_id");
            String myUserID = MyApplication.getUserID(Constants.UserID).toString();
            Log.d(TAG, "Sender ID : "+senderID+" My UserID: "+myUserID);
            if(senderID.compareTo(myUserID) == 0)
            {
                Log.d(TAG,"73 - Notiifcation received to self, ignoring..");
            }
            else
            {
                Log.d(TAG, "Sender:"+ senderID + " is not my userID: "+ myUserID +" so sending notification");
                sendNotification(pushData); //only notify if not the sender.
            }

            int badge_count = 0;
        }
        else
        {
            Log.d(TAG, "Notification Null");
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     * <p>
     * //     * @param messageBody FCM message body received.
     */

    private void sendNotification(Map<String, String> pushData) {

        Bitmap remote_picture = null;
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        //            notiStyle.setSummaryText(arr[0]);

        NotificationManager notificationManager = null;
        Notification notification = null;
        try {
            notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = null;

            String NotificationType = pushData.get("notification_type");
            Log.d(TAG, "Notification Type: " + NotificationType);
            if (NotificationType == "friendRequest")
            {
                Intent gotoIntent = new Intent(this,MainActivity.class);
                gotoIntent.putExtra("dataaction","1");
                gotoIntent.putExtra("goToIntent","friendRequest");
                // gotoIntent.setClassName(this, "com.nixbin.MeetNepali.activities.MainActivity");//Start activity when user taps on notification.
                contentIntent = PendingIntent.getActivity(this,
                        (int) (Math.random() * 100), gotoIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }
            else  if(NotificationType == "chat")
            {
                Bindcontract feeds=new Bindcontract();
                feeds.setFriendid(pushData.get("userid"));
                feeds.setFromid(pushData.get("userid"));


                feeds.setGrpisPrivate("private"); //get this from push notification later.

                if (pushData.get("chatType")== "group"){
                    feeds.setChattype("grp");
                    feeds.setGrpcreateddate(Integer.parseInt(pushData.get("grp_date")));
                }else {
                    feeds.setChattype("single");
                }

                feeds.setName(pushData.get("title"));
                feeds.setProfileImg(pushData.get("profilePicture"));
                Intent gotoIntent = new Intent(this,MainActivity.class);
                gotoIntent.putExtra("data",feeds);
                gotoIntent.putExtra("dataaction","2");
                //  gotoIntent.setAction(action_intent);
                //  gotoIntent.setClassName(this, "com.nixbin.MeetNepali.activities.MainActivity");//Start activity when user taps on notification.
                contentIntent = PendingIntent.getActivity(this,
                        (int) (Math.random() * 100), gotoIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }
            else
            {
                Intent gotoIntent = new Intent(this,MainActivity.class);
                gotoIntent.putExtra("dataaction","0");
                gotoIntent.putExtra("goToIntent","chat");
                // gotoIntent.setClassName(this, "com.nixbin.MeetNepali.activities.MainActivity");//Start activity when user taps on notification.
                contentIntent = PendingIntent.getActivity(this,
                        (int) (Math.random() * 100), gotoIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }

            Uri soundURI = Uri.parse("android.resource://"+this.getPackageName()+"/"+R.raw.notification);
            //String channelId = getString(R.string.default_notification_channel_id);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

            notification = mBuilder.setSmallIcon(getNotificationIcon()).setTicker(pushData.get("title")).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(pushData.get("title"))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(pushData.get("message")))
                    .setContentIntent(contentIntent)
                    .setContentText(pushData.get("message"))
                    .setSound(soundURI)
                    .build();
            //notification.sound =Uri.parse("android.resource://"+this.getPackageName()+"/"+R.raw.notification);//Here is FILE_NAME is the name of file that you want to play
            System.out.println("uri android "+Uri.parse("android.resource://"+this.getPackageName()+"/"+R.raw.notification));
            //hmm start


            // Vibrate if vibrate is enabled
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notificationManager.notify(0, notification);
            notification.flags = Notification.FLAG_AUTO_CANCEL;
           /* count++;
            notificationManager.notify(count, notification);*/
        } catch (Exception e) {
            //e.printStackTrace();
            Log.d("FCM", "231 - FBMessagingService - "+e.toString());
        }

    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.launcher_icon_trans : R.mipmap.launcher_icon;
    }
}
