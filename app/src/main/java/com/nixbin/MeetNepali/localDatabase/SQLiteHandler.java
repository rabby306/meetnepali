/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.nixbin.MeetNepali.localDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nixbin.MeetNepali.wrapper.models.Country;

import java.util.HashMap;
import java.util.List;

public class SQLiteHandler extends SQLiteOpenHelper {

	private static final String TAG = SQLiteHandler.class.getSimpleName();

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 4;

	// Database Name
	private static final String DATABASE_NAME = "meetnepali";

	// Login table name
	private static final String TABLE_USER = "user";
	private static final String TABLE_COUNTRY = "country";
	private static final String TABLE_REGION = "region";
	private static final String TABLE_CITY = "city";

	// Login Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_UID = "uid";
	private static final String KEY_CREATED_AT = "created_at";
	private static final String KEY_FB_TOKEN = "fb_token";
	private static final String REG_PUSH_TOKEN="push_token";


	public SQLiteHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_EMAIL + " TEXT UNIQUE,"+ KEY_PASSWORD + " TEXT,"+ KEY_UID + " TEXT,"
				+ KEY_CREATED_AT + " TEXT, " + KEY_FB_TOKEN +" TEXT"+")";
		//+ KEY_CREATED_AT + " TEXT, " + KEY_FB_TOKEN +" TEXT, "+ REG_PUSH_TOKEN +" TEXT"+")";

/*		String CREATE_COUNTRY_INFO = "CREATE TABLE IF NOT EXISTS " + TABLE_COUNTRY + "("
				+ "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				"countryCode" + " VARCHAR," +
				"countryName" + " VARCHAR," +
				"countryId" + " INTEGER," +
				"status" + " INTEGER" +
				")";

		String CREATE_REGION_INFO = "CREATE TABLE IF NOT EXISTS " + TABLE_REGION + "("
				+ "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				"regionId" + " INTEGER," +
				"regionName" + " VARCHAR," +
				"countryId" + " INTEGER," +
				"regionCode" + " VARCHAR" +
				")";

		String CREATE_CITY_INFO = "CREATE TABLE IF NOT EXISTS " + TABLE_CITY + "("
				+ "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				"cityId" + " INTEGER," +
				"cityName" + " VARCHAR," +
				"countryId" + " INTEGER," +
				"regionId" + " INTEGER" +
				")";*/



		db.execSQL(CREATE_LOGIN_TABLE);
/*		db.execSQL(CREATE_COUNTRY_INFO);
		db.execSQL(CREATE_REGION_INFO);
		db.execSQL(CREATE_CITY_INFO);*/
		Log.d(TAG, "Database tables created or upgraded");
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		Log.d(TAG, "Database Cleared");
		// Create tables again
		onCreate(db);
	}

/*	public String InsertCountryBulk(List<Country> countryList) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			db.beginTransaction();
			for (int i = 0; i < countryList.size(); i++) {
				Country f = countryList.get(i);
				ContentValues value = new ContentValues();

				value.put("countryCode", f.getCountryCode());
				value.put("countryName", f.getCountryName());
				value.put("countryId", f.getId());
				value.put("status", f.getStatus());

				db.insert(TABLE_COUNTRY, null, value);

			}
			db.setTransactionSuccessful();
			db.endTransaction();
			db.close();
			return "done insertion ";
		} catch (Exception ex) {

			db.endTransaction();
			db.close();
			return ex.getMessage();
		}


	}

	public String InsertRegionBulk(List<Country> countryList) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			db.beginTransaction();
			for (int i = 0; i < countryList.size(); i++) {
				Country f = countryList.get(i);
				ContentValues value = new ContentValues();

				value.put("countryCode", f.getCountryCode());
				value.put("countryName", f.getCountryName());
				value.put("countryId", f.getId());
				value.put("status", f.getStatus());

				db.insert(TABLE_COUNTRY, null, value);

			}
			db.setTransactionSuccessful();
			db.endTransaction();
			db.close();
			return "done insertion ";
		} catch (Exception ex) {

			db.endTransaction();
			db.close();
			return ex.getMessage();
		}


	}*/

	public void clearDB()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        Log.d(TAG, "Database Cleared and recreated");
        // Create tables again
        onCreate(db);
    }

	/**
	 * Storing user details in database
	 * */
	public void addUser(String name, String email, String password, String uid, String created_at, String fbtoken) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name); // Name
		values.put(KEY_EMAIL, email); // Email
		values.put(KEY_PASSWORD, password); // Password
		values.put(KEY_UID, uid); // Email
		values.put(KEY_CREATED_AT, created_at); // Created At
		values.put(KEY_FB_TOKEN, fbtoken);
		//values.put(REG_PUSH_TOKEN, push_token);

		// Inserting Row
		long id = db.insert(TABLE_USER, null, values);
		db.close(); // Closing database connection

		Log.d(TAG, "New user inserted into sqlite: " + id);
	}

	/**
	 * Getting user data from database
	 * */
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		String selectQuery = "SELECT  * FROM " + TABLE_USER;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			user.put("name", cursor.getString(1));
			user.put("email", cursor.getString(2));
			user.put("password", cursor.getString(3));
			user.put("uid", cursor.getString(4));
			user.put("created_at", cursor.getString(5));
			user.put("fbtoken", cursor.getString(6));
			//user.put("push_token", cursor.getString(7));

		}
		cursor.close();
		db.close();
		// notification_sound user
		Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

		return user;
	}

	/**
	 * Re crate database Delete all tables and create them again
	 * */
	public void deleteUsers() {
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_USER, null, null);
		db.close();

		Log.d(TAG, "Deleted all user info from sqlite");
	}

}
