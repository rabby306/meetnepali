package com.nixbin.MeetNepali.utilities;

/**
 * Created by Amit on 2/25/2018.
 */
import org.json.JSONObject;
public interface ServerCallBack {
    void onSuccess(String responseMessage, String responseData);
}
