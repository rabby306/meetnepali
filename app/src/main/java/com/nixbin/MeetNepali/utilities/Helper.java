package com.nixbin.MeetNepali.utilities;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nixbin.MeetNepali.R;
import com.nixbin.MeetNepali.webutility.AppConfig;

/**
 * Created by Amit on 2/25/2018.
 */

public class Helper {
    public static void testfunction()
    {
        Log.d("TEST", "Testing");
    }


    /*
    pictureID.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        enLargeImage(feeds.getContent());
                    }
                });
     */

    public static void enLargeImage(Context context, String imagePath) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(dialog.getWindow().FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.full_image);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.show();
        ImageView image = (ImageView) dialog.findViewById(R.id.image);

        Glide.with(context)
                .load(imagePath).placeholder(R.drawable.cast_album_art_placeholder)
                .into(image);
        image.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public static void debugPrint(String tag, String Message)
    {
        if(com.nixbin.MeetNepali.application.AppConfig.debug)
            Log.d(tag, Message);
    }


}
